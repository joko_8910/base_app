var gulp      = require('gulp');
var uglify    = require('gulp-uglify');
var cleanCSS  = require('gulp-clean-css');
var concat    = require('gulp-concat');
var pump      = require('pump');

gulp.task('default', function() {

  // place code for your default task here
});

gulp.task('minify-css', function () {
  gulp.src( [
          'assets/plugins/bootstrap/css/bootstrap.min.css',
          'assets/plugins/jvectormap/jquery-jvectormap-1.2.2.css',
          'assets/plugins/admin-lte/dist/css/AdminLTE.css',
          'assets/plugins/ionslider/ion.rangeSlider.css',
          'assets/plugins/ionslider/ion.rangeSlider.skinFlat.css',
          'assets/plugins/sweetalert2/sweetalert2.min.css',
          'assets/plugins/admin-lte/dist/css/skins/_all-skins.min.css',
          'assets/plugins/select2-4.0.4/dist/css/select2.min.css',
          'assets/plugins/select2-bootstrap-theme/dist/select2-bootstrap.min.css',
          'assets/plugins/datepicker/datepicker3.css',
          'assets/plugins/bar-rating/dist/themes/fontawesome-stars.css',
          'assets/plugins/iCheck/flat/blue.css',
          'assets/plugins/DataTables/datatables.min.css',
          'assets/plugins/summernote/summernote.css'
      ])
      .pipe(cleanCSS({
        relativeTo: 'plugins',
        target: 'assets',
        rebase: false,
        rebaseTo: '/s/'
      }))
      .pipe( concat('vendor.min.css') )
      .pipe( gulp.dest('assets/css/') );
});

gulp.task('compress-vendor', function (cb) {
  pump([
        gulp.src([
          'assets/plugins/jQuery/jquery-2.2.3.min.js',
          'assets/plugins/bootstrap/js/bootstrap.min.js',
          'assets/js/moment.js',
          'assets/plugins/datepicker/bootstrap-datepicker.js',
          'assets/plugins/fastclick/fastclick.js',
          'assets/plugins/sparkline/jquery.sparkline.min.js',
          // 'assets/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js',
          // 'assets/plugins/jvectormap/jquery-jvectormap-world-mill-en.js',
          'assets/plugins/slimScroll/jquery.slimscroll.min.js',
          'assets/plugins/chart.js/Chart.min.js',
          'assets/plugins/admin-lte/plugins/iCheck/icheck.min.js',
          'assets/plugins/sweetalert2/sweetalert2.min.js',
          'assets/plugins/ionslider/ion.rangeSlider.min.js',
          'assets/plugins/flot/jquery.flot.min.js',
          'assets/plugins/flot/jquery.flot.resize.min.js',
          'assets/plugins/knob/jquery.knob.js',
          'assets/plugins/bar-rating/dist/jquery.barrating.min.js',
          'assets/plugins/select2-4.0.4/dist/js/select2.min.js',
          // 'assets/js/cbpFWTabs.js',
          'assets/plugins/pace/pace.min.js',
          'assets/plugins/admin-lte/dist/js/app.js',
          'assets/plugins/jQuery-File-Upload/js/vendor/jquery.ui.widget.js',
          'assets/plugins/jQuery-File-Upload/js/jquery.iframe-transport.js',
          'assets/plugins/jQuery-File-Upload/js/jquery.fileupload.js',
          'assets/plugins/DataTables/datatables.min.js',
          'assets/plugins/jquery-maskmoney/jquery.maskMoney.min.js',
          'assets/plugins/summernote/summernote.min.js'
        ]).pipe( concat('vendor.min.js') ),
        uglify(),
        gulp.dest('assets/js')
    ],
    cb
  );
});

gulp.task('compress-app', function (cb) {
  pump([
        gulp.src('html/src/js/app.js'),
        uglify(),
        gulp.dest('assets/js')
    ],
    cb
  );
});
