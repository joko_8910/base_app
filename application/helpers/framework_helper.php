<?php
/**
 * @param  [string] $error [Database error message]
 * @return [string]        [Readable database error message]
 */
 function dbTranslateError( $error )
 {
	 if( strpos($error, 'Duplicate entry ') !== false ) {
		 return 'Data sudah ada';
	 }
	 return '';
 }

 /**
  * Mendapatkan data group dari user yang diisi
  * @param  [int]  $userId         id user yang ingin diambil data groupnya
  * @param  boolean $outputOnlyName  true > output hanya nama group (default)
  *                                  false > output berupa data array
  * @return [string|array]
  */
 function getUserRole($userId, $outputOnlyName = true)
 {
	 $This =& get_instance();
	 $userGroups = $This->ion_auth->get_users_groups($userId)->row();
	 if( $outputOnlyName ) {
		 return $userGroups->name;
	 }
	 return $userGroups;
 }

/**
 * Konversi mata uang ke format rupiah
 *
 * @param  [int] $number 	angka yang akan di konversi ke rupiah, contoh: 170000  akan menjadi Rp 17.000
 * @return [string]
 */
function formatRupiah($number, $withRp = true)
{
	if( $withRp ){
		return 'Rp ' . number_format($number, 0, ",", ".");
	} else {
		return number_format($number, 0, ",", ".");
	}
}


function unserializeArray( $array )
{
	$r = [];
	foreach ($array as $s) {
		$r[ $s['name'] ] = $s['value'];
	}
	return $r;
}

/**
 * Change array into jquery array that generate from .serializeArray()
 *
 * @param [array] $array
 * @return void
 */
function serializeArray( $array )
{
	$r = [];
	$index = 0;
	foreach ($array as $k => $v) {
		$r[$index]['name'] 	= $k;
		$r[$index]['value'] = $v;
		$index++;
	}
	return $r;
}

function getScheduleDuration( $start, $end )
{
	$start 	= str_replace(array(' AM', ' PM'), '', $start);
	$end 	= str_replace(array(' AM', ' PM'), '', $end);
	$startTime 	= strtotime(date('Y-m-d') .' '. $start . ':00');
	$endTime 	= strtotime(date('Y-m-d') .' '. $end . ':00');
	$s = $endTime - $startTime;
	return $s/60;
}

function moneyToK( $rupiah)
{
	if( $rupiah >= 1000 ) {
		return floor($rupiah/1000) . 'K';
	}
	return $rupiah;
}

function getEmailTemplate( $templateName )
{
	$This =& get_instance();
	$This->load->helper('file');
	$file = read_file( APPPATH. 'views/email/' . $templateName . '.tpl.php' );
	$r = str_replace('{{urllogo}}', base_url() . 'assets/images/azabu-email-logo.png', $file);
	$r = str_replace('{{url}}', base_url(), $r);
	$r = str_replace('{{address}}', getOptions('address'), $r);
	return $r;
}

function generateFormQuery( $param = array() )
{
	if( empty( $param ) ) return null;
	$r = null;
	foreach ($param as $key) {
		if( isset( $_GET[$key] ) ) {
			$query = $_GET[$key];
			$r .= '<input type="hidden" name="'. $key .'" value="'. $query .'">';
		}
	}
	return $r;
}

function getMapsLatLng( $type = "lat" )
{
	$t['lat'] = 1;
	$t['lng'] = 2;
	$maps = getOptions('maps');
	preg_match('/@(\-?[0-9]+\.[0-9]+),(\-?[0-9]+\.[0-9]+)/', $maps, $match );
	return $match[ $t[ $type ] ];
}

/**
 * Merubah nama bulan, hari dalam bahasa Inggris menjadi bahasa Indonesia
 * @param  [string] $string [format tanggal. contoh 2 januari 2012]
 * @return [string]         [format tanggal menjadi bahasa Indonesia]
 */
 function tglIndo( $string )
 {
	 $patterns = array();
	 $patterns[0] = '/Sunday/';
	 $patterns[1] = '/Monday/';
	 $patterns[2] = '/Tuesday/';
	 $patterns[3] = '/Wednesday/';
	 $patterns[4] = '/Thursday/';
	 $patterns[5] = '/Friday/';
	 $patterns[6] = '/Saturday/';
	 $patterns[7] = '/January/';
	 $patterns[8] = '/February/';
	 $patterns[9] = '/March/';
	 $patterns[10] = '/April/';
	 $patterns[11] = '/May/';
	 $patterns[12] = '/June/';
	 $patterns[13] = '/July/';
	 $patterns[14] = '/August/';
	 $patterns[15] = '/October/';
	 $patterns[16] = '/December/';
	 $replacements 		= array();
	 $replacements[0] 	= 'Senin';
	 $replacements[1] 	= 'Selasa';
	 $replacements[2] 	= 'Rabu';
	 $replacements[3] 	= 'Kamis';
	 $replacements[4] 	= 'Jumat';
	 $replacements[5] 	= 'Sabtu';
	 $replacements[6] 	= 'Minggu';
	 $replacements[7] = 'Januari';
	 $replacements[8] = 'Februari';
	 $replacements[9] = 'Maret';
	 $replacements[10] = 'April';
	 $replacements[11] = 'Mei';
	 $replacements[12] = 'Juni';
	 $replacements[13] = 'Juli';
	 $replacements[14] = 'Agustus';
	 $replacements[15] = 'Oktober';
	 $replacements[16] = 'Desember';
	 return preg_replace($patterns, $replacements, $string);
 }

 /**
  * Mencari selisih antara 2 tanggal, format: Y-m-d
  * @param  string  $tglAwal
  * @param  string  $tglAkhir
  * @param  boolean $withSymbol If true, the result with include '+' or '-' symbol
  * @return string
  */
 function cariSelisihTgl( $tglAwal = null, $tglAkhir = null, $withSymbol = false )
 {
	$diffDt1    = date_diff( date_create($tglAwal), date_create($tglAkhir) );
    $selisih   	= $diffDt1->format("%R%a");
	 if( $selisih <= 0 ) {
		 if( $withSymbol ) {
			 return str_replace('+', '-', $selisih);
		 } else {
			 return 0;
		 }
	 } else {
		 if( $withSymbol ) {
			 return str_replace('-', '+', $selisih);
		 } else {
			 return str_replace('+', '', $selisih);
		 }
	 }
 }

 /**
 * HTML snippet untuk menampilkan pesan warning
 * @param  [string] $text [pesan]
 * @return [string]
 */
function messageWarning( $text = NULL )
{
	$result = '<div class="alert alert-warning alert-dismissible" role="alert">
	  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	  <strong><i class="fa fa-exclamation-circle fa-lg fa-fw"></i></strong>' . $text . '
	</div>';
	return preg_replace( "/\r|\n/", "", $result );
}

/**
 * HTML snippet untuk menampilkan pesan sukses
 * @param  [string] $text [pesan]
 * @return [string]
 */
function messageSuccess( $text = NULL )
{
	$result = '<div class="alert alert-success alert-dismissible" role="alert">
	  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	  <strong><i class="fa fa-check-circle-o fa-lg fa-fw"></i></strong>' . $text . '
	</div>';
	return preg_replace( "/\r|\n/", "", $result );
}

/**
 * HTML snippet untuk menampilkan pesan info
 * @param  [string] $text [pesan]
 * @return [string]
 */
function messageInfo( $text = NULL )
{
	$result = '<div class="alert alert-info alert-dismissible" role="alert">
	  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	  <strong><i class="fa fa-info-circle fa-lg fa-fw"></i></strong>' . $text . '
	</div>';
	return preg_replace( "/\r|\n/", "", $result );
}

/**
 * HTML snippet untuk menampilkan pesan error
 * @param  [string] $text [pesan]
 * @return [string]
 */
function messageError( $text = NULL )
{
	$result = '<div class="alert alert-danger alert-dismissible" role="alert">
	  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	  <h4><i class="icon fa fa-times-circle-o"></i> Error!</h4>' . $text . '
	</div>';
	return preg_replace( "/\r|\n/", "", $result );
}

/**
 * HTML snippet untuk menampilkan label
 * @param  [string] $text       [text label]
 * @param  [string] $class_name [nama kelas tambahan untuk label]
 * @return [string]
 */
function textLabel( $text, $class_name )
{
	return '<span class="label ' . $class_name . '">' . $text . '</span>';
}

function fixedNotification($text, $class = "warning")
{
	return '<div class="fixed-notification '.$class.'"><a class="pull-right close-btn" href="#" data-toggle="tooltip" data-placement="left" title="Close this message" style="color: rgb(255, 255, 255); font-size: 20px;">×</a>'.$text.'</div>';
}

/**
 * HTML snippet untuk menampilkan label status apakah pending, approved, atau blacklist
 * @param  [type] $status [description]
 * @return [type]         [description]
 */
function statusLabelHtml( $status )
{
	if( is_null( $status ) ) $status = 'belum ada';
	switch ($status) {
		case 'pending':
			$text = textLabel( strtoupper( $status ), 'label-warning fixed-width');
			break;
		case 'approved':
			$text = textLabel( strtoupper( $status ), 'label-success fixed-width');
			break;
		case 'verified':
		$text = textLabel( strtoupper( $status ), 'label-success fixed-width');
		break;
		default:
			$text = textLabel( strtoupper( $status ), 'label-danger fixed-width');
			break;
	}
	return $text;
}

function getFormatIssueType( $name = 'idea' )
{
	switch ( $name) {
		case 'idea':
			$text = '<span class="vi-icon-circle" data-toggle="tooltip" data-placement="top" title="Idea"><i class="fa fa-lightbulb-o text-md"></i></span>';
			break;
		case 'enh':
			$text = '<span class="vi-icon-circle" data-toggle="tooltip" data-placement="top" title="Enhancement"><i class="fa fa-arrow-circle-o-up text-md"></i></span>';
			break;
		case 'bugs':
			$text = '<span class="vi-icon-circle" data-toggle="tooltip" data-placement="top" title="Bugs"><i class="fa fa-bug text-md" data-toggle="tooltip"></i></span>';
			break;
		default:
			$text = '<span class="vi-icon-circle" data-toggle="tooltip" data-placement="top" title="Assigment"><i class="fa fa-check-circle-o text-md"></i></span>';
			break;
	}
	return $text;
}

function getFormatIssuePriority( $name = 'high' )
{
	switch ( $name) {
		case 'high':
			$text = textLabel( strtoupper( $name ), 'label-danger label-md fixed-width' );
			break;
		case 'medium':
			$text = textLabel( strtoupper( $name ), 'label-warning label-md fixed-width' );
			break;
		default:
			$text = textLabel( strtoupper( $name ), 'label-success label-md fixed-width' );
			break;
	}
	return $text;
}

function getFormatIssueStatus( $name = 'new' )
{
	switch ( $name) {
		case 'ongoing':
			$text = textLabel( strtoupper( $name ), 'label-danger label-md fixed-width' );
			break;
		case 'solved':
			$text = textLabel( strtoupper( $name ), 'label-success label-md fixed-width' );
			break;
		case 'closed':
			$text = textLabel( strtoupper( $name ), 'bg-navy label-md fixed-width' );
			break;
		case 'ongoing':
			$text = textLabel( strtoupper( $name ), 'label-warning label-md fixed-width' );
			break;
		case 'new':
			$text = textLabel( strtoupper( $name ), 'label-success label-md fixed-width' );
			break;
		default:
			$text = textLabel( strtoupper( $name ), 'bg-aqua label-md fixed-width' );
			break;
	}
	return $text;
}

/**
 * Get the project status indicator end echoed with label in it
 * @param  string $status the project status
 * @return string         formatted html
 */
function getProjectStatusIndicator( $status, $dateUpdate = array() )
{
	$indicator['on_user'] 		  = 0;
	$indicator['on_developer'] 	= 1;
  $indicator['on_admin'] 		  = 2;
	$indicator['on_editor'] 		= 3;
	$indicator['on_progress'] 	= 4;

	$indicatorIndex = array_flip( $indicator );
	$statusOnProgress = false;
	$label = '';
	for ($i=0; $i < 3; $i++) {
		$tooltip = '';
		if( ! empty( $dateUpdate ) && ! empty( $dateUpdate[ $indicatorIndex[$i] ] ) ) {
			$postDate = strtotime( $dateUpdate[ $indicatorIndex[$i] ] );
			$now = time();
			$units = 1;
			$tooltip = 'data-toggle="tooltip" data-placement="top" title="Finished '. timespan($postDate, $now, $units) .' ago"';
		}
		if( $i < $indicator[ $status ] ) {
			$label .= '<i class="fa fa-check-circle text-success" '.$tooltip.'></i> ';
			$statusOnProgress = true;
		} else {
			if( $statusOnProgress ) {
				$tooltip = 'data-toggle="tooltip" data-placement="top" title="On Progress"';
				$label .= '<i class="fa fa-sm fa-circle-o text-danger"'.$tooltip.'></i> ';
				$statusOnProgress = false;
				$i++;
			}
			if( $i < 3 ) {
				$label .= '<i class="fa fa-sm fa-circle text-danger"></i> ';
			}

		}
	}

	$text['on_user'] 		= '<span class="text">With User</span>';
	$text['on_developer'] 	= '<span class="text">With Developer</span>';
	$text['on_admin'] 		= '<span class="text">With Admin</span>';
  $text['on_editor'] 	= '<span class="text">With Editor</span>';
	$text['on_proggress'] 	= '<span class="text">With proggress</span>';
	if( isset( $indicator[ $status ] ) && isset( $text[ $status ] ) ) {
		$label .= $text[ $status ];
		return textLabel( $label, 'label-indicator' );
	}
	return '-';
}

/**
 *
 * Retrieve group id based from group that has defined before
 * Group id was get from database on write manualy in MY_CONTROLlER
 *
 * @param  string $groupType The group name
 * @return int|booelan       return group id if the group type match and return false ig otherwise
 */
function getGroupId( $groupType )
{
	$groupId['admin'] = ADMIN_GROUP_ID;
	$groupId['developer'] = DEVELOPER_GROUP_ID;
	$groupId['editor'] = EDITOR_GROUP_ID;
	$groupId['user'] = USER_GROUP_ID;
	if( isset( $groupId[ $groupType ] ) ) return $groupId[ $groupType ];
	return false;
}

/**
 *
 * Restrict page that only access from ajax, otherwise display 404 page.
 * @return  void
 */
function onlyAjax()
{
	$This =& get_instance();
	if ( ! $This->input->is_ajax_request() ) show_404();
}

/**
 * Return app setting value the get from database
 * @param  string $key setting name
 * @return
 */
function getAppSetting( $key )
{
	$This =& get_instance();
	$This->load->model('appsettings_m');
	$r = $This->appsettings_m->get([
			'where' => [
				'name' => $key
			]
		])->row();
	if( empty( $r ) ) {
		return null;
	}
	return $r->value;
}

/**
 * Update app_settingg in database
 * @param  string $key   setting name
 * @param  string $value the new value
 * @return
 */
function updateAppSetting( $key, $value )
{
	$This =& get_instance();
	$This->load->model('appsettings_m');
	$This->appsettings_m->update([
				'value' => $value
			],[ 'name' => $key ]
		);
	return true;
}

/**
 * Set header content type as json
 * @return void
 */
function jsonContentType()
{
	header('Content-Type: application/json');
}

function labelColor($name='', $status)
{
  if ($status == 'danger') :
		$text = textLabel( strtoupper( $name ), 'label-danger label-md fixed-width' );
  elseif ($status == 'warning') :
		$text = textLabel( strtoupper( $name ), 'label-warning label-md fixed-width' );
  elseif ($status == 'info') :
		$text = textLabel( strtoupper( $name ), 'label-info label-md fixed-width' );
  elseif ($status == 'success') :
		$text = textLabel( strtoupper( $name ), 'label-success label-md fixed-width' );
  elseif ($status == 'primary') :
		$text = textLabel( strtoupper( $name ), 'label-primary label-md fixed-width' );
  else:
		$text = textLabel( strtoupper( $name ), 'label-default label-md fixed-width' );
  endif;
	return $text;
}

/**
 * Return date time that same with database format
 * @return string
 */
function getDatetime()
{
	return date('Y-m-d H:i:s');
}

/**
 * Create flat checkbox
 * The parameter was following parameter form_checkbox Codeigniter
 */
function flatCheckbox($name, $text, $value, $isChecked = false, $attr = array())
{

	if( isset( $attr['class'] ) ) {
		$attr['class'] .= ' icheck';
	} else {
		$attr['class'] = 'icheck';
	}
	return '<label class="app-icheck-label">' . form_checkbox($name, $value, $isChecked, $attr) . $text . '</label>';
}

/**
 * Record activity into database
 * @param  string  $text      	Note detail about the activity
 * @param  string  $type      	Kategory/Tipe log. Avaiable type: project|admin|user|editor|developer|issue
 * @param  string  $action      Jenis action log. Avaiable action: update|delete|add
 * @param  boolean $sendEmail 	Whether also send email to BOD User or not
 * @return void
 */
function writeLog( $text, $type, $action, $sendEmail = false )
{
	$This =& get_instance();
	$This->load->model('Applog_m');
	$userId = $This->ion_auth->user()->row()->id;
	$This->applog_m->insert([
		'user_id' 	=> $userId,
		'note'		=> $text,
		'action'	=> $action,
		'type'		=> $type,
		'create_at' => getDatetime()
	]);
}

/**
 * Send direct message to recipent. Save into database and create cache
 * This cache will check by Sent-Server for live notification
 * @param  int 		$sender   Sender User Id
 * @param  int 		$recipent Recipent User Id
 * @param  text 	$msg      Message
 * @return void
 */
function sendDm( $sender, $recipent, $msg )
{

	$This =& get_instance();
	$This->load->model('directmessage_m');

	$model = $This->directmessage_m;
	$data = array(
		'sender' 	=> $sender,
		'recipent' 	=> $recipent,
		'message'	=> $msg
	);
	$model->insert($data);
	$lastId = $model->getLastId();

	$data['id'] = $lastId;

	// after insert into database, automatically create data cache for noticiation
	// format of the cache name is dm-senderid-recipent-id
	$cacheName = "dm-$recipent";
	$cacheEngine = $This->app_cache->getCacheItem( $cacheName );
	if ( is_null( $cacheEngine->get() ) ) {

        $cacheEngine->set($data)->expiresAfter(60000);
        $This->app_cache->saveCache($cacheEngine);

    }

	return true;
}

/**
 * If some action need notif for another user. Use this function
 * @param  int $sender   sender user id
 * @param  int $recipent recipent user id
 * @param  string $msg      message for notification
 * @return void
 */
function sendNotification( $sender, $recipent, $msg )
{

	$This =& get_instance();
	$data = array(
		'sender' 	=> $sender,
		'recipent' 	=> $recipent,
		'message'	=> $msg
	);
	// after insert into database, automatically create data cache for noticiation
	// format of the cache name is dm-senderid-recipent-id
	$cacheName = "notif-$recipent";
	$cacheEngine = $This->app_cache->getCacheItem( $cacheName );
	if ( is_null( $cacheEngine->get() ) ) {
        $cacheEngine->set($data)->expiresAfter(600000);
        $This->app_cache->saveCache($cacheEngine);
    }
	return true;
}

/**
 * Update project log based on the role
 * So system know the time that role make an update
 * @param  string $idProject the project code
 * @param  string $role      defined role: on_user|on_admin|on_editor|on_user
 * @return void
 */
function updateProjectLog($idProject, $role)
{
	$data[ $role ] = getDatetime();
	$This =& get_instance();
	$This->load->model('projectupdatelog_m');
	$model = $This->projectupdatelog_m;
	$model->update( $data, [
		'id_project' => $idProject
	]);
	return true;
}

function getProjectUpdateLog( $idProject )
{
	$This =& get_instance();
	$This->load->model('projectupdatelog_m');
	$model = $This->projectupdatelog_m;
	$dateUpdate = $model->getOne([
			'where' => ['id_project' => $idProject ]
		]);
	return $dateUpdate;
}

function fileSizeToKB($value='')
{
	if ($value > 999 && $value <= 999999) :
	    $result = floor($value / 1000) . ' MB';
	elseif ($value > 999999) :
	    $result = floor($value / 1000000) . ' GB';
	else :
	    $result = $value . ' KB';
	endif;
	return $result;
}

function extensionIcon($ext='file')
{
	$ext_low = strtolower($ext);
	$doc = array('doc','docx','odt','rtf','log','msg','pages','tex','txt','wpd','wps','dot','wbk','docm','dotx','dotm','docb');
	$xls = array('xls','xlt','xlm','xlsx','xlsm','xltx','xltm','xlsb','xla','xlam','xll','xlw');
	$ppt = array('ppt','pot','pps','pptx','pptm','ppam','ppsx','ppsm','sldx','sldm');
	$img = array('jpg','png','jpeg','exif','tiff','gif','bmp','ppm','pgm','pbm','pnm','webp','hdr','heif','bat','bpg','cgm','svg');
	$arc = array('a','ar','cpio','shar','lbr','iso','lbr','mar','sbx','tar','bz2','gz','lz','lzma','lzo','rz','sfark','sz','xz','z','7z','s7z','ace','afa','alz','apk','arc','arj','b1','b6z','ba','bh','rar','cab','car','cfs','cpt','dar','dd','dgc','dmg','ear','gca','ha','hki','ice','jar','kgb','pak','pea','pim','pit','rk','sda','sea','sfx','shk','sit','sitx','sqx','tgz','tlz','tbz2','uca','wim','zip','zipx','xp3','zz','ecc','par','par2','rev');
	if (in_array($ext_low, $doc)):
		$icon = '<span class="fa fa-file-word-o blue"></span>';
	elseif (in_array($ext_low, $xls)):
		$icon = '<span class="fa fa-file-excel-o green"></span>';
	elseif (in_array($ext_low, $ppt)):
		$icon = '<span class="fa fa-file-powerpoint-o orange"></span>';
	elseif (in_array($ext_low, $img)):
		$icon = '<span class="fa fa-file-photo-o purple"></span>';
	elseif (in_array($ext_low, $arc)):
		$icon = '<span class="fa fa-file-zip-o brown-young"></span>';
	elseif ($ext_low == 'pdf'):
		$icon = '<span class="fa fa-file-pdf-o red"></span>';
	else:
		$icon = '<span class="fa fa-file-o brown-old"></span>';
	endif;

	return $icon;
}

function trackUploadPath( $uploadPath )
{
	return FCPATH . 'uploads\\' . $uploadPath;
}


function dateMonthFull( $date )
{
	return date("d F Y", strtotime($date));
}

function romanic_number($integer, $upcase = true)
{
    $table = array('M'=>1000, 'CM'=>900, 'D'=>500, 'CD'=>400, 'C'=>100, 'XC'=>90, 'L'=>50, 'XL'=>40, 'X'=>10, 'IX'=>9, 'V'=>5, 'IV'=>4, 'I'=>1);
    $return = '';
    while($integer > 0)
    {
        foreach($table as $rom=>$arb)
        {
            if($integer >= $arb)
            {
                $integer -= $arb;
                $return .= $rom;
                break;
            }
        }
    }
    if ($upcase) {
    	return strtoupper($return);
    }else{
	    return $return;
    }
}

/**
 * Ordering multidimensional array based on its value
 * Usage: arrayOrderBy(<array to sort>, <key to sort>, SORT_ASC|SORC_DESC);
 * @return array
 */
function arrayOrderBy() {

    $args = func_get_args();
    $data = array_shift($args);
    foreach ($args as $n => $field) {
        if (is_string($field)) {
            $tmp = array();
            foreach ($data as $key => $row)
                $tmp[$key] = $row[$field];
            $args[$n] = $tmp;
            }
    }
    $args[] = &$data;
    call_user_func_array('array_multisort', $args);
    return array_pop($args);
}

/**
 * Display rating score into star symbol
 * @param  int $score the score
 * @return string
 */
function showRating( $score ) {
	$star = '';
	for ( $starIteration = 0; $starIteration < 5; $starIteration ++ ) {
		if ( $starIteration < $score ) {
			$star .= '<span class="fa fa-star star-rating-checked"></span> ';
		} else {
			$star .= '<span class="fa fa-star-o"></span> ';
		}
	}
	return $star;
}

function getArrValueFromKey( $array, $key )
{
	if( isset( $array[ $key ] ) ) {
		return $array[ $key ];
	}
	return null;
}

function is_hollow($data, $default = '')
{
	if(!empty($data)){
		return array($data,false);
	}else{
		return array($default,true);
	}
}

function hideThisIf($args, $default = '')
{
	if($args){
		return 'hide';
	}else{
		return $default;
	}
}
?>
