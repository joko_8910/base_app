<?php

class User_Controller extends MY_Controller
{

	protected $viewFolder = "user";

	public function __construct()
	{

		parent::__construct();

		if ( $this->ion_auth->logged_in() ) {

			$user_groups = $this->ion_auth->get_users_groups()->row();

			if( $user_groups->name != 'user' ) show_404();

		} else {

			redirect('login','refresh');
		}

	}



}

?>
