<?php
defined('BASEPATH') OR exit('No direct script access allowed');

use phpFastCache\CacheManager;


class MY_Controller extends CI_Controller {

	protected $cacheManager;
	protected $userData;
	protected $userGroup;
	protected $viewFolder = "";
	protected $dwooCore;
	protected $pageTitle = 'Dashboard';

	public function __construct()
	{

		parent::__construct();

		$this->_setConfiguration();

		// initia phpchache
		$this->dwooCore = new Dwoo\Core();

		$this->_initUserData();

	}

	private function _setConfiguration()
	{

		define('VERSION', '1');
		define('WEBTITLE', 'LOCALHOST' );
		define('IMAGETITLE', base_url('assets/images/app_logo.png') );
		define('ADMIN_LOGOUT_URL', base_url('gma/login/logout') );
		define('ADMINURL', base_url('admin') . '/' );
		define('DEVELOPERURL', base_url('developer') . '/' );
		define('USERURL', base_url('user') . '/' );
		define('EDITORURL', base_url('editor') . '/' );

		define('ADMIN_GROUP_ID', 6);
		define('DEVELOPER_GROUP_ID', 7);
		define('USER_GROUP_ID', 8);
		define('EDITOR_GROUP_ID', 9);

		define('MAXIMAGESIZE', 2000);
		define('MAXFILESIZE', 1000);
		date_default_timezone_set('Asia/Jakarta');

	}

	private function _initUserData()
	{

		if ( $this->ion_auth->logged_in() ) {

			$this->userData 	= $this->ion_auth->user()->row();
			$this->userGroup 	= $this->ion_auth->get_users_groups()->row();
		}

	}

	/**
	 *
	 * Create default session data when user logged in
	 * @return array
	 */
	protected function _buildLoggedUserData()
	{

		if( ! empty( $this->userData ) ) {

			$data['myName'] 	= $this->userData->name;
			$data['myUserId'] 	= $this->userData->id;
			$data['myEmail'] 	= $this->userData->email;

			$data['myRole']		= $this->userGroup->name;

			return $data;
		}

		return array();

	}

	protected function _validateUser( $getUrlOnly = false )
	{

		if ( $this->ion_auth->logged_in() ) {

			$user_groups = $this->ion_auth->get_users_groups()->row();
			$user = $this->ion_auth->user()->row();
			if( is_null( $user_groups ) ) redirect('login/logout','refresh');

			$userAction['admin'] 			= ADMINURL . 'dashboard';
			$userAction['user'] 			= USERURL . 'dashboard';
			$userAction['editor'] 		= EDITORURL . 'dashboard';
			$userAction['developer'] 	= DEVELOPERURL . 'dashboard';

			$redirectUrl = 'login';
			if( isset( $userAction[ $user_groups->name ] ) ) {
				$redirectUrl = $userAction[ $user_groups->name ];
			}

			if( $getUrlOnly ) {

				return $redirectUrl;
			} else {

				redirect( $redirectUrl );
				exit();
			}


		}
	}

	/**
	 * Kirim email
	 * @param  array  $args parameter untuk mengirim email terdiri dari:
	 *                      	$emailTo : email target yang akan dikirim
	 *                      	$targetName : Nama dari email target
	 *                      	$subject: Judul Email
	 *                      	$body: Isi pesan dari email
	 * @return [boelan|string]       return true, jika pengiriman sukses
	 *                                      return pesan error jika pengiriman gagal
	 */
	public function sendMail( $args = array() )
	{

		extract($args);

		//Create a new PHPMailer instance
		$mail = new PHPMailer;

		//Tell PHPMailer to use SMTP
		$mail->isSMTP();

		//Enable SMTP debugging
		// 0 = off (for production use)
		// 1 = client messages
		// 2 = client and server messages
		$mail->SMTPDebug = 0;

		$mail->isHTML(true);

		//Set the hostname of the mail server
		// $mail->Host = 'mail.klikumkm.com';
		// $mail->Host = 'smtp.gmail.com';

		$mail->SMTPAutoTLS = false;

		// use
		$mail->Host = 'smtp-relay.gmail.com ';
		// if your network does not support SMTP over IPv6

		//Set the SMTP port number - 587 for authenticated TLS, a.k.a. RFC4409 SMTP submission
		// $mail->Port = 25;
		$mail->Port = 587;

		//Set the encryption system to use - ssl (deprecated) or tls
		$mail->SMTPSecure = 'tls';

		//Whether to use SMTP authentication
		$mail->SMTPAuth = true;

		//Username to use for SMTP authentication - use full email address for gmail
		$mail->Username = "email@app.com";
		// $mail->Username = "noreply@vivevio.co";

		//Password to use for SMTP authentication
		$mail->Password = "password";

		//Set who the message is to be sent from
		$mail->setFrom('email@app.com', 'APPNAME');

		//Set an alternative reply-to address
		$mail->addReplyTo('email@app.com', 'APPNAME');

		//Set who the message is to be sent to
		$mail->addAddress( $emailTo, $targetName);

		//Set the subject line
		$mail->Subject = $subject;

		//Replace the plain text body with one created manually
		$mail->Body    = $body;

		//send the message, check for errors
		if (!$mail->send()) {
		    return $mail->ErrorInfo;
		} else {
		    return true;
		}

	}

	protected function _uploadImage( $uploadPath )
	{

		$dir = FCPATH . 'uploads/' . $uploadPath;
		if (!file_exists($dir)) {
		    mkdir($dir, 0777, true);
		    chmod($dir, 0777);
		}
		$config['upload_path']          = $dir;
        $config['allowed_types']        = 'jpg|png|jpeg';
        $config['max_size']            	= MAXIMAGESIZE; //2mb
        $config['file_name']            = 'app_' . strtolower(url_title( $_FILES['file']['name'])) . '_' . date('dmyhis');

        $this->load->library('upload', $config);

        if ( ! $this->upload->do_upload('file')) {

        	$return['status'] 	= false;
        	$return['message'] 	= $this->upload->display_errors('','');

        } else {

        	$return['status'] 	= true;
        	$return['data'] 	= $this->upload->data();
        }

        return $return;

	}

	protected function _uploadRepositoryFile( $uploadPath )
	{

		$dir = FCPATH . 'uploads/repository/' . $uploadPath;
		if (!file_exists($dir)) {
		    mkdir($dir, 0777, true);
		    chmod($dir, 0777);
		}
		$config['upload_path']          = $dir;
        $config['allowed_types']        = '*';
        $config['max_size']            	= MAXFILESIZE; //1mb
        $config['file_name']            = 'app_' . strtolower(url_title( $_FILES['file']['name'])) . '_' . date('dmyhis');

        $this->load->library('upload', $config);

        if ( ! $this->upload->do_upload('file')) {

        	$return['status'] 	= false;
        	$return['message'] 	= $this->upload->display_errors('','');

        } else {

        	$return['status'] 	= true;
        	$return['data'] 	= $this->upload->data();
        }

        return $return;

	}

	/**
     *
     * Display or Load view file like built-in CI load->view()
     * But, included load header and footer template
     *
     * @param  string  $view     View Filename
     * @param  array  $data     Array to send into view file
     * @param  boolean $onlyView Whether display only content or with header and footer template
     * @return string
     */
	public function template( $view, $data = '', $onlyView = false ) {

		$data['msg'] = $this->session->flashdata('msg');
		$data = array_merge( $this->_buildLoggedUserData(), $data);

		if( ! isset( $data['webTitle'] ) ) {
			$data['webTitle'] = WEBTITLE;
		}

		if( $onlyView ) {

	        $this->load->view( $this->viewFolder . '/' . $view . '_v', $data);
		} else {

	        $this->load->view( $this->viewFolder . '/template/header', $data);
	        $this->load->view( $this->viewFolder . '/' . $view . '_v', $data);
	        $this->load->view( $this->viewFolder . '/template/footer', $data);
		}
    }

    /**
     *
     * Display or Load view file but support dwoo template engine
     * If you not familiar yet with dwoo template engine.
     * Check this url http://dwoo.org/plugins/ for complete syntac documentation
     *
     * @param  string  $view     View Filename
     * @param  array  $data     Array to send into view file
     * @param  boolean $onlyView Whether display only content or with header and footer template
     * @return string
     */
    public function renderDwoo( $view, $data = array(), $onlyView = false )
    {

    	$data['msg'] = $this->session->flashdata('msg');
		$data = array_merge( $this->_buildLoggedUserData(), $data);
    	$data['title_image'] = IMAGETITLE;

		if( ! isset( $data['webTitle'] ) ) {
			$data['webTitle'] = WEBTITLE;
		}

		if( ! isset( $data['pageTitle'] ) ) {
			$data['pageTitle'] = ucfirst( $this->viewFolder ) .' | ' . WEBTITLE;
		} else {
			$data['pageTitle'] .= ' - ' . ucfirst( $this->viewFolder ) .' | ' . WEBTITLE;
		}


    	// Logic for load app js for spesific role
    	// System check if app js for that role is exist so system will load it
    	$data['roleAppJs'] = null;
    	$filenameRoleAppJs =  'assets/js/app-' . strtolower( $this->viewFolder ) .'.js';
    	if( file_exists( FCPATH . $filenameRoleAppJs ) ) {
	    	$data['roleAppJs'] = '<script src="'. base_url( $filenameRoleAppJs ) .'"></script>';
    	}

    	if( $onlyView || $this->input->is_ajax_request() == true ) {

	        $c = $this->dwooCore->get( VIEWPATH . $this->viewFolder . '/' . $view . '_v.php', $data );
	    	echo $c;
		} else {

	        $h = $this->load->view( $this->viewFolder . '/template/header', $data, true );
	        $s = $this->load->view( $this->viewFolder . '/template/sidebar', $data, true );
	        $c = $this->dwooCore->get( VIEWPATH . $this->viewFolder . '/' . $view . '_v.php', $data );
	        $f = $this->load->view( $this->viewFolder . '/template/footer', $data, true );

	        echo $h . $s . $c . $f;
		}

    }


}

require_once APPPATH . 'core/Admin_Controller.php';
require_once APPPATH . 'core/User_Controller.php';
require_once APPPATH . 'core/Editor_Controller.php';
require_once APPPATH . 'core/Developer_Controller.php';

/* End of file MY_Controller.php */
/* Location: ./application/core/MY_Controller.php */
