<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class MY_Model extends CI_Model {
		protected $lastId;
    protected $instanceCache;
    protected $query;
    protected $queryArgs = array();
    protected $returnMethod;
    protected $cacheKey;
    protected $tableName        = "";
    protected $defaultPerPage   = 10;
    protected $cacheExpired     = 180; //in second
    protected $offset           = 0;

    public function __construct()
    {
        parent::__construct();
    }

    /**
     * __call
     *
     * Acts as a simple way to call model methods without loads of stupid alias'
     *
		 * @param $method
     * @param $arguments
     * @return mixed
     * @throws Exception
     */
    public function __call($method, $arguments)

    {
        if (!method_exists( $this->app_cache, $method) )
        {
            throw new Exception('Undefined method App_cache::' . $method . '() called');
        }
        $this->app_cache->setTable( $this->tableName );
        return call_user_func_array( array($this->app_cache, $method), $arguments);
    }

    /**
     *
     * Mendapatkan data dari database pakai atau tanpa argumen yang dimasukkan
     * Argumen berupa array dimensional dimana key dari array tersebut mengikuti query builder dari codeigniter
     *
     * Contoh:
     * array(
     *  'where' => array( 'id' => 1 )
     * )
     *
     * Sama dengan di codeigniter $this->db->where( array( $id => id ) );
     *
     * Jika ingin melakukan join pada table terkait, bisa melalui param function. Contoh:
     * array(
     *  'function' => function() {
     *      //join query builder default dari codeigniter
     *   }
     * )
     *
     * Di param function ini bisa diisi juga query builder selain join, yaitu where, like, or_like, dll.
     * Untuk query limit direkomendasikan menggunakan param limit. Contoh:
     * array(
     *  'limit' => array( 5, 0 ) // will display 5 data from offset 0
     * )
     *
     * @param  array  $args
		 *
     * @return array
     */
    public function get( $args = array() )
    {
        if( empty( $args ) ) {
            $args = $this->queryArgs;
        }
        foreach ($args as $key => $value) {
            if( $key == 'function' ) {
                call_user_func( $args[ $key ], array());
            } elseif( in_array( $key, array('limit', 'order_by', 'where_in')) ){
                $this->offset = $value[1];
                call_user_func_array( array($this->db, $key), array( $value[0], $value[1] ) );
            }else {
                call_user_func_array( array($this->db, $key), array($value) );
            }
        }
        $this->query = $this->db->get($this->tableName);
        return $this->query;
    }

    /**
     *
     * Set the cache key
     * @param string $name name of cache key
     * @return string return name of setted name
     */
    public function setCacheKey( $name = null )
    {
       $this->cacheKey = $this->app_cache->setCacheKey( $name );
    }

    private function _cachedResult()
    {
        // do original query
        $this->get();
        // as caching not stable yet
        // we disable caching feature by return on this line
        return call_user_func_array( array( $this->query, $this->returnMethod), array() );
        if( empty( $this->cacheKey ) ) {
            $this->setCacheKey();
        }
        $cachedString = $this->getCacheItem( $this->cacheKey );
        if ( is_null( $cachedString->get() ) ) {
            $q =  call_user_func_array( array( $this->query, $this->returnMethod), array() );
            $cachedString->set( $q )->expiresAfter( $this->cacheExpired );
            $this->saveCache($cachedString);
            return $cachedString->get();
        } else {
            return $cachedString->get();
        }
    }

    public function getMany( $args = array(), $returnMethod = 'result_array' )
    {
        $this->queryArgs        = $args;
        $this->returnMethod     = $returnMethod;
        return $this->_cachedResult();
    }

    public function getOne( $args = array(), $returnMethod = 'row_array' )
    {
        $this->queryArgs        = $args;
        $this->returnMethod     = $returnMethod;
        return $this->_cachedResult();
    }

    public function insert( $data = array() ) {
        $query = $this->db->insert($this->tableName, $data);
        $this->lastId = $this->db->insert_id();
        return $query;
    }

    public function insertBatch( $data = array() )
    {
        $query = $this->db->insert_batch($this->tableName, $data);
        return $query;
    }

    public function update( $data = array(), $where = array() ) {
        $this->db->where($where);
        $q= $this->db->update($this->tableName, $data);
        // delete old cache
        $this->deleteCache();
        return $q;
    }

    public function delete( $where ) {
        $this->db->where( $where );
        $q=$this->db->delete($this->tableName);
        // delete old cache
        $this->deleteCache();
        return $q;
    }

    /**
     *
     * Get the id for the last insert query
     * @return int
     */
    public function getLastId()
    {
        return $this->lastId;
    }

    /**
     *
     * Get the error from doing database query
     * @return string
     */
    public function getError()
    {
        return $this->db->error();
    }

    public function getPerPage()
    {
        return $this->defaultPerPage;
    }

    /**
     *
     * Do query when datatable send post request
     *
     *  $join = array(
     *            [0] => array (
     *                     'f_table' = foreign table which want to joined,
     *                     'f_column'= foreign column or which column from foreign table have relation to connect another table,
     *                     'i_table' = index table which want to companre, (optional : default used model table)
     *                     'i_column'= index column or which column from current connected table contain foreign key,
     *                     'method'= method for join ex. inner or left, (optional : default left)
     *                  )
     *          )
     *
     */
    public function getDataTablePost( $where = array(), $join = array(), $select = '*', $attributeDT = array() )
    {
        $this->db->select($select);
        $this->db->from($this->tableName);
        if ( !empty($where) ) :
             $this->db->where($where);
        endif;
        if ( !empty($join) ) :
            foreach ($join as $foreign) :
                if ( !empty( $foreign['i_table'] ) ) {
                    $foreign['i_table'] = $foreign['i_table'];
                }else{
                    $foreign['i_table'] = $this->tableName;
                }
                if ( !empty( $foreign['method'] ) ) {
                    $foreign['method'] = $foreign['method'];
                }else{
                    $foreign['method'] = 'left';
                }
                if (empty($foreign['manual'])) {
                    $this->db->join(
                        $foreign['f_table'] ,
                        $foreign['f_table'].'.'.$foreign['f_column']
                        .' = '.
                        $foreign['i_table'].'.'.$foreign['i_column'],
                        $foreign['method']
                    );
                }else{
                    $this->db->join( $foreign['manual'] , $foreign['on'] , $foreign['method'] );
                }
            endforeach;
        endif;
        $i = 0;

        foreach ($attributeDT['column_search'] as $item) : // loop kolom
            if($_POST['search']['value']) : // if datatable send POST for search
                if($i===0) :// first loop
                    $this->db->group_start(); // open bracket. query Where dengan OR, bisa jadi bisa gabungan keduanya
                    $this->db->like($item, $_POST['search']['value']);
                else :
                    $this->db->or_like($item, $_POST['search']['value']);
								endif;
                if(count($attributeDT['column_search']) - 1 == $i) //last loop
                    $this->db->group_end(); //tutup bracket
            endif;
            $i++;
        endforeach;
        if(isset($_POST['order'])) : // disini order proses
            $this->db->order_by($attributeDT['column_order'][$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        elseif(isset($attributeDT['order'])):
            $order = $attributeDT['order'];
            $this->db->order_by(key($order), $order[key($order)]);
				endif;
    }

    /**
     *  Get the data when datatable send post request
     *  @return object
     */
    function get_datatables( $where = array(), $join = array(), $select = '*', $attributeDT = array() )
    {
        $this->getDataTablePost( $where, $join, $select, $attributeDT );
        if($_POST['length'] != -1)
        $this->db->limit($_POST['length'], $_POST['start']);
        $query = $this->db->get();
        return $query->result();
    }

    /**
     *
     *  Get the value of total query from getDataTablePost()
     *  @return int
     */
    function count_filtered( $where = array(), $join = array(), $select = '*', $attributeDT = array() )
    {
        $this->getDataTablePost( $where, $join, $select, $attributeDT );
        $query = $this->db->get();
        return $query->num_rows();
    }

    /**
     *
     *  Get the value of total data at current table
     *  @return int
     */
    public function count_all()
    {
        $this->db->from($this->tableName);
        return $this->db->count_all_results();
    }
}

/* End of file MY_Model.php */
/* Location: ./application/core/MY_Model.php */
