<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>GMA Project</title>

  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">

    <!-- Custom css -->
  <link rel="stylesheet" href="<?php echo base_url("assets/css/admin-login.css"); ?>">
  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
</head>
<body>
  <div class='login'>
    <!-- Login Panel -->
    <div class="login-panel">
      <div class='login-logo'>
        <span>APP</span> lication
      </div>
      <div class='login_title'>
        <span>Login to your account</span>
      </div>
      <form action="<?php echo base_url('login/process') ?>" method="POST" id="form-login">
        <div class='login_fields'>
          <div class='login_fields__user'>
            <div class='icon'>
              <img src='<?php echo base_url('assets/images') ?>/user_icon.png'>
            </div>
            <input placeholder='Username' name="username" type='text'>
              <div class='validation'>
                <img src='<?php echo base_url('assets/images') ?>/tick.png'>
              </div>
            </input>
          </div>

          <div class='login_fields__password'>
            <div class='icon'>
              <img src='<?php echo base_url('assets/images') ?>/lock_icon.png'>
            </div>
            <input placeholder='Password' name="pwd" type='password'>
            <div class='validation'>
              <img src='<?php echo base_url('assets/images') ?>/tick.png'>
            </div>
          </div>

          <div class='login_fields__submit'>
            <input type='submit' class="btn" value='Log In'>
            <div class='forgot'>
              <!-- <a href='#'>Forget Your Password?</a> -->
            </div>
          </div>
        </div>
      </form>
    </div>
    <!-- Login Panel -->

    <div class='success panel-result'>
      <h2>Authentication Success</h2>
      <p>Welcome back, <span class="name">User</span></p>
      <p>System will redirecting you to the dashboard page.</p>
    </div>

    <div class='failed panel-result'>
      <h2>Authentication Failed</h2>
      <p class="error-text"></p>
      <div class='login_fields__submit'>
        <a href="#" class="btn login-try">Try Again</a>
        <div class='forgot'>
          <!-- <a href='#'>Forget Your Password?</a> -->
        </div>
      </div>
    </div>

  </div>

<div class='authent'>
  <img src='<?php echo base_url('assets/images/loading-pulse.svg') ?>'>
  <p>Authenticating...</p>
</div>

<!-- jQuery 2.2.3 -->
<script src="<?php echo base_url("assets/plugins/jQuery/jquery-2.2.3.min.js"); ?>"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.2/jquery-ui.min.js"></script>
<script>

  var windowWidth = $(window).width();

  $('a.login-try').on('click', function () {


    $('.failed').fadeOut();
    $('.success').fadeOut();
    $('.login').addClass('test')

    setTimeout(function(){

      $('.login').removeClass('test');
      $('.login .login-panel').fadeIn();
      $('.login .login-panel div').fadeIn();
    },500);
  });

  $('form#form-login').on('submit', function(){

    var form = $('form#form-login');
    var authentMoveRight = -220;
    if( windowWidth <= 480 ) {
      authentMoveRight = -100;
    }

    // -----------------------------------------------------
    // Login Proccess has been started
    // -----------------------------------------------------
    $('.login').addClass('test')

    setTimeout(function(){
      $('.login').addClass('testtwo')
    },300);
    setTimeout(function(){
      $(".authent").show().animate({right: authentMoveRight },{easing : 'easeOutQuint' ,duration: 600, queue: false });
      $(".authent").animate({opacity: 1},{duration: 200, queue: false }).addClass('visible');
    },500);

    $.ajax({
      type: 'POST',
      url: form.attr('action'),
      data: form.serializeArray(),
      dataType: 'json',
      success: function ( r ) {

          // -----------------------------------------------------
          // Login Proccess Done
          // -----------------------------------------------------
          $(".authent").show().animate({right:90},{easing : 'easeOutQuint' ,duration: 600, queue: false });
          $(".authent").animate({opacity: 0},{duration: 200, queue: false }).addClass('visible');
          $('.login').removeClass('testtwo')

          setTimeout(function(){
            $(".authent").hide();
            $('.login').removeClass('test')
            $('.login .login-panel').fadeOut(123);

          },300);

          if( r.status == 'ok' ) {


            setTimeout(function(){
              $('.success').fadeIn();
            },500);

            $('.success .name').html( r.name );

            // redirecting
            setTimeout(function(){

              window.location = r.redirectUrl;

            }, 2000);

          } else {

            setTimeout(function(){

              $('.failed').fadeIn();
              $('.failed .error-text').html( r.message );

            },500);

          }

      }

    })

    return false;

  });

  // -----------------------------------------------------
  // Animation interaction when user focus on input field
  // -----------------------------------------------------
  $('input[type="text"],input[type="password"]').focus(function(){
    $(this).prev().animate({'opacity':'1'},200)
  });

  $('input[type="text"],input[type="password"]').blur(function(){
    $(this).prev().animate({'opacity':'.5'},200)
  });

  $('input[type="text"],input[type="password"]').keyup(function(){
    if(!$(this).val() == ''){
      $(this).next().animate({'opacity':'1','right' : '30'},200)
    } else {
      $(this).next().animate({'opacity':'0','right' : '20'},200)
    }
  });
  // --------------------------------------------------------------------------



</script>
</body>
</html>
