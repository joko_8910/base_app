</div>
<!-- Content Wrapper. Contains page content -->

<footer class="main-footer">
	<div class="pull-right hidden-xs">
	  <b>APP Project</b> <?php echo VERSION ?>
	</div>
	<strong>Copyright &copy; <?php echo date("Y"); ?> <a href="<?php echo base_url() ?>">APP</a>.</strong> All rights reserved
	<!--  Thank you AdminLTE by http://almsaeedstudio.com Almsaeed Studio -->
</footer>
<script>
	// ADDITIONAL SCRIPT
	var LOADERGIF 	= '<?php echo base_url() ?>assets/images/Nicolas.gif';
	var ROLEURL 		= '<?php echo USERCURL ?>';
	var URLSTREAM 	= '<?php echo base_url() ?>api/getNotification';
	var BASEURL 		= '<?php echo base_url(); ?>';
	// PRELOADER CONFIG
	window.paceOptions = {
	  ajax: false
	}
</script>
<script src="<?php echo base_url()?>assets/js/vendor.min.js"></script>
<script>
	// custom preloader after its done
	Pace.on('done', function () {
		$('.app--wrapper').removeClass('app--wrapper--hide');
	});
</script>
<script src="<?php echo base_url()?>assets/js/app.js"></script>
<!-- Load App js spesicific for the role -->
<?php echo $roleAppJs ?>
</body>
</html>
