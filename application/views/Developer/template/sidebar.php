<!-- Left side column. contains the logo and sidebar -->
<aside class="main-sidebar" style="position: fixed;">
  <!-- sidebar: style can be found in sidebar.less -->
  <section class="sidebar" style="position: relative;">
    <div class="left50">
      <div>
          <!-- search -->
          <a href="" data-toggle="modal" data-target="#mySearch">
            <span class="fa fa-lg fa-search"></span>
          </a>
      </div>
    </div>
    <div class="left240">
      <ul class="sidebar-menu">
        <li>
          <a href="<?php echo DEVELOPERURL ?>dashboard" class="display--preloader--first">
            <i class="fa fa-lg fa-dashboard"></i> <span>Dashboard</span>
          </a>
        </li>
        <li class="treeview">
          <a href="#">
            <i class="fa fa-lg fa-shopping-basket "></i>
            <span class="fourteen" >MENU</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li>
              <a href="<?php echo DEVELOPERURL ?>purchase_list">
                <i class="fa fa-lg fa-file-o"></i> <span class="threeteen">ONE</span>
              </a>
            <li>
            <li>
              <a href="<?php echo DEVELOPERURL ?>credit_list">
                <i class="fa fa-lg fa-coffee"></i> <span class="threeteen">TWO</span>
              </a>
            </li>
          </ul>
        </li>

        <li class="treeview">
          <a href="#">
            <i class="fa fa-lg fa-user-circle"></i>
            <span><b><?php echo strtoupper( $this->ion_auth->get_users_groups()->row()->name ); ?></b></span>
            <br>
            <i class="fa"></i>
            <span style="font-size: 11px;" class="fourteen" ><?php echo ucwords( strtolower( $this->ion_auth->user()->row()->name ) ); ?></span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li>
              <a href="<?php echo DEVELOPERURL . 'user' ?>" class="display--preloader--first">
                <i class="fa fa-lg fa-cog"></i>
                <span class="threeteen">Settings</span>
              </a>
            </li>
            <li>
              <a href="<?php echo base_url('login/logout'); ?>">
                <i class="fa fa-lg fa-sign-out"></i>
                <span class="threeteen">Logout</span>
              </a>
            </li>
          </ul>
        </li>

      </ul>

    </div>
    <!-- <div>
    </div> -->
  </section>
  <!-- /.sidebar -->
</aside>

<div class="content-wrapper">
<!-- Content Header (Page header) -->
