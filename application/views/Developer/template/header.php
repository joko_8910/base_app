<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title><?php echo $pageTitle; ?></title>
  <link rel="shortcut icon" href="<?php echo $title_image ?>" />
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
  <link rel="stylesheet" href="<?php echo base_url()?>assets/css/vendor.min.css">
  <!-- Custom css -->
  <link rel="stylesheet" href="<?php echo base_url()?>assets/css/custom.css">

</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper app--wrapper app--wrapper--hide">

  <header class="main-header" style="position: fixed;">

    <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
      <!-- <span class="logo-mini"><b>A</b>LT</span> -->
      <span class="sr-only">Toggle navigation</span>
    </a>

    <!-- Logo -->
    <a href="<?php echo DEVELOPERURL ?>dashboard" class="logo">
      <!-- mini logo for sidebar mini 50x50 pixels -->
      <!-- <span class="logo-mini"><b>A</b>LT</span> -->
      <!-- logo for regular state and mobile devices -->
      <span class="logo-lg"><b>APP</b>'s PROJECT</span>
    </a>
    <!-- Sidebar toggle button-->


  </header>
