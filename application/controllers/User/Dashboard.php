<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends User_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('App_m');
		$this->load->helper('security');
		date_default_timezone_set("Asia/Jakarta");
	}

	public function index()
	{
		$data = null;
		$this->renderDwoo('dashboard/index', $data);
	}

}

/* End of file Dashboard.php */
/* Location: ./application/controllers/Developer/Dashboard.php */
