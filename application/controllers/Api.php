<?php
defined('BASEPATH') OR exit('No direct script access allowed');

use Igorw\EventSource\Stream;

/**
 * This Controller purposed for Menu/Module that not have dependency
 */
class Api extends MY_Controller {

	public function __construct()
	{

		parent::__construct();
		$this->load->helper('security');

	}

	public function getUserByGroup( $groupType )
	{

		// make sure its from ajax only
		onlyAjax();
		$this->load->model('users_m');

		$search = $this->input->get('s');
		$args 	= [];

		if( ! empty( $search ) ) {
			$users = $this->users_m->searchUserByName( $search, getGroupId( $groupType ) );
		} else {
			$users = $this->ion_auth->users( getGroupId( $groupType ) )->result_array();
		}

		$result = array();

		if( ! empty( $users ) ) {

			$index = 0;
			foreach ($users as $user) {

				$result['items'][$index]['id'] 	= $user['id'];
				$result['items'][$index]['text'] = $user['name'];
				$index++;
			}


		}

		echo json_encode( $result );

	}

	public function getSometing() {

		// make sure its from ajax only
		onlyAjax();
		$this->load->model('app_m');

		$search = $this->input->get('s');

		$this->material_m->setCacheKey( $search );
		$query = $this->material_m->getMany([
			'like' => [
				'name' => xss_clean( $search )
			]
		]);
		$result = array();

		if( ! empty( $query ) ) {

			$index = 0;
			foreach ($query as $data) {

				$result['items'][$index]['data'] 	= $data;
				$result['items'][$index]['id'] 		= $data['id'];
				$result['items'][$index]['text'] 	= $data['name'];
				$index++;

			}


		}

		jsonContentType();
		echo json_encode( $result );
	}

	public function uploadImage( $folder )
	{

		onlyAjax();

		$upload = $this->_uploadImage( $folder );
		if( $upload['status'] ) {

			$response['status'] 	= 'ok';
			$response['data'] 		= $upload['data'];
			$response['imageUrl'] 	= base_url('uploads/') . $folder .'/'. $response['data']['file_name'];
		} else {

			$response['status'] 	= 'error';
			$response['message'] 	= $upload['message'];
		}

		jsonContentType();
		echo json_encode( $response );

	}

	public function deleteImage( $folder )
	{

		onlyAjax();
		$post = $this->input->post();
		$dir = FCPATH . 'uploads/'. $folder .'/' . $post['file'];
		if( file_exists($dir)) {
			unlink( $dir );
		}

		echo 'ok';
	}

	public function uploadRepositoryFile( $folder )
	{

		onlyAjax();

		$upload = $this->_uploadRepositoryFile( $folder );
		if( $upload['status'] ) {

			$response['status'] 	= 'ok';
			$response['data'] 		= $upload['data'];
			$response['imageUrl'] 	= base_url('uploads/repository/') . $folder .'/'. $response['data']['file_name'];
		} else {

			$response['status'] 	= 'error';
			$response['message'] 	= $upload['message'];
		}

		jsonContentType();
		echo json_encode( $response );

	}

	public function deleteRepositoryFile( $folder )
	{

		onlyAjax();
		$post = $this->input->post();
		$dir = FCPATH . 'uploads/repository/'. $folder .'/' . $post['file'];
		if( file_exists($dir)) {
			unlink( $dir );
		}

		echo 'ok';
	}

	public function getNotification()
	{

		$stream = new Stream();

		foreach (Stream::getHeaders() as $name => $value) {
		    header("$name: $value");
		}

		$dmData 	= [];
		$notifData 	= [];

		$cacheNameDm 	= "dm-" . $this->userData->id;
		$cacheEngine 	= $this->app_cache->getCacheItem( $cacheNameDm );
		$dmData 		= $cacheEngine->get();

		$cacheNameNotif 	= "notif-" . $this->userData->id;
		$cacheEngine 		= $this->app_cache->getCacheItem( $cacheNameNotif );
		$notifData 			= $cacheEngine->get();


		if ( ! empty( $dmData ) || ! empty( $notifData ) ) {

			$data['dm'] 	= $dmData;
			$data['notif']	= $notifData;

			$stream
		        ->event()
		        	->setEvent('notification')
		            ->setData( json_encode( $data ) )
		        ->end()
		        ->flush();

			$cacheEngine = $this->app_cache->deleteCache( $cacheNameDm );
			$cacheEngine = $this->app_cache->deleteCache( $cacheNameNotif );

		}

	}
}

/* End of file Api.php */
/* Location: ./application/controllers/Api.php */
