<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Login extends MY_Controller{

    function __construct(){

        parent::__construct();

    }

    public function index($msg = NULL){

        $this->_validateUser();

        // Load our view to be displayed
        // to the user
        $data['msg'] = $msg;
        $this->load->view('login_view', $data);

    }

    public function process(){

        onlyAjax();
        $post = $this->input->post();

        $identity = $post['username'];
        $password = $post['pwd'];
        $remember = TRUE; // remember the user
        $result   = array();

        $log = $this->ion_auth->login($identity, $password, $remember);
        if( $log ) {

            $user  = $this->ion_auth->user()->row();

            $result['name']         = $user->name;
            $result['status']       = 'ok';
            $result['redirectUrl']  = $this->_validateUser(true);
        } else {

            $this->ion_auth->set_error_delimiters('','<br>');

            $result['status'] = 'error';
            $result['message'] = $this->ion_auth->errors();

        }

        echo json_encode( $result );
    }

    public function logout()
    {

        $this->ion_auth->logout();
        redirect('login','refresh');

    }

    public function t( $userId = 25 )
    {

        $groups = $this->ion_auth->groups()->result_array();

        $options = [];
        foreach ($groups as $g) {
            $options[$g['id']] = ucfirst( $g['name'] ) ;
        }

        $defaultIdGroup = getUserRole( $userId, false);
        $data['dropdownGroup'] = form_dropdown('id_group', $options, $defaultIdGroup->id);

        echo $data['dropdownGroup'];
    }

}
?>
