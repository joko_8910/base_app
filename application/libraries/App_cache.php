<?php

use phpFastCache\CacheManager;

/**
*
*/
class App_cache
{

	protected $instanceCache;
    protected $cacheKey;
    protected $tableName = "";
    protected $cacheExpired     = 180; //in second
    protected $offset           = 0;

	function __construct()
	{

		// ---------------------------------------------
        // Setup PHPFastCache
        // ------------------------------------------------
        CacheManager::setDefaultConfig([
          "path" => APPPATH . 'cache/',
          "itemDetailedDate" => false
        ]);

        $this->instanceCache = CacheManager::getInstance('files');
        // --------------------------------------------------

	}

	public function setTable( $name )
	{
		$this->tableName = $name;
	}

	/**
	 * __get
	 *
	 * Enables the use of CI super-global without having to define an extra variable.
	 *
	 * I can't remember where I first saw this, so thank you if you are the original author. -Militis
	 *
	 * @access	public
	 * @param	$var
	 * @return	mixed
	 */
	public function __get($var)
	{

		return get_instance()->$var;
	}

	/**
     *
     * Check if chache of choosen key if exist or not
     * @param  string  $name cache key
     * @return boolean
     */
    public function isCachExist( $name )
    {

        $this->setCacheKey( $name );
        if( $this->instanceCache->hasItem( $this->cacheKey ) ) {
            return false;
        } else {
            return true;
        }

    }

    /**
     *
     * Update existed cache value
     * @param  string $name  cache key
     * @param  string $value  new value of the cache
     * @return boolean
     */
    public function updateCache( $name, $value )
    {

        $this->setCacheKey( $name );

        if( $this->isCachExist( $name ) ) {

            $cachedString = $this->instanceCache->getItem( $this->cacheKey );
            $cachedString->set( $value )->expiresAfter( $this->cacheExpired );
            $this->instanceCache->save($cachedString);

        }

        return true;
    }

    /**
     *
     * Delete existed cache
     * @param  string $name  cache key
     * @return boolean
     */
    public function deleteCache( $name = null )
    {

        $this->setCacheKey( $name );

        if( $this->instanceCache->hasItem( $this->cacheKey ) ) {
            return $this->instanceCache->deleteItem( $this->cacheKey );
        }

        return true;
    }

    public function getCacheItem( $cacheKey )
    {

        return $this->instanceCache->getItem( $cacheKey );
    }

    public function saveCache( $cachedString )
    {
	    return $this->instanceCache->save($cachedString);
    }

    /**
     *
     * Set the cache key
     * @param string $name name of cache key
     * @return string return name of setted name
     */
    public function setCacheKey( $name = null )
    {

        if( is_null( $name ) ) {
            $this->cacheKey = $this->tableName . $this->offset;
        } else {
            $this->cacheKey = $name;
        }

        return $this->cacheKey;
    }

}
