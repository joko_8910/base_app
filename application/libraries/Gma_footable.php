<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class App_footable
{

	protected $columns = [];
	protected $indexCol = 0;

	public function __construct()
	{

		$this->load->helper('url');
	}

	/**
	 * __get
	 *
	 * Enables the use of CI super-global without having to define an extra variable.
	 *
	 * I can't remember where I first saw this, so thank you if you are the original author. -Militis
	 *
	 * @access	public
	 * @param	$var
	 * @return	mixed
	 */
	public function __get($var)
	{
		return get_instance()->$var;
	}

	public function addColumn($title, $extra = array())
	{

		$this->columns[ $this->indexCol ]['title'] 		= $title;
		$this->columns[ $this->indexCol ]['name'] 		= url_title($title);
		$this->columns[ $this->indexCol ]['breakpoints'] = 'xs sm md';

		if( ! empty( $extra ) ) {

			foreach ($extra as $key => $value) {

				$this->columns[ $this->indexCol][$key] = $value;
			}
		}

		$this->indexCol += 1;

	}

	public function getColumns()
	{

		return $this->columns;
	}


}

/* End of file AppFootable.php */
/* Location: ./application/libraries/App_footable.php */
