function generateNumber( ele ) {

  var defaultQuantity = $(ele).closest('tr').find('td.req-purchase');
  
  swal.setDefaults({
    input: 'text',
    confirmButtonText: 'Next <i class="fa fa-chevron-circle-right"></i>',
    showCancelButton: true,
    animation: false,
    progressSteps: ['1', '2']

  })

  var steps = [
    {
      title: 'Input the Quantity',
      text: 'Input the Request Quantity to Purchased',
      inputValue: ''
    },
    {
      title: 'Input Price',
      text: 'Input price to Buy that Quantity',
      inputValue: ''
    },
  ]


  swal.queue(steps).then(function ( result ) {
    
    swal.resetDefaults()

    defaultQuantity.text( result[0] );
    // defaultIssue.text( result[1] );

      swal(
        'Nomor PO Berhasil di Generate',
        'simpan nomor berikut: 145/GMA-FUS/234',
        'success'
      )
  }, function () {
    swal.resetDefaults()
  })

    var button = $(ele);

    button.addClass('hide');
    button.parent().find('.get-po-number').removeClass('hide');
    button.parent().find('.edit-po').removeClass('hide');
    button.closest('tr').find('td.status-purchase').html('<span class="label bg-orange">Requesting Approval</span>');

    return false;
}

function getNumber() {
  swal(
    'Berikut Nomor PO',
    '145/GMA-FUS/242',
    'success'
  )
  return false;
}

function finalizeProject(ele) {
  swal(
    'Finalize Project',
    'Success'
    )

  var button = $(ele);

  button.addClass('hide');
  button.parent().find('.get-po-number').removeClass('hide');
  button.parent().find('.edit-po').removeClass('hide');
  button.closest('tr')
}

function undisableBtnBast() {
    document.getElementById("bast").disabled = false;
}




function editOrder( ele ) {

  var button = $(ele);

  var defaultIssue    = button.closest('tr').find('td.issue');
  var defaultQuantity = button.closest('tr').find('td.quantity');

  swal.setDefaults({
    input: 'text',
    confirmButtonText: 'Next <i class="fa fa-chevron-circle-right"></i>',
    showCancelButton: true,
    animation: false,
    progressSteps: ['1', '2']
  })

  var steps = [
    {
      title: 'How many has Purchased',
      text: 'Insert quantity of Purchased Item',
      inputValue: defaultQuantity.text()
    },
    {
      title: 'Is there any Issue occured?',
      text: 'If there any issue, type the issue. If not leave it empty',
      inputValue: defaultIssue.text()
    },
  ]

  swal.queue(steps).then(function ( result ) {
    
    swal.resetDefaults()

    defaultQuantity.text( result[0] );
    defaultIssue.text( result[1] );

    swal({
      title: 'All done!',
      text: 'Data has been saved',
      type: 'success'
    })
  }, function () {
    swal.resetDefaults()
  })

  return false;
}

function approvePo( ele ) {

  swal({
    title: 'Purchase Order Approved',
    text: 'You has been approved a purchase order. Dont forget to make a signature into the the hard copy',
    type: 'success'
  })

  return false;
}

function rejectPo( ele ) {

  swal({
    title: 'Reject Purchase Order',
    text: 'You are about reject a purchase order. Please Add an additional note',
    input: 'textarea',
    showCancelButton: true,
    confirmButtonText: 'Submit',
    showLoaderOnConfirm: true,
    preConfirm: function (email) {
      return new Promise(function (resolve, reject) {
        setTimeout(function() {
          if (email === 'taken@example.com') {
            reject('This email is already taken.')
          } else {
            resolve()
          }
        }, 1000)
      })
    },
    allowOutsideClick: false
  }).then(function (email) {
    swal({
      type: 'success',
      title: 'Data has been saved'
    })
  })

  return false;
}

function rejectProject( ele ) {
  swal({
    title: 'Reject Project',
    text: 'You are about reject a project. Please Add an additional note',
    input: 'textarea',
    showCancelButton: true,
    confirmButtonText: 'Submit',
    showLoaderOnConfirm: true,
    preConfirm: function (email) {
      return new Promise(function (resolve, reject) {
        setTimeout(function() {
          if (email === 'taken@example.com') {
            reject('This email is already taken.')
          } else {
            resolve()
          }
        }, 1000)
      })
    },
    allowOutsideClick: false
  }).then(function (email) {
    swal({
      type: 'success',
      title: 'Data has been saved'
    })
  })

  return false;
}

  
$(document).ready(function() {
    $('.datepicker').datepicker();
    $('.js-example-basic-single').select2();

  //Initialize Select2 Elements
    $(".select2").select2();
 // auto suggestion 
  $('#magicsuggest').magicSuggest({
    data: ['Kabel Udara ADSS span 100 12 core','  Joint closure dome 12 core','Tiang']
  });

   $('#magicsuggest2').magicSuggest({
    data: ['PT ABC <i class="fa fa-star-o"></i> 4.0','PT Fiberstar <i class="fa fa-star-o"></i> 4.5','PT CBN <i class="fa fa-star-o"></i> 4.4']
  });

  /* ION SLIDER */
  $("#range_1").ionRangeSlider({
    min: 0,
    max: 5000,
    from: 1000,
    to: 4000,
    type: 'double',
    step: 1,
    prefix: "$",
    prettify: false,
    hasGrid: true
  });
  //start vendor
  $('#ratingvendor').barrating({
      theme: 'fontawesome-stars'
    });
   //start vendor
  $('#ratingvendor2').barrating({
      theme: 'fontawesome-stars'
    });
   //start vendor
  $('#ratingvendor3').barrating({
      theme: 'fontawesome-stars'
    });
  $('#ratingvendor4').barrating({
      theme: 'fontawesome-stars'
    });
  $("#range_2").ionRangeSlider({});
  $("#range_5").ionRangeSlider({
    min: 0,
    max: 10,
    type: 'single',
    step: 0.1,
    postfix: " mm",
    prettify: false,
    hasGrid: true
  });
  $("#range_6").ionRangeSlider({
    min: -50,
    max: 50,
    from: 0,
    type: 'single',
    step: 1,
    postfix: "°",
    prettify: false,
    hasGrid: true
  });
  $("#range_4").ionRangeSlider({
    type: "single",
    step: 100,
    postfix: " light years",
    from: 55000,
    hideMinMax: true,
    hideFromTo: false
  });
  $("#range_3").ionRangeSlider({
    type: "double",
    postfix: " miles",
    step: 10000,
    from: 25000000,
    to: 35000000,
    onChange: function (obj) {
      var t = "";
      for (var prop in obj) {
        t += prop + ": " + obj[prop] + "\r\n";
      }
      $("#result").html(t);
    },
    onLoad: function (obj) {
      //
    }
  });
});
// Bullet charts
    $('.sparkbullet').sparkline('html', {type: 'bullet'});
 
 /*
     * DONUT CHART
     * -----------
     */

    // var donutData = [
    //   {label: "Material", data: 30, color: "#3c8dbc"},
    //   {label: "Pengerjaan", data: 20, color: "#0073b7"},
    //   {label: "Perizinan", data: 50, color: "#00c0ef"}
    // ];
    // $.plot("#donut-chart", donutData, {
    //   series: {
    //     pie: {
    //       show: true,
    //       radius: 1,
    //       innerRadius: 0.5,
    //       label: {
    //         show: true,
    //         radius: 2 / 3,
    //         formatter: labelFormatter,
    //         threshold: 0.1
    //       }

    //     }
    //   },
    //   legend: {
    //     show: false
    //   }
    // });
    /*
     * END DONUT CHART
     */
     /*

  
 
   * Custom Label formatter
   * ----------------------
   */
  function labelFormatter(label, series) {
    return '<div style="font-size:11px; text-align:center; padding:2px; color: #fff; font-weight: 600;">'
        + label
        + "<br>"
        + Math.round(series.percent) + "%</div>";
  }
var tableIssue = $('#tableIssue').DataTable({
        "processing": true,
        "serverSide": true,
        "ordering": false,
        "ajax": $('#tableIssue').attr('data-url'),
        "oLanguage": {
            "sSearch": "Search Project: "
        }
    });

function filterIssues( ele ) {

  var dropdownFilterClient = JSON.parse($('.filter-options .vi-dropdown-client').text()),
    dropdownFilterType = JSON.parse($('.filter-options .vi-dropdown-type').text()),
    dropdownFilterPriority = JSON.parse($('.filter-options .vi-dropdown-priority').text());

  swal.withForm({
    title: 'Filter Issues Setup',
    // text: 'Any text that you consider useful for the form',
    showCancelButton: true,
    confirmButtonColor: '#DD6B55',
    confirmButtonText: 'Save Setup',
    closeOnConfirm: false,
    formFields: [
        { type: 'select', name: 'filterClient', options: dropdownFilterClient},
        { type: 'select', name: 'filterType', options: dropdownFilterType},
        { type: 'select', name: 'filterPriority', options: dropdownFilterPriority}
    ]
  }, function(isConfirm) {

    if( isConfirm ) {

            swal("System now saving your setup", "Please wait a while...");

            var dataForm = this.swalForm;
            $.ajax({
                type: 'POST',
                url: $(ele).attr('data-url'),
                data: {
                    'filterIssue' : true,
                    'filterData' : dataForm
                },
                success: function ( r ) {

                    if( r == 'ok' ) {
                        tableIssue.ajax.reload();
                        swal.close();
                    } else {
                        swal("Server Problem", "Sorry, System meet unkown problem. Please setup again.", "error")
                    }
                }
            });
      
    }

  })

  return false;
}

// -----------------------------------------------------
  // Ketika user klik pada tombol edit, maka semua input di 1 row dengan tombol tersebut
  // berubah dari readonly menjadi editable
  // 
  // @page => admin/pembayaran/detail
  // -----------------------------------------------------
  $(document).on( 'click', '#formPembayaran .edit-pembayaran', function ( e ) {
    
    var parentEle = $(this).parent().parent();

    parentEle.find('td input').removeAttr('readonly');
    parentEle.find('td.hari-denda input').focus();

    $(this).addClass('hide');
    $(this).next().removeClass('hide');

    e.preventDefault();
  });

  // -----------------------------------------------------
  // Function ketika user klik tombol "done", maka semua input di 1 row dengan tombol tersebut
  // berubah menjadi readonly
  // 
  // @page => admin/pembayaran/detail
  // -----------------------------------------------------
  $(document).on( 'click', '#formPembayaran .selesai-pembayaran', function ( e ) {
    
    var parentEle = $(this).parent().parent();

    parentEle.find('td input').attr('readonly','readonly');

    $(this).addClass('hide');
    $(this).prev().removeClass('hide');

    e.preventDefault();
  });
