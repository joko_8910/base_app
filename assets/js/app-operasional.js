$(document).ready( function () {

	initDataTable('.datatable', {
		'columns' : [ { "width": "1%" }, null, null, null, null, null,
		    	{ 
		    		'className': 'text-center',
		    	},
		    	{ 
		    		'width': '32%',
		    		'className': 'text-center' ,
		    	}]
	});


	// -----------------------------------------------------
	// Initiate select2 for finding pic name
	// -----------------------------------------------------
	initSelect2Ajax('.select--pic');

	// -----------------------------------------------------
	// Initiate select2 for finding provinsi/area in create project
	// -----------------------------------------------------	
	initSelect2Ajax('.select--provinsi', function () {

		$('.select--provinsi').on("select2:selecting", function(e) { 

			var result 		= e.params.args.data;
			var idProvinsi 	= result.id;

			$('.field--generate--project--code').val('Generating Project Code...');

			$.ajax({
				type: 'POST',
				url: urlGenerateProjectCode,
				data: {
					'id_provinsi': idProvinsi
				},
				success: function ( r ) {

					$('.field--generate--project--code').val( r );

				}
			});

			
		});
	});



	// -----------------------------------------------------
	// Initiate select2 for finding material in edit boq
	// -----------------------------------------------------
	initSelect2Ajax('.cari--material', function () {
		
		$('.cari--material').on("select2:selecting", function(e) { 

			var result 		= e.params.args.data;

			$('input[name="master--code"]').val( result.data.id );
			$('input[name="master--unit"]').val( result.data.unit );

			localStorage.setItem('boqMaterial', JSON.stringify(result.data));

		});

	});

	// -----------------------------------------------------
	// Initiate select2 for finding services in edit boq
	// -----------------------------------------------------
	initSelect2Ajax('.cari--service', function () {
		
		$('.cari--service').on("select2:selecting", function(e) { 

			var result 		= e.params.args.data;

			$('input[name="s--master--code"]').val( result.data.id );

			localStorage.setItem('boqServices', JSON.stringify(result.data));

		});

	});
	
	window.createProject = function createProject( form ) {

		showLoading('Creating Project...');

		$.ajax({
			type: 'POST',
			url: $(form).attr('action'),
			data: $(form).serializeArray(),
			dataType: 'json',
			success: function ( r ) {

				if( r.status == 'ok' ) {

					showSuccess('Project has been created. System will redirecting you...');

					setTimeout( function () {
						window.location = r.redirectUrl;
					}, 2000);

				} else {

					swal(
					  'Ahh.. We facing error problem',
					  r.message,
					  'error'
					)

				}
				
			}

		});

		return false;
    }

    // -----------------------------------------------------
    	// Table rejectedproject
    // -----------------------------------------------
    initDataTable('.ops--reject--project', {
		'columns' : [ { "width": "1%" }, null, null,
				{ 
		    		'className': 'text-center' 
		    	},
		    	{ 
		    		'className': 'text-center' 
		    	},
		    	{ 
		    		'className': 'text-center' 
		    	},
		    	{ 
		    		'className': 'text-center' 
		    	}]
	});

    // -----------------------------------------------------
	// Function for add material to boq
	// used in page: operasional/boq
	// -----------------------------------------------------
    window.opsTambahMaterial = function opsTambahMaterial (e) {
    	
    	var code 	= $('input[name="master--code"]');
    	var qty  	= $('input[name="master--qty"]');
    	var unit  	= $('input[name="master--unit"]');
    	var name  	= $('select[name="master--name"]');

    	var projectCode = $('input[name="pcode"]').val();

    	// make sure user has been selected any avaiable material
    	if( code.val() == '' || qty.val() == '' || unit.val() == '' ) {

    		showError('You forget input the quantity or select the material');

    		return false;
    	}

    	var table = $('table#table-bom');
    	var data = JSON.parse( localStorage.getItem('boqMaterial') );
    	var Html = "";

    	// check whether in table already have the selected id or not
    	// this prevent duplicate material id/name/item
    	var check = table.find('tr.m-' + data.id );
    	if( check.length > 0 ) {
    		swal(
			  'Oops...',
			  'You already input this Material.',
			  'error'
			)

			return false;
    	}

    	// so, there we have make sure there is no duplicate data
    	// lets build the html for append into the table
    	Html += '<tr class="m-'+ data.id +'" data-delete="'+ROLEURL+'/project/deleteBoqItem/material/'+data.id+'/'+ projectCode +'">';
    	Html += '<td><input type="text" class="form-control" name="m-code[]" value="'+ data.id +'" /></td>';
    	Html += '<td><input type="text" class="form-control" name="m-name['+ data.id +']" value="'+ data.name +'" /></td>';
    	Html += '<td><input type="text" class="form-control" name="m-unit['+ data.id +']" value="'+ data.unit +'" /></td>';
    	Html += '<td><input type="text" class="form-control" name="m-qty['+ data.id +']" value="'+ qty.val() +'" required /></td>';
    	Html += '<td><button onclick="return opsRemoveBoqItem(this)" class="btn bg-red btn-sm"><i class="fa fa-trash-o"></i></button></td>'
    	Html += '</tr>';

    	table.find('tbody').append( Html );

    	// reset all field and localstorage
    	code.val('');
    	unit.val('');
    	qty.val('');
		$('.cari--material').val(null).trigger("change");
		localStorage.setItem('boqMaterial', null);

    	return false;

    }

    // -----------------------------------------------------
	// Function for add service to boq
	// used in page: operasional/boq
	// -----------------------------------------------------
    window.opsTambahService = function opsTambahService (e) {
    	
    	var code 	= $('input[name="s--master--code"]');
    	var qty  	= $('input[name="s--master--qty"]');
    	var name  	= $('select[name="s--master--name"]');

    	var projectCode = $('input[name="pcode"]').val();

    	// make sure user has been selected any avaiable material
    	if( code.val() == '' || qty.val() == '' ) {

    		showError('You forget input the quantity or select the service');
    		return false;
    	}

    	var table = $('table#table-bos');
    	var data = JSON.parse( localStorage.getItem('boqServices') );
    	var Html = "";

    	// check whether in table already have the selected id or not
    	// this prevent duplicate material id/name/item
    	var check = table.find('tr.s-' + data.id );
    	if( check.length > 0 ) {
    		swal(
			  'Oops...',
			  'You already input this Services.',
			  'error'
			)

			return false;
    	}

    	// so, there we have make sure there is no duplicate data
    	// lets build the html for append into the table
    	Html += '<tr class="s-'+ data.id +'" data-delete="'+ROLEURL+'/project/deleteBoqItem/service/'+data.id+'/'+ projectCode +'">';
    	Html += '<td><input type="text" class="form-control" name="s-code[]" value="'+ data.id +'" /></td>';
    	Html += '<td><input type="text" class="form-control" name="s-name['+ data.id +']" value="'+ data.name +'" /></td>';
    	Html += '<td><input type="text" class="form-control" name="s-qty['+ data.id +']" value="'+ qty.val() +'" required /></td>';
    	Html += '<td><button onclick="return opsRemoveBoqItem(this)" class="btn bg-red btn-sm"><i class="fa fa-trash-o"></i></button></td>'
    	Html += '</tr>';

    	table.find('tbody').append( Html );

    	// reset all field and localstorage
    	code.val('');
    	qty.val('');
		$('.cari--service').val(null).trigger("change");
		localStorage.setItem('boqServices', null);

    	return false;

    }

    // -----------------------------------------------------
	// Delete Boq Item
	// used in page: operasional/boq
	// -----------------------------------------------------
    window.opsRemoveBoqItem = function opsRemoveBoqItem(e) {
    	
    	var ele = $(e);

    	showSweetAlert( ele, function () {

    		showLoading('Deleting Item...');

	    	$.ajax({
	    		type: 'GET',
	    		url: ele.closest('tr').attr('data-delete'),
	    		success: function (r) {
	    			
	    			if( r=='ok' ) {
				    	ele.closest('tr').remove();
				    	swal.close();
	    			} else {
						showError('Error on deleting item. Please refresh page and try again');
	    			}
	    		}
	    	});
    	
    	});

    	return false;
    }

    window.afterDeleteMap = function afterDeleteMap() {

    	var button = $(arguments[0]);
    	var placeholder = button.closest('.gma-upload-square-placeholder');

    	placeholder.find('input[name="map_url"]').val('');
    	button.parent().html('');
    	placeholder.removeClass('done');
    	placeholder.find('.gma-item').show();

    }

    gmaAjaxFileUpload('.ops--upload--map', function (ele) {
    	
    	data = arguments[1];

    	var inputField 	=  $(this)[0].getAttribute('data-target');
    	var deleteUrl 	=  $(this)[0].getAttribute('data-delete-url');

    	$('.gma-upload-square-placeholder').find('.gma-item').hide();
    	$('.gma-upload-square-placeholder').addClass('done');

    	var Html = '<a href="#" class="remove-image btn bg-red" onclick="return ajaxDeleteImage(this, \''+deleteUrl+'\', \''+ data.data.file_name +'\',afterDeleteMap)"><i class="fa fa-trash-o"></i></a>';
    	Html += '<img src="'+ data.imageUrl +'" alt="upload" />';

    	$('.gma-upload-square-placeholder .upload-result').html( Html );

    	$('.gma-upload-square-placeholder').find('input[name="map_url"]').val(data.data.file_name);

    });

    window.validateEditBoq = function validateEditBoq( form ) {
    	
    	// check if user has input material
    	var checkBom = $('table#table-bom').find('tbody input');
    	if( checkBom.length <= 0 ) {

    		showError('No Material Selected. Please insert atleast one material.');
	    	return false;

    	}

    	// check if user has input services
    	var checkBos = $('table#table-bos').find('tbody input');
    	if( checkBos.length <= 0 ) {

    		showError('No Service Selected. Please insert atleast one service.');
	    	return false;
    	}

    	preloaderPage();


    }

    $('#issues-project').dataTable({
    	"processing": true,
		"serverSide": true,
        "order": [],	
		"ajax": {
			'url' : $('#issues-project').attr('data-url'),
			'type': 'POST'
		},
		//Set column definition initialisation properties.
        'columnDefs': [
	        { 
	            'targets': [ 0 ], //first column / numbering column
	            'orderable': true, //set not orderable
	        },
        ],
	});

    var issuesProjectTable = $('#issues-project').dataTable();
 
    // $('#issues-project tbody').on( 'click', 'tr', function () {
    //     if ( $(this).hasClass('selected') ) {
    //         $(this).removeClass('selected');
    //     }
    //     else {
    //         issuesProjectTable.$('tr.selected').removeClass('selected');
    //         $(this).addClass('selected');
    //     }
    // } );
 
 	// select row and do something
    // $('#button').click( function () {
    //     issuesProjectTable.row('.selected').remove().draw( false );
	// } );

	// outside document ready
	window.openDetail = function openDetail( elem ) {
		var $id 			= $( elem ).attr('data-id');
		var $username  		= $( elem ).attr('data-username');
		var $userid  		= $( elem ).attr('data-userid');
		var $login  		= $( elem ).attr('data-login');
		var $type  			= $( elem ).attr('data-type');
		var $name 			= $( elem ).attr('data-name');
		var $title 			= $( elem ).attr('data-title');
		var $priority 		= $( elem ).attr('data-priority');
		var $status 		= $( elem ).attr('data-status');
		var $description 	= $( elem ).attr('data-description');
		var $image  		= $( elem ).attr('data-image');
		var $created  		= $( elem ).attr('data-created');
		var $modified  		= $( elem ).attr('data-modified');
		var $html 	= '<a href="#" class="close-issue" onclick="return closeIssueSeeDetail()"><i class="fa fa-times-circle-o"></i></a>';
		$html 	   += '<div class="issue-detail-header">';
		$html 	   += '<div class="pull-left">';
		$html 	   += '<h3>'+ $name +'</h3>';
		$html 	   += '<p>'+ $title +'</p></div>';

		if ($login === $userid) {
			$html 	   += '<div class="pull-right">';
			if ( $status == 'Solved' ) {
				$html 	   += '<div style="position: relative; z-index: 6;"><div style="padding-left:19px" class="label bg-aqua label-lg flat pointer" id="currentStatus" onclick="currentStatus( this )">'+ $status +' <span style="width:40px" class="fa fa-caret-down"></span></div>';
			}else{
				$html 	   += '<div style="position: relative; z-index: 6;"><div style="padding-left:16px" class="label bg-aqua label-lg flat pointer" id="currentStatus" onclick="currentStatus( this )">'+ $status +' <span style="width:8px" class="fa fa-caret-down"></span></div>';
			}
			$html 	   += '<div class="label bg-yellow-active label-lg flat pointer" onclick="deleteIssues( this )" data-unique="'+ $id +'"><span class="fa fa-trash-o"></span></div></div>';
			$html 	   += '<div class="label bg-aqua label-lg flat issues-detail-status-list disappear"> <ul class="list-unstyled"><li onclick="chooseStatusOne( this )" data-username="'+$username+'" data-unique="'+ $id +'" class="pointer">On Progress</li>';
			$html 	   += '<li onclick="chooseStatusTwo( this )" data-username="'+$username+'" data-unique="'+ $id +'" class="pointer">Solved</li></ul></div></div>';
		// console.log($status);
		$html 	   += '<div class="pull-right">';
		if ( $status == 'Solved' ) {
			$html 	   += '<div style="position: relative; z-index: 6;"><div style="padding-left:19px" class="label bg-aqua label-lg flat pointer" id="currentStatus" onclick="currentStatus( this )">'+ $status +' <span style="width:40px" class="fa fa-caret-down"></span></div>';
		}else{
			$html 	   += '<div class="pull-right">';
			if ( $status == 'Solved' ) {
				$html 	   += '<div style="position: relative; z-index: 6;"><div style="padding-left:19px" class="label bg-aqua label-lg flat pointer" id="currentStatus" >'+ $status +' </div>';
			}else{
				$html 	   += '<div style="position: relative; z-index: 6;"><div style="padding-left:16px" class="label bg-aqua label-lg flat pointer" id="currentStatus" >'+ $status +' </div>';
			}
			$html 	   += '</div></div>';
		}

		
		$html 	   += '<div class="clearfix"></div><div class="col-md-12 no-padding">';
		$html 	   += '<div class="label bg-aqua label-lg flat" style="margin-right:20px">Type : '+ $type +'</div>';
		$html 	   += '<div class="label bg-yellow-active label-lg flat">Priority : '+ $priority +'</div></div>';
		$html 	   += '</div>';
		$html 	   += '<div class="issue-detail-body">';
		$html 	   += '<div class="no-filter box box-solid">';
		$html 	   += '<div class="box-body">';
		$html 	   += '<div class="post">';
		$html 	   += '<div class="user-block">';
		$html 	   += '<img class="img-circle img-bordered-sm" src="'+ $image +'" alt="user image">';
		$html 	   += '<span class="username">';
		$html 	   += '<a href="#">'+ $username +'</a></span>';
		$html 	   += '<span class="description">'+ $created +'</span></div>';
		$html 	   += '<p>'+ $description +'</p>';
		$html 	   += '<p id="modified">Last Updated by '+$username+' on '+$modified+'</p></div></div></div>';
		
		$('#sideDetail').html($html);

		// console.log(elem);
		// console.log($id+'\n'+$username+'\n'+$type+'\n'+$name+'\n'+$title+'\n'+$priority+'\n'+$status+'\n'+$description+'\n'+$image+'\n'+$created);
	    
	    if( $('.issue-detail-wrapper').hasClass('open') ) {
	        $('.issue-detail-wrapper').removeClass('open');
	        $('.vi-overlay').removeClass('in');
	    } else {
	        $('.issue-detail-wrapper').addClass('open');
	        $('.vi-overlay').addClass('in');
	    }
	}

	window.closeIssueSeeDetail = function closeIssueSeeDetail( ele ) {
	    
	    if( $('.issue-detail-wrapper').hasClass('open') ) {

	        $('.issue-detail-wrapper').removeClass('open');
	        $('.vi-overlay').removeClass('in');

	    } else {
	        
	        $('.issue-detail-wrapper').addClass('open');
	        $('.vi-overlay').addClass('in');

	    }

	    return false;
	}

	window.currentStatus = function currentStatus( ele ) {
	    
	    if( $( '.issues-detail-status-list' ).hasClass('disappear') ) {

	        $( '.issues-detail-status-list' ).removeClass('disappear');
	        $( '.issues-detail-status-list' ).addClass('appear');
	        $( 'li.pointer' ).css( "color", "white" );

	    } else {

	        $( '.issues-detail-status-list' ).removeClass('appear');
	        $( 'li.pointer' ).css( "color", "transparent" );
	        $( '.issues-detail-status-list' ).addClass('disappear');
	    }
	    return false;
	}

	window.chooseStatusOne = function chooseStatusOne( elem ) {
		var $id 			= $( elem ).attr('data-unique');
		var $username 			= $( elem ).attr('data-username');
	    $( '#currentStatus' ).html( 'On Progress <span class="fa fa-caret-down"></span>' ).css( 'padding-left', '16px' );
	    $( '#modified' ).html( 'Last Updated by '+$username+' on '+dateNow() );
		$.ajax({
		  url: "statusIssue?s=on progress&id="+$id
		}).done(function() {
		  issuesProjectTable.api().ajax.reload( null, false );
		});
	}

	window.chooseStatusTwo = function chooseStatusTwo( elem ) {
		var $id 			= $( elem ).attr('data-unique');
		var $username 			= $( elem ).attr('data-username');
	    $( '#currentStatus' ).html( 'Solved <span style="width:40px" class="fa fa-caret-down"></span>' ).css( 'padding-left', '19px' );
	    $( '#modified' ).html( 'Last Updated by '+$username+' on '+dateNow() );
		$.ajax({
		  url: "statusIssue?s=solved&id="+$id
		}).done(function() { 
		  issuesProjectTable.api().ajax.reload( null, false );
		});
	}

	window.deleteIssues = function deleteIssues( elem ) {
		showSweetAlert( elem, function(){
			var $id 			= $( elem ).attr('data-unique');
			showLoading('Deleting Item...');
			$.ajax({
			  	url: "deleteIssueData?id="+$id
			}).done(function() { 
				swal.close();
			  	issuesProjectTable.api().ajax.reload( null, false );
		        $('.issue-detail-wrapper').removeClass('open');
		        $('.vi-overlay').removeClass('in');
			});
		});

		return false;
	}

    window.afterDeleteFile = function afterDeleteFile() {
    	var button = $(arguments[0]);
    	var filename = arguments[2];
    	var placeholder = button.closest('.gma-upload-square-placeholder');

    	button.parent().html('');
    	placeholder.find('#filename'+filename).remove();
    	placeholder.find('#name'+filename).remove();
    	placeholder.find('#size'+filename).remove();

    }

	window.setDefaultProject = function setDefaultProject( ele ) {

		showLoading('Setting Active Project...');

		$.ajax({
			type: 'POST',
			url: $(ele).attr('href'),
			success: function ( r ) {

				if( r == 'ok' ) {

					swal(
					  'Success',
					  'The Project has been set active',
					  'success'
					).then(function () {

						preloaderPage();
						window.location = ROLEURL + 'project/implementation';
					})
					
				} else {
					showError('We have server problem. Please try again.');
				}

			}
		});

		return false;

	}

	gmaAjaxFileUpload('.ops--upload--repo', function (ele) {
    	data = arguments[1];
    	var deleteUrl 	=  $(this)[0].getAttribute('data-delete-url');
    	$('#repo_category_image').show();
    	var Html = '<li class="list-unstyled margin-20" style="box-shadow: 0px 0px 3px grey;"><img class="pull-left" src="'+ data.imageUrl +'" alt="'+ data.data.file_name +'" style="width:30%" />';
    	Html += '<a href="#" class="remove-image btn bg-red pull-right flat" onclick="return ajaxDeleteFile(this, \''+deleteUrl+'\', \''+ data.data.file_name +'\', \''+ data.data.raw_name +'\',afterDeleteFile)"><i class="fa fa-trash-o"></i></a><div class="clearfix"></div></li>';

    	$('.gma-upload-square-placeholder').append(
		    $('<input>', {
		        type: 'hidden',
		        id: 'filename' + data.data.raw_name,
		        name: 'repo_filename[]',
		        val: data.data.file_name
		    })
		);

    	$('.gma-upload-square-placeholder').append(
		    $('<input>', {
		        type: 'hidden',
		        id: 'name' + data.data.raw_name,
		        name: 'repo_name[]',
		        val: data.data.client_name
		    })
		);

    	$('.gma-upload-square-placeholder').append(
		    $('<input>', {
		        type: 'hidden',
		        id: 'size' + data.data.raw_name,
		        name: 'repo_size[]',
		        val: data.data.file_size
		    })
		);

    	$('.gma-upload-square-placeholder .list-upload').append(Html);
    });
    $('#datatable').dataTable({
    	"processing": true,
		"serverSide": true,
        "order": [],	
		"ajax": {
			'url' : $('#datatable').attr('data-url'),
			'type': 'POST'
		},
		//Set column definition initialisation properties.
        'columnDefs': [
	        { 
	            'targets': [ 0 ], //first column / numbering column
	            'orderable': true, //set not orderable
	        },
        ],
	});

	var thisDataTable = $('#datatable').dataTable();

    window.downloadRepository = function downloadRepository( elem ) {
	    
		var $url 			 = $( elem ).attr('data-url');
		window.location.href = $url;

	}

    window.updateRepository = function updateRepository( elem ) {
	    
		var $url 			= $( elem ).attr('data-url');
		showLoading('Updating your data...');
		$.ajax({
		  	url: $url,
		}).done(function() { 
		  	thisDataTable.api().ajax.reload( null, false );
			swal.close();
		});

	}

    window.deleteRepository = function deleteRepository( elem ) {
	    
		var $url 			= $( elem ).attr('data-url');

		showSweetAlert( elem, function(){
			showLoading('Deleting your data...');
			$.ajax({
			  	url: $url,
			}).done(function() { 
			  	thisDataTable.api().ajax.reload( null, false );
				swal.close();
			});
		});
		
		return false;
	}

	$('#updateRepo').on('show.bs.modal', function (event) {
	  var button = $(event.relatedTarget) // Button that triggered the modal
	  var $filename = button.data('filename') // Extract info from data-* attributes
	  var $url = button.data('url') // Extract info from data-* attributes
	  // If necessary, you could initiate an AJAX request here (and then do the updating in a callback).
	  // Update the modal's content. We'll use jQuery here, but you could use a data binding library or other methods instead.
	  var modal = $(this)
	  modal.find('.modal-title').text('Update category for ' + $filename)
	  modal.find('#filename').val($filename)
	  modal.find('form').attr('action',$url)
	})

	$('#updateR').on('click', function (e){
		$url = $('#formupdate').attr('action');
		showLoading('Updating your data...');
		$.ajax({
		  	url: $url,
		  	data: $('#formupdate').serialize()
		}).done(function() { 
			$('#updateRepo').modal('hide');
		  	thisDataTable.api().ajax.reload( null, false );
			swal.close();
		});
	});

    window.downloadAllRepo = function downloadAllRepo( elem ) {
	    
		var $url 			 = $( elem ).attr('data-url');
		window.location.href = $url;

	}
	function implementationSearchVendor() {
		
		var submitIcon 	= $('.searchbox-icon');
	    var inputBox 	= $('.searchbox-input');
	    var searchBox 	= $('.searchbox');
	    var isOpen 		= false;

	    submitIcon.click( function(){

	        if(isOpen == false){
	            searchBox.addClass('searchbox-open');
	            inputBox.focus();
	            isOpen = true;
	        } else {
	            searchBox.removeClass('searchbox-open');
	            inputBox.focusout();
	            isOpen = false;
	        }

	    });  

	    submitIcon.mouseup( function(){
	        return false;
	    });

	    searchBox.mouseup( function(){
            return false;
        });

	    $(document).mouseup( function(){

            if(isOpen == true){

                $('.searchbox-icon').css('display','block');
                submitIcon.click();
            }

        });
	    
	    window.buttonUp = function buttonUp( ele ){

		    var inputVal 	= $('.searchbox-input').val();
		    inputVal 		= $.trim(inputVal).length;

		    $(ele).focus();

		    if( inputVal !== 0){

		        $('.searchbox-icon').css('display','none');
		    } else {

		        $('.searchbox-input').val('');
		        $('.searchbox-icon').css('display','block');
		    }

		}

	} implementationSearchVendor();

	// -----------------------------------------------------
	// Initiate select2 for Contractor
	// -----------------------------------------------------	
	function showRatingBar( score ) {
		
		var rating = '';
	    for (var i = 0; i < 5; i++) {

	    	if( i < score ) {
	    		rating += '<span class="fa fa-star star-rating-checked"></span> ';
	    	} else {
		    	rating += '<span class="fa fa-star-o"></span> ';
	    	}
	    }

	    return rating;
	}

	initSelect2Ajax('.select--contractor', function () {
		
		$('.select--contractor').on("select2:selecting", function(e) { 

		});

	}, {
		escapeMarkup: function (markup) { return markup; },
		templateResult: function ( state ) {
			
			if (state.loading) {
				return 'Searching contractor...';
			}

		    var rating = showRatingBar( state.data.performance_score );
		    var Html = '';
		    Html = '<div class="select-contractor-wrap">';
		   	Html += '<div class="select-icon"><i class="fa fa-building"></i></div>';
		   	Html += '<div class="select-text-wrap">';
		   	Html += '<div class="select-text">'+ state.text +'</div>';
		   	Html += '<div class="select-meta">';
		   	Html += 'Type: '+ ucFirst( state.data.type ) + ' <span class="span select-sepator"></span>';
		   	Html += 'Rating: <span class="gma-rating-bar">'+ rating  +'</span>';
		   	Html += '</div>';
		   	Html += '</div></div>';

		   	return Html;

		},
		templateSelection: function ( state ) {

		    if( state.id == "" ) {
		    	return state.text;
		    }

		    var rating = showRatingBar( state.data.performance_score );
		    
			var $state = $( '<span class="gma-rating-bar"><span class="text">' + state.text  + '</span> ' + rating + '</span>');
			return $state;

		},

	});

	// -----------------------------------------------------
	// Initiate select2 for Contractor
	// -----------------------------------------------------	
	initSelect2('.select--bos--item', function () {

		$('.select--bos--item').on("select2:selecting", function(e) { 

			
		});
	});

	initDataTable('.ops--ongoing--project', {
		'columns' : [ { "width": "1%" }, null, null,
				{ 
		    		'className': 'text-center' 
		    	},
		    	{ 
		    		'className': 'text-center' 
		    	},
		    	{ 
		    		'className': 'text-center' 
		    	},
		    	{ 
		    		'className': 'text-center' 
		    	},
		    	{ 
		    		'className': 'text-center' 
		    	}]
	});

	initSelect2('.select--issue--task');

	window.implementationAddBosItem = function implementationAddBosItem( ele ) {
		
		var parent = $(ele).closest('tr');
		var table = $(ele).closest('table');
		
		var masterItemIdRaw 	= $('select[name="master-item-name"]').val();
		var masterItemIdArray 	= masterItemIdRaw.split('--');

		var masterItemId 	= parseInt(masterItemIdArray[0]);
		var masterItemIdQty = parseInt(masterItemIdArray[1]);

		var masterItemName 	= parent.find('select[name="master-item-name"] option[value="'+ masterItemIdRaw +'"]').text();
		var masterPlanStart = parent.find('input[name="master-item-start"]');
		var masterPlanEnd 	= parent.find('input[name="master-item-end"]');
		var masterPlanQty 	= parent.find('input[name="master-qty"]');
		var projectCode 	= $('input[name="pcode"]').val();

		var Html = "";

		// make sure there is no duplicate data
		if( table.find('tbody tr.task-' + masterItemId ).length > 0 ) {

			showError('You already input this service.');
    		return false;
		}
		
		// check if plan start, plan end, and quantity is not empty
		if( masterPlanStart.val() == "" || masterPlanEnd.val() == "" || masterPlanQty.val() == ""  ) {

    		showError('You forget input the quantity or set the plan date');
    		return false;
		}

		var existedData = getHtmlJsonData();
		if( existedData.length != 0 ) {
			console.log( existedData );
		}

		masterPlanQty = masterPlanQty.val();
		masterPlanQty = masterPlanQty.replace(/\./g, '');

		// check if quantity not overload
		if( parseInt(masterPlanQty) > masterItemIdQty ) {

    		showError('The number you input exceeds the maximum limit. The maximum is ' + masterItemIdQty);
    		return false;
		}


		// so, there we have make sure there is no empty field
    	// lets build the html for append into the table
    	Html += '<tr class="task-'+ masterItemId +'" data-delete="'+ROLEURL+'/project/deleteImpTask/'+ masterItemId +'">';
    	Html += '<td><input type="hidden" name="task-id[]" value="'+ masterItemId +'" />';
    	Html += '<input type="text" class="form-control" value="'+ masterItemName +'" readonly /></td>';
    	Html += '<td><input type="text" class="form-control" name="task-start['+ masterItemId +']" value="'+ masterPlanStart.val() +'" readonly /></td>';
    	Html += '<td><input type="text" class="form-control" name="task-end['+ masterItemId +']" value="'+ masterPlanEnd.val() +'" readonly /></td>';
    	Html += '<td><input type="text" class="form-control" name="task-qty['+ masterItemId +']" value="'+ masterPlanQty +'" required /></td>';
    	Html += '<td><button class="btn btn-sm bg-red" onclick="return implementationRemoveBosItem(this)" type="button"><i class="fa fa-times-circle"></i></button></td>'
    	Html += '</tr>';

    	table.find('tbody').append(Html);

    	//reset master field
    	masterPlanStart.val('');
    	masterPlanEnd.val('');
    	parent.find('input[name="master-qty"]').val('0');

		return false;
	}

	window.implementationRemoveBosItem = function implementationRemoveBosItem(ele) {
		
		var tr = $(ele).closest('tr');
		var form = $(ele).closest('form');	
		var contractor = form.find('[name="contractor"]').val();
		
		showSweetAlert( ele, function () {

    		showLoading('Deleting item...');
			if( contractor == '' ) {

				tr.remove();
				swal.close();

			} else {

				$.ajax({
					type: 'POST',
					url: tr.data('delete'),
					data: {
						'contractor' : contractor
					},
					success: function ( r ) {

						if( r == 'ok' ) {
							tr.remove();
							swal.close();
						}
					}

				});
			}
    	
    	});


		return false;
	}

	window.implementationSaveTask = function implementationSaveTask( form ) {
			
		// validate form 1 first -- contractor, plan start and end
		if( $(form).find('select[name="contractor"]').val == '' || $(form).find('input[name="plan-start"]').val() == '' || $(form).find('input[name="plan-end"]').val() == '' ) {

			showError('You forget input the contractor or set the plan date.');
    		return false;
		}

		// validate the task list
		var table = $(form).find('tbody input');
		if( table.length <= 0 ) {

			showError('You forget add the assigment task.');
    		return false;
		}

		showLoading('Saving task assigment...');

		var formData 	= $(form).serializeArray();
		var existedData = getHtmlJsonData();

		formData.push({
			'name' : 'existed-data',
			'value'	: JSON.stringify(existedData)
		});

		$.ajax({
			type: 'POST',
			data: formData,
			url: $(form).attr('action'),
			success: function (r) {
				console.log( r.indexOf('error:'));
				if( r == 'ok' ) {

					showSuccess('Task assigment has been saved.');

				} else if( r.indexOf('error:') >= 0 ){
					var errorMsg = r.split(':');
					showError( errorMsg[1]);
				} else {

					showError('There is a problem with server. Please try again.');

				}
			}
		});

		return false;
	}

	window.implementationSearchContractor = function implementationSearchContractor(ele) {

		var defaultContent 	= $('.gma-contractor-list .gma-content.default--content');
		var searchResult	= $('.gma-contractor-list .gma-content.search--result');
		var noResult		= $('.gma-contractor-list .gma-content.no--result');

		var loading = $('.gma-contractor-list .gma-section-loading');
		var value 	= $(ele).val(); 
		var form 	= $(ele).closest('form');

		if( value.length == 0 ) {

			searchResult.hide();
			noResult.hide();
			loading.hide();
			defaultContent.show();

		} else if( value.length >= 2 ) {

			defaultContent.hide();
			loading.show();

			$.ajax({
				type: 'POST',
				url: form.attr('action'),
				data: form.serializeArray(),
				success: function (r) {

					loading.hide();

					if( r == 'error' ) {

						searchResult.hide();
						noResult.show();

					} else {

						noResult.hide();
						searchResult.html( r );
						searchResult.show();

					}	

				}

			})

		}


	}


	function implementUpdateProgress() {

		$('#modalImpUpdateProgress').on('show.bs.modal', function ( event ) {

			var button = $(event.relatedTarget) // Button that triggered the modal
			var tr = button.closest('tr');
			var modal = $(this)
			var modalTable = modal.find('table');

			var taskName 	= tr.find('td.name a').text();
			var finishDate 	= tr.find('td.finish--date').html();
			var dataPlan	= tr.find('td.data--plan').html();
			var percentage  = tr.find('td.data--percentage').text();

			modal.find('.modal-title').text( taskName );
			modal.find('td.finish--date').html(finishDate);
			modal.find('td.data--plan').html(dataPlan);
			modal.find('td.data--percentage').text(percentage);

			modal.find('input[name="id_imp_detail"]').val( button.data('id') );
			modal.find('input[name="extra-data[task-name]"]').val( taskName.trim() );

		});

		$('#modalImpUpdateProgress').on('hide.bs.modal	', function ( event ) {

			var modal = $(this)
			modal.find('input[name="actual"]').val(0);

		});

	} implementUpdateProgress();

	window.implementationCalculateProgress = function implementationCalculateProgress( tr ) {
		
		var plan 	= tr.find('td.data--plan span.plan').text();
		var actual 	= tr.find('td.data--plan span.actual').text();
		
		plan = parseInt( plan.replace(/\./g, '') );
		actual = parseInt( actual.replace(/\./g, '') );
		var percentage = Math.round( ( actual / plan ) * 100 );

		tr.find('td.data--percentage').text( percentage + '%' );
		
		if( percentage == 100 ) {
			tr.find('td.action--btn').html('<span class="label bg-green">done</span>');
		}

		return percentage;
	}

	window.updateDailyTask = function updateDailyTask( form ) {
		
		var formData = $(form).serializeArray();

		// make sure there is no empty data
		if( formData[1].value.length == 0 ) {

			showError('You forget input the update value');
			return false;
		}

		//make sure the quantity is not overload
		var modal = $('#modalImpUpdateProgress');
		var dataPlan 	= modal.find('td.data--plan span.plan').text();
		var dataActual 	= modal.find('td.data--plan span.actual').text();
		var dataPlanQty = parseInt( dataPlan.replace(/\./g, '') );
		dataPlanQty = dataPlanQty - parseInt( dataActual );

		if( parseInt( formData[1].value.replace(/\./g, '') ) > dataPlanQty ) {

    		showError('The number you input exceeds the maximum limit. The maximum is ' + dataPlan );
			return false;
		}

		var msg = 'You want to update with the value <span class="label label-outline">'+ formData[1].value +'</span>';
		msg += '<br><span class="text-danger">After you save this update, you can not be able to change the value.</span>';
		msg += '<br>So, Are sure want to save with this value?';

		swal({
		  title: 'Wait..',
		  html: msg,
		  type: 'warning',
		  showCancelButton: true,
		  confirmButtonColor: '#3085d6',
		  cancelButtonColor: '#d33',
		  confirmButtonText: 'Yes, Save it',
		  cancelButtonText: 'No, its Wrong'
		}).then(function () {
			
			showLoading('Saving update...');

			$.ajax({
				type: 'POST',
				url: $(form).attr('action'),
				data: formData,
				success: function ( r ) {

					if( r == 'ok' ) {

						showSuccess('Update has been saved. You not be able to change it again.').then( function() {
							var table 		= $('table.update--daily');	
							var actual		= table.find('tr.task-' + formData[0].value + ' td.data--plan span.actual');
							var currentActual = parseInt( actual.text().replace(/\./g, '') );

							var acumulateActual = currentActual + parseInt( formData[1].value.replace(/\./g, '') );
							actual.text( formatRupiah( acumulateActual, false ) );
							modal.modal('hide');
							
							implementationCalculateProgress( table.find('tr.task-' + formData[0].value ) );

						});

					} else {
						showError('We have a server problem. Please try again');
					}
				}
			});

		}, function (dismiss) {

			swal.close();

		});

		return false;
	}

	window.finishDailyTask = function finishDailyTask( form ) {
		
		var formData = $(form).serializeArray();

		// make sure there is no empty data
		if( formData[1].value.length == 0 ) {

			showError('You forget input the update value');
			return false;
		}

		//make sure the quantity is not overload
		var modal = $('#modalImpFinishProgress');
		var dataPlan 	= modal.find('td.data--plan span.plan').text();
		var dataActual 	= modal.find('td.data--plan span.actual').text();
		var dataPlanQty = parseInt( dataPlan.replace(/\./g, '') );
		dataPlanQty = dataPlanQty - parseInt( dataActual );

		if( parseInt( formData[1].value.replace(/\./g, '') ) > dataPlanQty ) {

    		showError('The number you input exceeds the maximum limit. The maximum is ' + dataPlan );
			return false;
		}

		var msg = 'You want to update with the value <span class="label label-outline">'+ formData[1].value +'</span>';
		msg += '<br><span class="text-danger">After you save this update, you can not be able to change the value.</span>';
		msg += '<br>So, Are sure want to save with this value?';

		swal({
		  title: 'Wait..',
		  html: msg,
		  type: 'warning',
		  showCancelButton: true,
		  confirmButtonColor: '#3085d6',
		  cancelButtonColor: '#d33',
		  confirmButtonText: 'Yes, Save it',
		  cancelButtonText: 'No, its not finish yet'
		}).then(function () {
			
			showLoading('Saving update...');

			$.ajax({
				type: 'POST',
				url: $(form).attr('action'),
				data: formData,
				success: function ( r ) {

					if( r == 'ok' ) {

						showSuccess('Update has been saved. You not be able to change it again.').then( function() {
							var table 		= $('table.update--daily');	
							var actual		= table.find('tr.task-' + formData[0].value + ' td.data--plan span.actual');
							var currentActual = parseInt( actual.text().replace(/\./g, '') );

							var acumulateActual = currentActual + parseInt( formData[1].value.replace(/\./g, '') );
							actual.text( formatRupiah( acumulateActual, false ) );
							modal.modal('hide');
							
							implementationCalculateProgress( table.find('tr.task-' + formData[0].value ) );

						});

					} else {
						showError('We have a server problem. Please try again');
					}
				}
			});

		}, function (dismiss) {

			swal.close();

		});

		return false;
	}

	function implementationAddIssue() {

		$('#myaddIssue').on('show.bs.modal', function (event) {

		  var button 	= $(event.relatedTarget) // Button that triggered the modal
		  var id 		= button.data('id');
		  var index 	= button.data('target-index');

		  var table = button.closest('table');
		  var taskName = table.find('tr.task--name--' + index + ' td').text();

		  var modal = $(this);

		  modal.find('input[name="id_ref"]').val(id);
		  modal.find('input.task-name').val( taskName );

		});

	} implementationAddIssue();
	$('#ratingModal').on('show.bs.modal', function (event) {
	  var button = $(event.relatedTarget) // Button that triggered the modal
	  var $id = button.data('id') // Extract info from data-* attributes
	  var $name = button.data('name') // Extract info from data-* attributes
	  var $type = button.data('type') // Extract info from data-* attributes
	  var $project = button.data('project') // Extract info from data-* attributes
	  var $url = button.data('url') // Extract info from data-* attributes
	  // If necessary, you could initiate an AJAX request here (and then do the updating in a callback).
	  // Update the modal's content. We'll use jQuery here, but you could use a data binding library or other methods instead.
	  var modal = $(this)
	  modal.find('.modal-title').text('Review for '+ $name +' at Project '+ $project)
	  modal.find('#id_contractor').val($id)
	  modal.find('form').attr('action',$url)
	})

	window.mouseOverEmoji = function mouseOverEmoji( ele ) {
		var poor = 'red fa-frown-o';
		var meh = 'purple fa-meh-o';
		var good = 'green fa-smile-o';
		$(ele)
		.mousemove(function(e){
			if (ele.id == 'poor') {
				$( ele ).closest('#surveyEmoji').find('#good').removeClass(meh);
				$( ele ).closest('#surveyEmoji').find('#good').addClass(poor);
				$( ele ).closest('#surveyEmoji').find('#meh').removeClass(meh);
				$( ele ).closest('#surveyEmoji').find('#meh').addClass(poor);
				$( ele ).removeClass(meh);
				$( ele ).addClass(poor);
			}
			else if(ele.id == 'meh'){
				$( ele ).closest('#surveyEmoji').find('#poor').removeClass(good+' '+poor);
				$( ele ).closest('#surveyEmoji').find('#poor').addClass(meh);
				$( ele ).closest('#surveyEmoji').find('#good').removeClass(good+' '+poor);
				$( ele ).closest('#surveyEmoji').find('#good').addClass(meh);
				$( ele ).removeClass(good+' '+poor);
				$( ele ).addClass(meh);
			}
			else if(ele.id == 'good'){
				$( ele ).closest('#surveyEmoji').find('#poor').removeClass(meh);
				$( ele ).closest('#surveyEmoji').find('#poor').addClass(good);
				$( ele ).closest('#surveyEmoji').find('#meh').removeClass(meh);
				$( ele ).closest('#surveyEmoji').find('#meh').addClass(good);
				$( ele ).removeClass(meh);
				$( ele ).addClass(good);
			}
		})
		.mouseleave(function(e) {
			if (ele.id == 'poor') {
				$( ele ).closest('#surveyEmoji').find('#good').removeClass(poor);
				$( ele ).closest('#surveyEmoji').find('#good').addClass(meh);
				$( ele ).closest('#surveyEmoji').find('#meh').removeClass(poor);
				$( ele ).closest('#surveyEmoji').find('#meh').addClass(meh);
				$( ele ).removeClass(poor);
				$( ele ).addClass(meh);
			}
			else if(ele.id == 'meh'){
				$( ele ).closest('#surveyEmoji').find('#poor').removeClass(good+' '+poor);
				$( ele ).closest('#surveyEmoji').find('#poor').addClass(meh);
				$( ele ).closest('#surveyEmoji').find('#good').removeClass(good+' '+poor);
				$( ele ).closest('#surveyEmoji').find('#good').addClass(meh);
				$( ele ).removeClass(good+' '+poor);
				$( ele ).addClass(meh);
			}
			else if(ele.id == 'good'){
				$( ele ).closest('#surveyEmoji').find('#poor').removeClass(good);
				$( ele ).closest('#surveyEmoji').find('#poor').addClass(meh);
				$( ele ).closest('#surveyEmoji').find('#meh').removeClass(good);
				$( ele ).closest('#surveyEmoji').find('#meh').addClass(meh);
				$( ele ).removeClass(good);
				$( ele ).addClass(meh);
			}
		})
		.click(function(e) {
			if (ele.id == 'poor') {
				$( ele ).closest('#surveyEmoji').find('#good').removeClass(meh);
				$( ele ).closest('#surveyEmoji').find('#good').addClass(poor);
				$( ele ).closest('#surveyEmoji').find('#meh').removeClass(meh);
				$( ele ).closest('#surveyEmoji').find('#meh').addClass(poor);
				$( ele ).removeClass(meh);
				$( ele ).addClass(poor);
				$( ele ).closest('#surveyEmoji').find('input').val('0');
			}
			else if(ele.id == 'meh'){
				$( ele ).closest('#surveyEmoji').find('#poor').removeClass(good+' '+poor);
				$( ele ).closest('#surveyEmoji').find('#poor').addClass(meh);
				$( ele ).closest('#surveyEmoji').find('#good').removeClass(good+' '+poor);
				$( ele ).closest('#surveyEmoji').find('#good').addClass(meh);
				$( ele ).removeClass(good+' '+poor);
				$( ele ).addClass(meh);
				$( ele ).closest('#surveyEmoji').find('input').val('1');
			}
			else if(ele.id == 'good'){
				$( ele ).closest('#surveyEmoji').find('#poor').removeClass(meh);
				$( ele ).closest('#surveyEmoji').find('#poor').addClass(good);
				$( ele ).closest('#surveyEmoji').find('#meh').removeClass(meh);
				$( ele ).closest('#surveyEmoji').find('#meh').addClass(good);
				$( ele ).removeClass(meh);
				$( ele ).addClass(good);
				$( ele ).closest('#surveyEmoji').find('input').val('3');
			}
			$( ele ).unbind('mouseleave');
		});
	}

	$('#formRating').submit(function(){
		var $this = $(this);
		var $data = $this.serialize();
		var $url = $('#formRating').attr('action');
		showSweetConfirm( $this, function(){
			showLoading('Updating your data...');
			$.ajax({
			  	url: $url,
			  	data: $data,
			  	method: 'POST'
			}).done(function(r) { 
				$('#ratingModal').modal('hide');
			  	thisDataTable.api().ajax.reload( null, false );
				swal.close();
				$('#msg').html(r);
			});
		});
		return false;
	});

	$('#submit_rating_done').click(function(){
		var $url = $( '#submit_rating_done' ).attr('data-url');
		showLoading('Updating your data...');
			$.ajax({
			  	url: $url,
			  	method: 'POST'
			}).done(function(r) { 
			  	thisDataTable.api().ajax.reload( null, false );
				swal.close();
				$('#msg').html(r);
			});
	});

	// -----------------------------------------------------
	// Line Chart in Implementation Page
	// -----------------------------------------------------
	var implementationLineChart = $(".implementation--line--chart");
	if( implementationLineChart.length > 0 ) {

		var urlGet = implementationLineChart.data('url');
		$.get( urlGet, function( r ) {
			$('.gma--chart.gma-section-loading').hide();
			$('.gma--chart.content').show();

			console.log(r)

	        var line_chart = new Chart( implementationLineChart, {
	            type: 'line',
	            data: {
	                labels: r.labels,
	                datasets: [
	                {
	                	fill: false,
	                    label: 'Plan',
	                    data: r.dataPlan,
	                    backgroundColor: '#f44336',
	                    borderColor: '#f44336',
	                    borderWidth: 1
	                },
	                {
	                	fill: false,
	                    label: 'Actual',
	                    data: r.dataActual,
	                    backgroundColor: '#3694F4',
	                    borderColor: '#3694F4',
	                    borderWidth: 1
	                },
	                ]
	            },
	            options: {
	            	legend: {
	            		labels: {
	                		boxWidth: 20
	            		},
	            		position: 'bottom'
	            	},
	                scales: {
	                    yAxes: [{
	                        ticks: {
	                            beginAtZero:true
	                        }
	                    }]
	                }
	            }
	        });
		});

	}



    // -----------------------------------------------------
	// Delete contractor
	// used in page: operasional/contractor
	// -----------------------------------------------------
    window.opsRemoveContractor = function opsRemoveContractor(e) {
    	
    	var ele = $(e);

    	showSweetAlert( ele, function () {

    		showLoading('Deleting Item...');

	    	$.ajax({
	    		type: 'GET',
	    		url: ele.attr('data-delete'),
	    		success: function (r) {
	    			
	    			if( r=='ok' ) {
				    	swal.close();
	  					thisDataTable.api().ajax.reload( null, false );
	    			} else {
						showError('Error on deleting item. Please refresh page and try again');
	    			}
	    		}
	    	});
    	
    	});

    	return false;
    }

    // Material Request By
    $(".material_list").change(function(){

    	$('.request-material-item').empty();
    	if ($("#material_list").val() == 'bom')
    	{
    		var unit_url = "request_material/getMaterialBom";
			
			$('.request-material-item').select2({
				width: 'resolve',
				ajax: {
					url: unit_url,
					dataType: 'json',
					delay: 250,
				},
				placeholder: 'Item Name',
			});
			$('#select2-request-material-item-container').attr('style','margin-top : -7px');
		    $('.select2-selection--single').attr('style','border-radius: 0px;');
    	}
    	else
    	{
    		var unit_url = "request_material/getMaterial";
    		$('.request-material-item').select2({
				width: 'resolve',
				ajax: {
					url: unit_url,
					dataType: 'json',
					delay: 250,
				},
				placeholder: 'Item Name',
				minimumInputLength: 2,
			});
			$('#select2-request-material-item-container').attr('style','margin-top : -7px');
		    $('.select2-selection--single').attr('style','border-radius: 0px;');
    	}
    });

    // Select2 for Material Assignment
	var unit_url = $("#request-material-item").attr('data-url');
	$('.request-material-item').select2({
		width: 'resolve',
		ajax: {
			url: unit_url,
			dataType: 'json',
			delay: 250,
		},
		placeholder: 'Item Name'
	});
	$('#select2-request-material-item-container').attr('style','margin-top : -7px');
    $('.select2-selection--single').attr('style','border-radius: 0px;');
	
	// Only Number input
	$("#request_qty").keydown(function (e) {
        // Allow: backspace, delete, tab, escape, enter and .
        if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
             // Allow: Ctrl/cmd+A
            (e.keyCode == 65 && (e.ctrlKey === true || e.metaKey === true)) ||
             // Allow: Ctrl/cmd+C
            (e.keyCode == 67 && (e.ctrlKey === true || e.metaKey === true)) ||
             // Allow: Ctrl/cmd+X
            (e.keyCode == 88 && (e.ctrlKey === true || e.metaKey === true)) ||
             // Allow: home, end, left, right
            (e.keyCode >= 35 && e.keyCode <= 39)) {
                 // let it happen, don't do anything
                 return;
        }
        // Ensure that it is a number and stop the keypress
        if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
            e.preventDefault();
        }
    });

    $('#request-material-table').dataTable({
    	"processing": true,
		"serverSide": true,
        "order": [],	
		"ajax": {
			'url' : $('#request-material-table').attr('data-url'),
			'type': 'POST'
		},
		//Set column definition initialisation properties.
        'columnDefs': [
	        { 
	            'targets': [ 0 ], //first column / numbering column
	            'orderable': true, //set not orderable
	        },
        ],
	});

	$('#material-inventori-table').dataTable({
    	"processing": true,
		"serverSide": true,
        "order": [],	
		"ajax": {
			'url' : $('#material-inventori-table').attr('data-url'),
			'type': 'POST'
		},
		//Set column definition initialisation properties.
        'columnDefs': [
	        { 
	            'targets': [ 0 ], //first column / numbering column
	            'orderable': true, //set not orderable
	        },
        ],
	});

	$('#editUsedMaterial').on('show.bs.modal', function (event) {
	  var button = $(event.relatedTarget) // Button that triggered the modal
	  var $id_material = button.data('id_material') // Extract info from data-* attributes
	  var $name = button.data('name') // Extract info from data-* attributes
	  var $unit = button.data('unit') // Extract info from data-* attributes
	  var $project = button.data('project') // Extract info from data-* attributes
	  var $quantity = button.data('quantity') // Extract info from data-* attributes
	  var $url = button.data('url') // Extract info from data-* attributes
	  // If necessary, you could initiate an AJAX request here (and then do the updating in a callback).
	  // Update the modal's content. We'll use jQuery here, but you could use a data binding library or other methods instead.
	  var modal = $(this)
	  modal.find('.modal-title').text('Update Report Used Material for ' + $name)
	  modal.find('#id_material').val($id_material)
	  modal.find('#name').val($name)
	  modal.find('#unit').val($unit)
	  modal.find('#project').val($project)
	  modal.find('#quantity').val($quantity)
	  modal.find('form').attr('action',$url)
	})

	$('#applyEditUsedMaterial').on('click', function (e){
		$quantity = $('#quantity').val();

		var str = $('#used').val();
		var $used_input = str.replace(".", "");

		console.log(parseInt($used_input) + ' < ' + parseInt($quantity));
		// return false;

		if (parseInt($used_input) <= parseInt($quantity)) {
	    	$url = $('#formEditUsedMaterial').attr('action');
			showLoading('Updating your data...');
			$.ajax({
			  	url: $url,
			  	data: $('#formEditUsedMaterial').serialize(),
			}).done(function(){ 
				location.reload();
			});
		} else  {
			showError('Error on updating report, your used quantity material more than actual stock on your project.');
		}
	});

	$('#treeview').click(function(event) {
	    if(typeof(Storage) !== "undefined") {
	        if (localStorage.clickcount) {
	            localStorage.clickcount = Number(localStorage.clickcount)+1;
	        } else {
	            localStorage.clickcount = 1;
	        }
	    } 
	});
	
    if (localStorage.clickcount % 2 == 0) {
    	$('#treeview').attr('class', 'active');
    }else{
    	$('#treeview').removeClass('active');
    }

    

	window.taskDone = function taskDone( ele ) {
		
		var formData = $( '#formDaily' ).serializeArray();

		//make sure the quantity is not overload
		var modal = $('#modalImpUpdateProgress');
		var dataPlan 	= modal.find('td.data--plan span.plan').text();
		var dataActual 	= modal.find('td.data--plan span.actual').text();
		var dataPlanQty = parseInt( dataPlan.replace(/\./g, '') );
		dataPlanQty = dataPlanQty - parseInt( dataActual );

		if( parseInt( formData[1].value.replace(/\./g, '') ) > dataPlanQty ) {

    		showError('The number you input exceeds the maximum limit. The maximum is ' + dataPlan );
			return false;
		}
		
		var msg = 'You want to set this task as <span class="label label-outline"><span style="color:#CA0D1F">DONE</span></span>';
		msg += '<br><span class="text-danger">After you save this update, you can not be able to change the value.</span>';
		msg += '<br>So, Are sure want to set this task as <span class="label label-outline"><span style="color:#CA0D1F">DONE</span></span>?';

		swal({
		  title: 'Wait..',
		  html: msg,
		  type: 'warning',
		  showCancelButton: true,
		  confirmButtonColor: '#3085d6',
		  cancelButtonColor: '#d33',
		  confirmButtonText: 'Yes, Set it as DONE',
		  cancelButtonText: 'No, I change my mind'
		}).then(function () {
			
			showLoading('Finishing Task...');

			$.ajax({
				type: 'POST',
				url: $( '#formDaily' ).attr('action-done'),
				data: formData,
				success: function ( r ) {

					if( r == 'ok' ) {

						showSuccess('Update has been saved. You not be able to change it again.').then( function() {
							var table 		= $('table.update--daily');	
							var actual		= table.find('tr.task-' + formData[0].value + ' td.data--plan span.actual');
							var currentActual = parseInt( actual.text().replace(/\./g, '') );

							var acumulateActual = currentActual + parseInt( formData[1].value.replace(/\./g, '') );
							actual.text( formatRupiah( acumulateActual, false ) );
							modal.modal('hide');
							
							implementationCalculateProgress( table.find('tr.task-' + formData[0].value ) );

						});

					} else {
						showError('We have a server problem. Please try again');
					}
				}
			});

		}, function (dismiss) {

			swal.close();

		});

		return false;
	}

});
