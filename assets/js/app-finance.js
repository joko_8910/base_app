$(document).ready( function () {

	function toRp(angka){
	    var rev     = parseInt(angka, 10).toString().split('').reverse().join('');
	    var rev2    = '';
	    for(var i = 0; i < rev.length; i++){
	        rev2  += rev[i];
	        if((i + 1) % 3 === 0 && i !== (rev.length - 1)){
	            rev2 += '.';
	        }
	    }
	    return rev2.split('').reverse().join('');
	}

	initDataTable('.datatable', {
		'columns' : [ { "width": "1%" }, null, null, null, null, 
		    	{ 
		    		'className': 'text-center' 
		    	},
		    	{ 
		    		'className': 'text-center' 
		    	},
		    	{ 
		    		'className': 'text-center' 
		    	},
		    	{ 
		    		'className': 'text-center' 
		    	},
		    	{ 
		    		'width': '15%',
		    		'className': 'text-center' 
		    	}]
	});

	function getMarginPercentage( ele, marginInputManually ) {

		var tr 			= $(ele).closest('tr');
		var cost 		= parseInt( tr.find('.data--cost input').val().replace(/\./g, '') );
		var selling 	= parseInt( tr.find('.data--selling input').val().replace(/\./g, '') );
		var untung 		= selling - cost; 

		var margin = 0;
		if( cost != 0 ) {
			margin = Math.floor( 100 * untung/selling );
		}

		if( undefined == marginInputManually ) {
			tr.find('.data--margin input').val( margin );
		}

		return margin;

	}

	function countOutcome() {

		var totalCost 	= 0;
		var allCost 	= 0;

		ops 	 = $('#operational_cost').data('plain-text');
		unexpect = $('#unexpected_cost').data('plain-text');

		$('table.table--boq tbody tr').each( function (i, v) {
			
			var tr = $(this);
			var cost = parseInt(tr.find('.data--total--cost input').val());
			totalCost = totalCost + cost;
		});

		$('.data--bom .data--outcome').text( formatRupiah(totalCost) );
		$('.data--bom .data--outcome').attr( 'data-plain-text', totalCost );

		allCost = allCost + totalCost;
		totalCost = 0;

		$('table.table--bos tbody tr').each( function (i, v) {
			
			var tr = $(this);
			var cost = parseInt(tr.find('.data--total--cost input').val());
			totalCost = totalCost + cost;
		});
		allCost = allCost + totalCost + ops + unexpect;

		$('.data--bos .data--outcome').text( formatRupiah(totalCost) );
		$('.data--bos .data--outcome').attr( 'data-plain-text', totalCost );

		$('.data--master--outcome').text( formatRupiah(allCost) );
		$('.data--master--outcome').attr( 'data-plain-text', allCost );		

	}

	function countIncome() {

		var totalSell 	= 0;
		var allSell 	= 0;

		$('table.table--boq tbody tr').each( function (i, v) {
			
			var tr = $(this);
			var cost = parseInt(tr.find('.data--total--selling input').val());
			totalSell = totalSell + cost;
		});

		$('.data--bom .data--income').text( formatRupiah(totalSell) );
		$('.data--bom .data--income').attr( 'data-plain-text', totalSell );

		allSell = allSell + totalSell;
		totalSell = 0;
		$('table.table--bos tbody tr').each( function (i, v) {
			
			var tr = $(this);
			var cost = parseInt(tr.find('.data--total--selling input').val());
			totalSell = totalSell + cost;
		});

		allSell = allSell + totalSell;
		$('.data--bos .data--income').text( formatRupiah(totalSell) );
		$('.data--bos .data--income').attr( 'data-plain-text', totalSell );

		$('.data--master--income').text( formatRupiah(allSell) );
		$('.data--master--income').attr( 'data-plain-text', allSell );

		countOverallProfit();
		countOverallMargin();

	}

	function countOverallProfit() {

		var cost 	= 0;
		var sell 	= 0;
		var profit 	= 0;

		// -----------------------------------------------------
		// MATERIAL
		// -----------------------------------------------------
		cost = parseInt( $('.data--bom .data--outcome').attr( 'data-plain-text' ) );
		sell = parseInt( $('.data--bom .data--income').attr( 'data-plain-text' ) );
		profit = sell - cost;
		$('.data--bom .data--profit').attr( 'data-plain-text', profit );
		$('.data--bom .data--profit .format--text').text( formatRupiah(profit) );

		// -----------------------------------------------------
		// SERVICE
		// -----------------------------------------------------
		cost = parseInt( $('.data--bos .data--outcome').attr( 'data-plain-text' ) );
		sell = parseInt( $('.data--bos .data--income').attr( 'data-plain-text' ) );
		profit = sell - cost;
		$('.data--bos .data--profit').attr( 'data-plain-text', profit );
		$('.data--bos .data--profit .format--text').text( formatRupiah(profit) );

		// -----------------------------------------------------
		// MASTER PROFIT
		// -----------------------------------------------------
		cost = parseInt( $('.data--master--outcome').attr( 'data-plain-text' ) );
		sell = parseInt( $('.data--master--income').attr( 'data-plain-text' ) );
		profit = sell - cost;
		$('.data--master--profit').attr( 'data-plain-text', profit );
		$('.data--master--profit .format--text').text( formatRupiah(profit) );
		$('input[name="profit"]').val( profit );

	}

	function countOverallMargin() {

		// MATERIAL
		var cost 	= parseInt( $('.data--bom .data--outcome').attr( 'data-plain-text' ) );
		var sell 	= parseInt( $('.data--bom .data--income').attr( 'data-plain-text' ) );
		var profit 	= parseInt( $('.data--bom .data--profit').attr( 'data-plain-text' ) );

		var margin = 0;
		if( cost > 0 ) {
			margin = Math.floor( profit / sell * 100 );
		}

		$('.data--bom .data--percentage--profit .format--text').text( margin + '%' );

		// SERVICES
		cost 	= parseInt( $('.data--bos .data--outcome').attr( 'data-plain-text' ) );
		sell 	= parseInt( $('.data--bos .data--income').attr( 'data-plain-text' ) );
		profit 	= parseInt( $('.data--bos .data--profit').attr( 'data-plain-text' ) );

		margin = 0;
		if( cost > 0 ) {
			margin = Math.floor( profit / sell * 100 );
		}

		$('.data--bos .data--percentage--profit .format--text').text( margin + '%' );

		// MASTER
		sell 	= parseInt( $('.data--master--income').attr( 'data-plain-text' ) );
		cost 	= parseInt( $('.data--master--outcome').attr( 'data-plain-text' ) );
		profit 	= parseInt( $('.data--master--profit').attr( 'data-plain-text' ) );

		margin = 0;
		if( cost > 0 ) {
			margin = Math.floor( profit / sell * 100 );
		}

		$('.data--master--percentage--profit .format--text').text( margin + '%' );
		$('input[name="profit_percentage"]').val( margin );
	}

	$('#refreshOrb').click(function(event) {
		showLoading('Refresh Data...');
		setTimeout( function(){
			countOutcome()
			countIncome()
			swal.close();
		}, 1500);
	});

	window.finInputBoqCost = function finInputBoqCost( ele ) {
		
		var tr 			= $(ele).closest('tr');
		var quantity 	= parseInt( tr.find('.data--quantity').text() );
		var value 		= $(ele).val();

		if( value.length > 0 && value != 0 ) {
			tr.find('.data--margin input').removeAttr('readonly');
		} else {
			tr.find('.data--margin input').attr('readonly', 'readonly');
		}

		value = parseInt( value.replace(/\./g, '') );

		var totalCost = quantity * value;

		tr.find('td.data--total--cost .format--text').text( formatRupiah(totalCost) );
		tr.find('td.data--total--cost input').val( totalCost );
		getMarginPercentage( ele );
		countOutcome();

	}

	window.finInputBoqSelling = function finInputBoqSelling( ele, marginInputManually ) {
		
		var tr 		= $(ele).closest('tr');
		var quantity = parseInt( tr.find('.data--quantity').text() );
		var value 	= $(ele).val();

		value = parseInt( value.replace(/\./g, '') );

		var totalCost = quantity * value;

		tr.find('td.data--total--selling .format--text').text( formatRupiah(totalCost) );
		tr.find('td.data--total--selling input').val( totalCost );
		getMarginPercentage( ele, marginInputManually );
		countIncome();

	}

	window.finInputBoqMargin = function finInputBoqMargin( ele ) {
		
		var tr 				= $(ele).closest('tr');
		var cost 			= parseInt( tr.find('.data--cost input').val().replace(/\./g, '') );
		var persenMargin 	= $(ele).val();

		var selling = Math.round(100 * cost / (100 - persenMargin)); 
		// var sellingRound = (selling)
		var untung 	= selling - cost;

		var eleSelling = tr.find('.data--selling input');
		eleSelling.val( selling );
		
		eleSelling.maskMoney('mask');
		finInputBoqSelling( eleSelling[0], true );
	}

	window.validateBoq = function validateBoq( form ) {
		
		var budget = $('input[name="budget"]');

		if( budget.val() <= 0 ) {
			showError('You forget input the budget.');

			return false;
		}

		preloaderPage();

	}

	// -----------------------------------------------------
	// This function for BOQ that had edited 
	// On the first load of the page, system automatically trigger every input
	// This for auto counting the margin, profit, etc
	// 
	// @page : finance/boq
	// -----------------------------------------------------
	function iniFinEditFieldBoq() {
		
		if( $('.init--fin--table--boq').length > 0 ) {

			$('input[name="budget"]').maskMoney('mask');

			$('.data--bom table.table--boq tbody tr').each( function (i, v) {
				
				var tr = $(this);
				var costInput = tr.find('.data--cost input');
				var sellInput = tr.find('.data--selling input');

				costInput.keyup();
				sellInput.keyup();

				costInput.maskMoney('mask');
				sellInput.maskMoney('mask');

			});

			$('.data--bos table.table--bos tbody tr').each( function (i, v) {
				
				var tr = $(this);
				var costInput = tr.find('.data--cost input');
				var sellInput = tr.find('.data--selling input');

				costInput.keyup();
				sellInput.keyup();

				costInput.maskMoney('mask');
				sellInput.maskMoney('mask');

			});
		}

	} iniFinEditFieldBoq();

	$('#issues-project').dataTable({
    	"processing": true,
		"serverSide": true,
        "order": [],	
		"ajax": {
			'url' : $('#issues-project').attr('data-url'),
			'type': 'POST'
		},
		//Set column definition initialisation properties.
        'columnDefs': [
	        { 
	            'targets': [ 0 ], //first column / numbering column
	            'orderable': true, //set not orderable
	        },
        ],
	});

    var issuesProjectTable = $('#issues-project').dataTable();
 
    // $('#issues-project tbody').on( 'click', 'tr', function () {
    //     if ( $(this).hasClass('selected') ) {
    //         $(this).removeClass('selected');
    //     }
    //     else {
    //         issuesProjectTable.$('tr.selected').removeClass('selected');
    //         $(this).addClass('selected');
    //     }
    // } );
 
 	// select row and do something
    // $('#button').click( function () {
    //     issuesProjectTable.row('.selected').remove().draw( false );
	// } );

	// outside document ready
	window.openDetail = function openDetail( elem ) {
		var $id 			= $( elem ).attr('data-id');
		var $username  		= $( elem ).attr('data-username');
		var $userid  		= $( elem ).attr('data-userid');
		var $login  		= $( elem ).attr('data-login');
		var $type  			= $( elem ).attr('data-type');
		var $name 			= $( elem ).attr('data-name');
		var $title 			= $( elem ).attr('data-title');
		var $priority 		= $( elem ).attr('data-priority');
		var $status 		= $( elem ).attr('data-status');
		var $description 	= $( elem ).attr('data-description');
		var $image  		= $( elem ).attr('data-image');
		var $created  		= $( elem ).attr('data-created');
		var $modified  		= $( elem ).attr('data-modified');
		var $html 	= '<a href="#" class="close-issue" onclick="return closeIssueSeeDetail()"><i class="fa fa-times-circle-o"></i></a>';
		$html 	   += '<div class="issue-detail-header">';
		$html 	   += '<div class="pull-left">';
		$html 	   += '<h3>'+ $name +'</h3>';
		$html 	   += '<p>'+ $title +'</p></div>';
		if ($login === $userid) {
			$html 	   += '<div class="pull-right">';
			if ( $status == 'Solved' ) {
				$html 	   += '<div style="position: relative; z-index: 6;"><div style="padding-left:19px" class="label bg-aqua label-lg flat pointer" id="currentStatus" onclick="currentStatus( this )">'+ $status +' <span style="width:40px" class="fa fa-caret-down"></span></div>';
			}else{
				$html 	   += '<div style="position: relative; z-index: 6;"><div style="padding-left:16px" class="label bg-aqua label-lg flat pointer" id="currentStatus" onclick="currentStatus( this )">'+ $status +' <span style="width:8px" class="fa fa-caret-down"></span></div>';
			}
			$html 	   += '<div class="label bg-yellow-active label-lg flat pointer" onclick="deleteIssues( this )" data-unique="'+ $id +'"><span class="fa fa-trash-o"></span></div></div>';
			$html 	   += '<div class="label bg-aqua label-lg flat issues-detail-status-list disappear"> <ul class="list-unstyled"><li onclick="chooseStatusOne( this )" data-username="'+$username+'" data-unique="'+ $id +'" class="pointer">On Progress</li>';
			$html 	   += '<li onclick="chooseStatusTwo( this )" data-username="'+$username+'" data-unique="'+ $id +'" class="pointer">Solved</li></ul></div></div>';
		}else{
			$html 	   += '<div class="pull-right">';
			if ( $status == 'Solved' ) {
				$html 	   += '<div style="position: relative; z-index: 6;"><div style="padding-left:19px" class="label bg-aqua label-lg flat pointer" id="currentStatus" >'+ $status +' </div>';
			}else{
				$html 	   += '<div style="position: relative; z-index: 6;"><div style="padding-left:16px" class="label bg-aqua label-lg flat pointer" id="currentStatus" >'+ $status +' </div>';
			}
			$html 	   += '</div></div>';
		}

		$html 	   += '<div class="clearfix"></div><div class="col-md-12 no-padding">';
		$html 	   += '<div class="label bg-aqua label-lg flat" style="margin-right:20px">Type : '+ $type +'</div>';
		$html 	   += '<div class="label bg-yellow-active label-lg flat">Priority : '+ $priority +'</div></div>';
		$html 	   += '</div>';
		$html 	   += '<div class="issue-detail-body">';
		$html 	   += '<div class="no-filter box box-solid">';
		$html 	   += '<div class="box-body">';
		$html 	   += '<div class="post">';
		$html 	   += '<div class="user-block">';
		$html 	   += '<img class="img-circle img-bordered-sm" src="'+ $image +'" alt="user image">';
		$html 	   += '<span class="username">';
		$html 	   += '<a href="#">'+ $username +'</a></span>';
		$html 	   += '<span class="description">'+ $created +'</span></div>';
		$html 	   += '<p>'+ $description +'</p>';
		$html 	   += '<p id="modified">Last Updated by '+$username+' on '+$modified+'</p></div></div></div>';
		
		$('#sideDetail').html($html);

		// console.log(elem);
		// console.log($id+'\n'+$username+'\n'+$type+'\n'+$name+'\n'+$title+'\n'+$priority+'\n'+$status+'\n'+$description+'\n'+$image+'\n'+$created);
	    
	    if( $('.issue-detail-wrapper').hasClass('open') ) {
	        $('.issue-detail-wrapper').removeClass('open');
	        $('.vi-overlay').removeClass('in');
	    } else {
	        $('.issue-detail-wrapper').addClass('open');
	        $('.vi-overlay').addClass('in');
	    }
	}

    // ==============================================
	// datatable for service list or etc
    // ==============================================
	$('#datatable').dataTable({
    	"processing": true,
		"serverSide": true,
        "order": [],	
		"ajax": {
			'url' : $('#datatable').attr('data-url'),
			'type': 'POST'
		},
		//Set column definition initialisation properties.
        'columnDefs': [
	        { 
	            'targets': [ 0 ], //first column / numbering column
	        },
        ],
	});

    // ==============================================
	 //service list start
    // ==============================================
    var unit_url_s = $("#s_unit").attr('data-url');
    $("#s_unit").select2({
    	tags: true,
		width: 'resolve',
		ajax: {
			url: unit_url_s,
			dataType: 'json',
			delay: 250,
		},
		placeholder: 'ex. Metre, Gram',
		minimumInputLength: 1,
	});

    $('#select2-s_unit-container').attr('style','margin-top : -7px');
    $('.select2-selection--single').attr('style','border-radius: 0px;');

    var edit_unit_url_s = $("#s_edit_unit").attr('data-url');
    $("#s_edit_unit").select2({
    	tags: true,
		width: 'resolve',
		ajax: {
			url: edit_unit_url_s,
			dataType: 'json',
			delay: 250,
		},
		placeholder: 'ex. Metre, Gram',
		minimumInputLength: 1,
	});

    $('#select2-s_edit_unit-container').attr('style','margin-top : -7px');

    $('#s_item').on('blur', function(){
    	var item_url = $("#s_item").attr('data-url');
    	$("#s_code").val('generating...');
		$.ajax({
		  	url: item_url,
		  	method: 'POST',
		  	data: "item=" + $("#s_item").val() + ' ',
		}).done(function(response) { 
			$("#s_code").val(response);
		});
    });

    $('#s_item').on('keyup', function(){
    	var item_url = $("#s_item").attr('data-check');
    	$("#s_code_check").text('generating...');
		$.ajax({
		  	url: item_url,
		  	method: 'POST',
		  	data: "item=" + $("#s_item").val(),
		}).success(function(response) { 
			$("#s_code_check").html(response);
		});
    });

    $('#saveService').on('click',function(){
    	var addForm_url = $("#s_addForm").attr('action');
		showLoading('Saving Data...');
		$.ajax({
		  	url: addForm_url,
		  	method: 'POST',
		  	data: $('#s_addForm').serialize(),
		}).done(function() { 
			$("#addService").modal('hide')
			swal.close();
		  	thisDataTable.dataTable().api().ajax.reload( null, false );
		});
    });

    $('#applyEditService').on('click',function(){
    	var url = $("#editService").find('form').attr('action');
		showLoading('Updating Data...');
		$.ajax({
		  	url: url,
		  	method: 'POST',
		  	data: $("#editService").find('form').serialize(),
		}).done(function() { 
			$("#editService").modal('hide')
			swal.close();
		  	thisDataTable.dataTable().api().ajax.reload( null, false );
		});
    });

	$('#editService').on('show.bs.modal', function (event) {
		var button 	= $(event.relatedTarget) // Button that triggered the modal
		var $url 	= button.data('url') // Extract info from data-* attributes
		var $name 	= button.data('name') // Extract info from data-* attributes
		var $id 	= button.data('id') // Extract info from data-* attributes
		var $unit 	= button.data('unit') // Extract info from data-* attributes
		var $cost 	= button.data('cost') // Extract info from data-* attributes
		var $selling 	= button.data('selling') // Extract info from data-* attributes
		// If necessary, you could initiate an AJAX request here (and then do the updating in a callback).
		// Update the modal's content. We'll use jQuery here, but you could use a data binding library or other methods instead.
		var modal = $(this)
		modal.find('.modal-title').text('Update for ' + $name)
		$('#s_edit_unit').append('<option value="'+$unit+'" selected="selected">'+$unit+'</option>');
		$('#s_edit_unit').trigger('change');
		modal.find('#s_edit_item').val($name)
		modal.find('#s_edit_code').val($id)
		modal.find('#s_edit_cost').val($cost)
		modal.find('#s_edit_selling').val($selling)
		modal.find('form').attr('action',$url)	
	})

	window.deleteService = function deleteService( elem ) {
		showSweetAlert( elem, function(){
			var $id 			= $( elem ).attr('data-id');
			showLoading('Deleting Item...');
			$.ajax({
			  	url: "service_list/delete?id="+$id
			}).done(function() { 
				swal.close();
			  	thisDataTable.api().ajax.reload( null, false );
			});
		});

		return false;
	}

    // ==============================================
    //service list end
    // ==============================================

    // ==============================================
    //material list start
    // ==============================================
    var unit_url = $("#mi_unit").attr('data-url');
    $("#mi_unit").select2({
    	tags: true,
		width: 'resolve',
		ajax: {
			url: unit_url,
			dataType: 'json',
			delay: 250,
		},
		placeholder: 'ex. Metre, Gram',
		minimumInputLength: 1,
	});

    $('#select2-mi_unit-container').attr('style','margin-top : -7px');
    $('.select2-selection--single').attr('style','border-radius: 0px;');

    var edit_unit_url = $("#mi_edit_unit").attr('data-url');
    $("#mi_edit_unit").select2({
    	tags: true,
		width: 'resolve',
		ajax: {
			url: edit_unit_url,
			dataType: 'json',
			delay: 250,
		},
		placeholder: 'ex. Metre, Gram',
		minimumInputLength: 1,
	});

    $('#select2-mi_edit_unit-container').attr('style','margin-top : -7px');

    $('#mi_item').on('blur', function(){
    	var item_url = $("#mi_item").attr('data-url');
    	$("#mi_code").val('generating...');
		$.ajax({
		  	url: item_url,
		  	method: 'POST',
		  	data: "item=" + $("#mi_item").val() + ' ',
		}).done(function(response) { 
			$("#mi_code").val(response);
		});
    });

    $('#mi_item').on('keyup', function(){
    	var item_url = $("#mi_item").attr('data-check');
    	$("#mi_code_check").text('generating...');
		$.ajax({
		  	url: item_url,
		  	method: 'POST',
		  	data: "item=" + $("#mi_item").val(),
		}).success(function(response) { 
			$("#mi_code_check").html(response);
		});
    });

    $('#saveMaterial').on('click',function(){
    	var addForm_url = $("#mi_addForm").attr('action');
		showLoading('Saving Data...');
		$.ajax({
		  	url: addForm_url,
		  	method: 'POST',
		  	data: $('#mi_addForm').serialize(),
		}).done(function() { 
			$("#addMaterial").modal('hide')
			swal.close();
		  	thisDataTable.dataTable().api().ajax.reload( null, false );
		});
    });

    $('#applyEditMaterial').on('click',function(){
    	var url = $("#editMaterial").find('form').attr('action');
		showLoading('Updating Data...');
		$.ajax({
		  	url: url,
		  	method: 'POST',
		  	data: $("#editMaterial").find('form').serialize(),
		}).done(function() { 
			$("#editMaterial").modal('hide')
			swal.close();
		  	thisDataTable.dataTable().api().ajax.reload( null, false );
		});
    });

	$('#editMaterial').on('show.bs.modal', function (event) {
		var button 	= $(event.relatedTarget) // Button that triggered the modal
		var $url 	= button.data('url') // Extract info from data-* attributes
		var $name 	= button.data('name') // Extract info from data-* attributes
		var $id 	= button.data('id') // Extract info from data-* attributes
		var $unit 	= button.data('unit') // Extract info from data-* attributes
		var $cost 	= button.data('cost') // Extract info from data-* attributes
		var $selling 	= button.data('selling') // Extract info from data-* attributes
		// If necessary, you could initiate an AJAX request here (and then do the updating in a callback).
		// Update the modal's content. We'll use jQuery here, but you could use a data binding library or other methods instead.
		var modal = $(this)
		modal.find('.modal-title').text('Update for ' + $name)
		$('#mi_edit_unit').append('<option value="'+$unit+'" selected="selected">'+$unit+'</option>');
		$('#mi_edit_unit').trigger('change');
		modal.find('#mi_edit_item').val($name)
		modal.find('#mi_edit_code').val($id)
		if ($cost != '' || $cost != null) {
			
			$defaultCost = $cost;
		}
		else{
				$defaultCost = 0;
			}
		modal.find('#mi_edit_cost').val($defaultCost)
		modal.find('#mi_edit_selling').val($selling)
		modal.find('form').attr('action',$url)	
	})

	window.deleteMaterial = function deleteMaterial( elem ) {
		showSweetAlert( elem, function(){
			var $id 			= $( elem ).attr('data-id');
			showLoading('Deleting Item...');
			$.ajax({
			  	url: "material_list/delete?id="+$id
			}).done(function() { 
				swal.close();
			  	thisDataTable.api().ajax.reload( null, false );
			});
		});

		return false;
	}

    // ==============================================
    //material list end
    // ==============================================

	var thisDataTable = $('#datatable').dataTable();

	window.closeIssueSeeDetail = function closeIssueSeeDetail( ele ) {
	    
	    if( $('.issue-detail-wrapper').hasClass('open') ) {

	        $('.issue-detail-wrapper').removeClass('open');
	        $('.vi-overlay').removeClass('in');

	    } else {
	        
	        $('.issue-detail-wrapper').addClass('open');
	        $('.vi-overlay').addClass('in');

	    }

	    return false;
	}

	window.currentStatus = function currentStatus( ele ) {
	    
	    if( $( '.issues-detail-status-list' ).hasClass('disappear') ) {

	        $( '.issues-detail-status-list' ).removeClass('disappear');
	        $( '.issues-detail-status-list' ).addClass('appear');
	        $( 'li.pointer' ).css( "color", "white" );

	    } else {

	        $( '.issues-detail-status-list' ).removeClass('appear');
	        $( 'li.pointer' ).css( "color", "transparent" );
	        $( '.issues-detail-status-list' ).addClass('disappear');
	    }
	    return false;
	}

	window.chooseStatusOne = function chooseStatusOne( elem ) {
		var $id 			= $( elem ).attr('data-unique');
		var $username 			= $( elem ).attr('data-username');

	    $( '#modified' ).html( 'Last Updated by '+$username+' on '+dateNow() );
	    $( '#currentStatus' ).html( 'On Progress <span class="fa fa-caret-down"></span>' ).css( 'padding-left', '16px' );
		$.ajax({
		  url: "issues/status?s=on progress&id="+$id
		}).done(function() {
		  issuesProjectTable.api().ajax.reload( null, false );
		});
	}

	window.chooseStatusTwo = function chooseStatusTwo( elem ) {
		var $id 			= $( elem ).attr('data-unique');
		var $username 			= $( elem ).attr('data-username');

	    $( '#modified' ).html( 'Last Updated by '+$username+' on '+dateNow() );
	    $( '#currentStatus' ).html( 'Solved <span style="width:40px" class="fa fa-caret-down"></span>' ).css( 'padding-left', '19px' );
		$.ajax({
		  url: "issues/status?s=solved&id="+$id
		}).done(function() { 
		  issuesProjectTable.api().ajax.reload( null, false );
		});
	}

	window.deleteIssues = function deleteIssues( elem ) {
		showSweetAlert( elem, function(){
			var $id 			= $( elem ).attr('data-unique');
			showLoading('Deleting Item...');
			$.ajax({
			  	url: "issues/deleteData?id="+$id
			}).done(function() { 
				swal.close();
			  	issuesProjectTable.api().ajax.reload( null, false );
		        $('.issue-detail-wrapper').removeClass('open');
		        $('.vi-overlay').removeClass('in');
			});
		});

		return false;
	}

	// purchase list start
	window.getPrint = function getPrint( elem ) {
		var $this = $( elem )
		var $target = $this.attr('data-target')
		var $printContents = document.getElementById($target).innerHTML;
		var $originalContents = document.body.innerHTML;

		document.body.innerHTML = $printContents;

		window.print();

		document.body.innerHTML = $originalContents;
	}

  	$('textarea').summernote({
  		height: 180,
		toolbar: [
		// [groupName, [list of button]]
			['style', ['bold', 'italic', 'underline', 'clear']],
			['fontsize', ['fontsize']],
			['para', ['ul', 'ol', 'paragraph']],
			['height', ['height']]
		]
  	})
    // ==============================================
	// purchase list end
    // ==============================================

    // ==============================================
	// purchase list page start
    // ==============================================
	$('#additional_desc_item~.note-editor,#payment_term,#delivery,#additional_desc_po~.note-editor,#pic,#fob').hide();

	$('#createPO').on('show.bs.modal', function (event) {
		var button = $(event.relatedTarget) // Button that triggered the modal
		var $url  		= button.data('url') // Extract info from data-* attributes
		var $from 		= button.data('from')
		var $to 		= button.data('to')
		var $fromAdd 	= button.data('fromadd')
		var $toAdd 		= button.data('toadd')
		var $req 		= button.data('req')
		var $ship 		= button.data('ship')
		var $item 		= button.data('item')
		var $itemCode 	= button.data('itemCode')
		var $qty 		= button.data('qty')
		var $unit 		= button.data('unit') 
		var $qty 		= button.data('qty') 
		var $price 		= button.data('price') 
		var $nomor_po 	= button.data('nomor_po') 
		var $amount 	= button.data('amount') 

		var modal 		= $(this)
	  	modal.find('#nomor_po').val($nomor_po)
	  	modal.find('#itemCode').val($itemCode)
	  	modal.find('#vendor').val($from)
	  	modal.find('#sendto').val($to)
	  	modal.find('#from_address').val($fromAdd)
	  	modal.find('#from_addressP').html($fromAdd)
	  	modal.find('#to_address').val($toAdd)
	  	modal.find('#to_addressP').html($toAdd)
	  	modal.find('#requisioner').val($req)
	  	modal.find('#shipvia').val($ship)
	  	modal.find('#item').val($item+' ('+$unit+') ')
	  	modal.find('#price').val('Rp. '+toRp($price))
	  	modal.find('#qty').val(toRp($qty))
	  	modal.find('#amount').val('Rp. '+toRp($amount))
	  	modal.find('#amountShadow').val($amount)
	  	modal.find('form').attr({action:$url,method:'post'})
	})
	$('#commitPO').on('click', function(e){
		var $form = $('#createPO').find('form');
		$.ajax({
		  	url: $form[0].action,
		  	method: 'POST',
		  	data: $form.serialize()
		}).done(function() { 
			// location.reload();
		});
	})
	$('#createPO').on('hidden.bs.modal', function(){
		showSweetConfirm( this, function(){
	    	$('#formPO')[0].reset();
		});
		return false;
	});
	window.rangeInputNumber = function rangeInputNumber( elem ) {
		var max = parseInt($(elem).attr('max'));
		var min = parseInt($(elem).attr('min'));
		var shadow = '#'+elem.id+'Shadow';

		if (max != undefined || max != null || max != '') {
			if ($(elem).val() > max)
			{
			  $(elem).val(max);
			  $(shadow).text(toRp(max))
			}
			else if ($(elem).val() < min)
			{
			  $(elem).val(min);
			  $(shadow).text(toRp(min))
			}     
			else
			{
			  $(shadow).text(toRp($(elem).val()))
			}  
		}else{
			if ($(elem).val() < min)
			{
			  $(elem).val(min);
			  $(shadow).text(toRp(min))
			}     
			else
			{
			  $(shadow).text(toRp($(elem).val()))
			}  
		}
	}
	window.editVal = function editVal( elem ) {
		$(elem).parent().siblings('input,.note-editor').slideDown( "slow" );
		$(elem).siblings(elem).fadeIn( "slow" );
		$(elem).hide();
	}
	window.doneVal = function doneVal( elem ) {
		$(elem).parent().siblings('input,.note-editor').slideUp( "slow" );
		$(elem).siblings(elem).fadeIn( "slow" );
		$(elem).hide();
	}
	window.dpVal = function dpVal( elem ) {
		$('#dp').slideDown( "slow" );
		$('#payment_percent').attr( "required",'true' );
		$('#payment_note_full').slideUp( "slow" );
		$('#payment_note_full').find('input').val('');
		$('#payment_note_full').siblings(elem).children('.fa-check-square-o').hide();
		$('#payment_note_full').siblings(elem).children('.fa-edit').fadeIn();
	}
	window.lunasVal = function lunasVal( elem ) {
		var $amount = $('#amountShadow').val()

		$('#dp').slideUp( "slow" );
		$('#dp').find('input').val('');
		$('#debtShadow').text('');
		$('#amount').val('Rp. '+toRp($amount))
	}
	window.lunasEditVal = function lunasEditVal( elem ) {
		$('#payment_note_full').slideDown( "slow" );
		$(elem).siblings(elem).fadeIn( "slow" );
		$(elem).hide();
	}
	window.lunasDoneVal = function lunasDoneVal( elem ) {
		$('#payment_note_full').slideUp( "slow" );
		$(elem).siblings(elem).fadeIn( "slow" );
		$(elem).hide();
	}
	$('#qty,#price,#payment_percent').on('keyup, keydown', function(elem){
		// Allow: backspace, delete, tab, escape, enter and .
        if ($.inArray(elem.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
             // Allow: Ctrl+A, Command+A
            (elem.keyCode === 65 && (elem.ctrlKey === true || elem.metaKey === true)) || 
             // Allow: home, end, left, right, down, up
            (elem.keyCode >= 35 && elem.keyCode <= 40)) {
                 // let it happen, don't do anything
                 return;
        }
        // Ensure that it is a number and stop the keypress
        if ((elem.shiftKey || (elem.keyCode < 48 || elem.keyCode > 57)) && (elem.keyCode < 96 || elem.keyCode > 105)) {
            elem.preventDefault();
        }
	})
	$('#payment_term,#delivery,#pic,#fob').on('keyup', function(){
		var input = $(this).val();
		var shadow = '#'+this.id+'Shadow';

		$(shadow).html(input);
	})
	$('#amountShadow, #payment_percent').on('keyup', function(){
		var $payment_percent = $('#payment_percent').val()
		var $amount 	= $('#amountShadow').val()
		var $creditAmount 	= ( $payment_percent ) / 100 * $amount
		var $debt 		= ( 100 - $payment_percent ) / 100 * $amount

		if( $payment_percent != ''){
			$('#amount').val('Rp. '+toRp($creditAmount))
			$('#amountPay').val($creditAmount)
			$('#debtShadow').text('Debt : Rp. '+toRp($debt))
		}else{
			$('#amount').val('Rp. '+toRp($amount))
		}
	})
	$('#price').on('focus', function(){
		var default_value = 0;
        if($(this).val() == default_value)
	        {
	             $(this).val("");
	        }
	})
	$('#price').on('blur', function(){
		var default_value = 0;
        if($(this).val().length == 0)
        {
            $(this).val(default_value);
        }
    });
    window.getPONumber = function getPONumber( elem ) {
    	var $this = $(elem);
	    swal(
	      'Purchase Order Number is',
	      $this.attr('data-nomor'),
	      'success'
	    )  
	    return false;
	}
	window.getPrint = function getPrint( elem ) {
		var $this = $( elem )
		var $target = $this.attr('data-target')
		var $printContents = document.getElementById($target).innerHTML;
		var $originalContents = document.body.innerHTML;

		document.body.innerHTML = $printContents;

		window.print();

		document.body.innerHTML = $originalContents;
	}
    // ==============================================
    // purchase list end
    // ==============================================


    // ==============================================
    // boq unexpected cost and operasional cost start
    // ==============================================
    var sourceMultiply 			 = $('#budget');
    var operasional 			 = $('#operational_cost');
    var unexpected 				 = $('#unexpected_cost');
    var operasionalPercentage 	 = $('#operational_cost_percentage');
    var unexpectedPercentage 	 = $('#unexpected_cost_percentage');

    sourceMultiply.keyup(function(event) {
	    var sourceMultiplyVal 		 = sourceMultiply.val();
	    var operasionalPercentageVal = operasionalPercentage.val();
    	var unexpectedPercentageVal  = unexpectedPercentage.val();

    	if ( sourceMultiplyVal == '' ) {
    		operasional.val('0')
    		unexpected.val('0')
    	}
    	
    	noDotVal = sourceMultiplyVal.replace(/\./g, '');

    	resultOPS = parseInt(noDotVal) * parseInt(operasionalPercentageVal) / 100;
    	resultUNE = parseInt(noDotVal) * parseInt(unexpectedPercentageVal) / 100;

    	operasional.data('plain-text',resultOPS);
    	unexpected.data('plain-text',resultUNE);
    	operasional.val(formatRupiah(resultOPS, false));
    	unexpected.val(formatRupiah(resultUNE, false));
    });

    operasional.keyup(function(event) {
	    var sourceMultiplyVal 		 = sourceMultiply.val();
	    var operasionalVal 			 = operasional.val();

    	if ( operasionalVal == '' || sourceMultiplyVal == ''  ) {
    		operasionalPercentage.val('0')
    	}
    	
    	noDotVal = sourceMultiplyVal.replace(/\./g, '');

    	result = parseInt(operasionalVal.replace(/\./g, '')) / parseInt(noDotVal) * 100;
    	operasionalPercentage.val( formatRupiah(result, false));
    });

    unexpected.keyup(function(event) {
	    var sourceMultiplyVal 		 = sourceMultiply.val();
	    var operasionalVal 			 = operasional.val();
	    var unexpectedVal 			 = unexpected.val();
	    var operasionalPercentageVal = operasionalPercentage.val();
    	var unexpectedPercentageVal  = unexpectedPercentage.val();

    	if ( unexpectedVal == '' || sourceMultiplyVal == ''  ) {
    		unexpectedPercentage.val('0')
    	}
    	
    	noDotVal = sourceMultiplyVal.replace(/\./g, '');

    	result = parseInt(unexpectedVal.replace(/\./g, '')) / parseInt(noDotVal) * 100;
    	unexpectedPercentage.val( formatRupiah(result, false));
    });

    operasionalPercentage.keyup(function(event) {
	    var sourceMultiplyVal 		 = sourceMultiply.val();
	    var operasionalVal 			 = operasional.val();
	    var operasionalPercentageVal = operasionalPercentage.val();

    	if ( operasionalPercentageVal == '' || sourceMultiplyVal == '' ) {
    		operasional.val('0')
    	}
    	
    	noDotVal = sourceMultiplyVal.replace(/\./g, '');

    	result = parseInt(noDotVal) * parseInt(operasionalPercentageVal) / 100;
    	operasional.data('plain-text',result);
    	operasional.val( formatRupiah(result, false)) ;
    });

    unexpectedPercentage.keyup(function(event) {
	    var sourceMultiplyVal 		 = sourceMultiply.val();
	    var unexpectedVal 			 = unexpected.val();
    	var unexpectedPercentageVal  = unexpectedPercentage.val();

    	if ( unexpectedPercentageVal == '' || sourceMultiplyVal == ''  ) {
    		unexpected.val('0')
    	}

    	noDotVal = sourceMultiplyVal.replace(/\./g, '');

    	result = parseInt(noDotVal) * parseInt(unexpectedPercentageVal) / 100
    	unexpected.data('plain-text',result);
    	unexpected.val( formatRupiah(result, false));
    });
    // ==============================================
    // boq unexpected cost and operasional cost end
    // ==============================================

	window.approveFPD = function approveFPD( elem ) {
		var $this = $( elem )
		var $url = $this.attr('data-target')
		$.ajax({
			url: $url,
			type: 'POST'
		})
		.done(function() {
			location.reload();
		});
	}
});