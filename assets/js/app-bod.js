
$(document).ready( function () {

	initDataTable('.datatable--bod', {
		'columns' : [ { "width": "1%" }, null, null,
				{ 
		    		'className': 'text-center' 
		    	},
		    	{ 
		    		'className': 'text-center' 
		    	},
		    	{ 
		    		'className': 'text-center' 
		    	},
		    	{ 
		    		'className': 'text-center' 
		    	},
		    	{ 
		    		'className': 'text-center' 
		    	},
		    	{ 
		    		'width': '25%',
		    		'className': 'text-center' 
		    	} ]
	} );

	window.bodRejectProject = function bodRejectProject(ele) {
		
		swal({
			title: 'Reject Project',
			text: 'You are about reject a project. Please Add an additional note',
			input: 'textarea',
			inputPlaceholder: 'Type your note here',
			showCancelButton: true,
			inputValidator: function (value) {
				return new Promise(function (resolve, reject) {
					if (value) {
						resolve()
					} else {
						reject('You need to write a note!');
					}
				})
			}

		}).then(function (text) {

			if (text) {

				showLoading('Saving data...');

				$.ajax({
					type: 'POST',
					url: $(ele).attr('href'),
					data: {
						note: text
					},
					success: function (r) {

						preloaderPage();
						window.location = r;
					}
				});

			} else {
				reject('You need to write a note!');
			}

		});

		return false;
	}

	window.bodRejectOngoingProject = function bodRejectOngoingProject(ele) {
		
		swal({
			title: 'WATCH OUT!',
			text: 'You are about cancel a running project. Are sure want to do this?',
			showCancelButton: true,
			confirmButtonColor: '#3085d6',
			cancelButtonColor: '#d33',
			confirmButtonText: 'Yes, do it!',
			cancelButtonText: 'No, cancel!'
		}).then(function () {

			bodRejectProject( ele );
		}, function (dismiss) {

			swal.close();

		});


		return false;
	}

	window.bodRequestRevision = function bodRequestRevision( ele ) {
		
		swal.setDefaults({
			input: 'text',
			confirmButtonText: 'Next &rarr;',
			showCancelButton: true,
			progressSteps: ['1', '2']
		})

		var steps = [
		{
			title: 'Choose Division',
			text: 'Choose which division will receive this revision request.',
			input: 'select',
			  inputOptions: {
			    'on_ops': 'Operasional',
			    'on_finance': 'Finance'
			  },
		},
		{
			title: 'Write a Note',
			text: 'Add some revision note',
			input: 'textarea'
		}];

		swal.queue(steps).then(function (result) {

			swal.resetDefaults()
			showLoading('Proccessing data...');
			
			$.ajax({
				type: 'POST',
				url: $(ele).attr('href'),
				data: {
					status: result[0],
					note: result[1]
				},
				success: function ( r ) {

					preloaderPage();
					window.location = r;

				}
			});

		}, function () {
			swal.resetDefaults()
		})

		return false;
	}

    $('#issues-project').dataTable({
    	"processing": true,
		"serverSide": true,
        "order": [],	
		"ajax": {
			'url' : $('#issues-project').attr('data-url'),
			'type': 'POST'
		},
		//Set column definition initialisation properties.
        'columnDefs': [
	        { 
	            'targets': [ 0 ], //first column / numbering column
	            'orderable': true, //set not orderable
	        },
        ],
	});

    var issuesProjectTable = $('#issues-project').dataTable();
 
    // $('#issues-project tbody').on( 'click', 'tr', function () {
    //     if ( $(this).hasClass('selected') ) {
    //         $(this).removeClass('selected');
    //     }
    //     else {
    //         issuesProjectTable.$('tr.selected').removeClass('selected');
    //         $(this).addClass('selected');
    //     }
    // } );
 
 	// select row and do something
    // $('#button').click( function () {
    //     issuesProjectTable.row('.selected').remove().draw( false );
	// } );

	window.openDetail = function openDetail( elem ) {
		var $id 			= $( elem ).attr('data-id');
		var $username  		= $( elem ).attr('data-username');
		var $userid  		= $( elem ).attr('data-userid');
		var $login  		= $( elem ).attr('data-login');
		var $type  			= $( elem ).attr('data-type');
		var $name 			= $( elem ).attr('data-name');
		var $title 			= $( elem ).attr('data-title');
		var $priority 		= $( elem ).attr('data-priority');
		var $status 		= $( elem ).attr('data-status');
		var $description 	= $( elem ).attr('data-description');
		var $image  		= $( elem ).attr('data-image');
		var $created  		= $( elem ).attr('data-created');
		var $modified  		= $( elem ).attr('data-modified');
		var $html 	= '<a href="#" class="close-issue" onclick="return closeIssueSeeDetail()"><i class="fa fa-times-circle-o"></i></a>';
		$html 	   += '<div class="issue-detail-header">';
		$html 	   += '<div class="pull-left">';
		$html 	   += '<h3>'+ $name +'</h3>';
		$html 	   += '<p>'+ $title +'</p></div>';
		if ($login === $userid) {
			$html 	   += '<div class="pull-right">';
			if ( $status == 'Solved' ) {
				$html 	   += '<div style="position: relative; z-index: 6;"><div style="padding-left:19px" class="label bg-aqua label-lg flat pointer" id="currentStatus" onclick="currentStatus( this )">'+ $status +' <span style="width:40px" class="fa fa-caret-down"></span></div>';
			}else{
				$html 	   += '<div style="position: relative; z-index: 6;"><div style="padding-left:16px" class="label bg-aqua label-lg flat pointer" id="currentStatus" onclick="currentStatus( this )">'+ $status +' <span style="width:8px" class="fa fa-caret-down"></span></div>';
			}
			$html 	   += '<div class="label bg-yellow-active label-lg flat pointer" onclick="deleteIssues( this )" data-unique="'+ $id +'"><span class="fa fa-trash-o"></span></div></div>';
			$html 	   += '<div class="label bg-aqua label-lg flat issues-detail-status-list disappear"> <ul class="list-unstyled"><li onclick="chooseStatusOne( this )" data-username="'+$username+'" data-unique="'+ $id +'" class="pointer">On Progress</li>';
			$html 	   += '<li onclick="chooseStatusTwo( this )" data-username="'+$username+'" data-unique="'+ $id +'" class="pointer">Solved</li></ul></div></div>';
		}else{
			$html 	   += '<div class="pull-right">';
			if ( $status == 'Solved' ) {
				$html 	   += '<div style="position: relative; z-index: 6;"><div style="padding-left:19px" class="label bg-aqua label-lg flat pointer" id="currentStatus" >'+ $status +' </div>';
			}else{
				$html 	   += '<div style="position: relative; z-index: 6;"><div style="padding-left:16px" class="label bg-aqua label-lg flat pointer" id="currentStatus" >'+ $status +' </div>';
			}
			$html 	   += '</div></div>';
		}
		$html 	   += '<div class="clearfix"></div><div class="col-md-12 no-padding">';
		$html 	   += '<div class="label bg-aqua label-lg flat" style="margin-right:20px">Type : '+ $type +'</div>';
		$html 	   += '<div class="label bg-yellow-active label-lg flat">Priority : '+ $priority +'</div></div>';
		$html 	   += '</div>';
		$html 	   += '<div class="issue-detail-body">';
		$html 	   += '<div class="no-filter box box-solid">';
		$html 	   += '<div class="box-body">';
		$html 	   += '<div class="post">';
		$html 	   += '<div class="user-block">';
		$html 	   += '<img class="img-circle img-bordered-sm" src="'+ $image +'" alt="user image">';
		$html 	   += '<span class="username">';
		$html 	   += '<a href="#">'+ $username +'</a></span>';
		$html 	   += '<span class="description">'+ $created +'</span></div>';
		$html 	   += '<p>'+ $description +'</p>';
		$html 	   += '<p id="modified">Last Updated by '+$username+' on '+$modified+'</p></div></div></div>';
		
		$('#sideDetail').html($html);

		// console.log(elem);
		// console.log($id+'\n'+$username+'\n'+$type+'\n'+$name+'\n'+$title+'\n'+$priority+'\n'+$status+'\n'+$description+'\n'+$image+'\n'+$created);
	    
	    if( $('.issue-detail-wrapper').hasClass('open') ) {
	        $('.issue-detail-wrapper').removeClass('open');
	        $('.vi-overlay').removeClass('in');
	    } else {
	        $('.issue-detail-wrapper').addClass('open');
	        $('.vi-overlay').addClass('in');
	    }
	}

	window.closeIssueSeeDetail = function closeIssueSeeDetail( ele ) {
	    
	    if( $('.issue-detail-wrapper').hasClass('open') ) {

	        $('.issue-detail-wrapper').removeClass('open');
	        $('.vi-overlay').removeClass('in');

	    } else {
	        
	        $('.issue-detail-wrapper').addClass('open');
	        $('.vi-overlay').addClass('in');

	    }

	    return false;
	}

	window.currentStatus = function currentStatus( ele ) {
	    
	    if( $( '.issues-detail-status-list' ).hasClass('disappear') ) {

	        $( '.issues-detail-status-list' ).removeClass('disappear');
	        $( '.issues-detail-status-list' ).addClass('appear');
	        $( 'li.pointer' ).css( "color", "white" );

	    } else {

	        $( '.issues-detail-status-list' ).removeClass('appear');
	        $( 'li.pointer' ).css( "color", "transparent" );
	        $( '.issues-detail-status-list' ).addClass('disappear');
	    }
	    return false;
	}

	window.chooseStatusOne = function chooseStatusOne( elem ) {
		var $id 			= $( elem ).attr('data-unique');
		var $username 			= $( elem ).attr('data-username');

	    $( '#modified' ).html( 'Last Updated by '+$username+' on '+dateNow() );
	    $( '#currentStatus' ).html( 'On Progress <span class="fa fa-caret-down"></span>' ).css( 'padding-left', '16px' );
		$.ajax({
		  url: "issues/status?s=on progress&id="+$id
		}).done(function() {
		  issuesProjectTable.api().ajax.reload( null, false );
		});
	}

	window.chooseStatusTwo = function chooseStatusTwo( elem ) {
		var $id 			= $( elem ).attr('data-unique');
		var $username 			= $( elem ).attr('data-username');

	    $( '#modified' ).html( 'Last Updated by '+$username+' on '+dateNow() );
	    $( '#currentStatus' ).html( 'Solved <span style="width:40px" class="fa fa-caret-down"></span>' ).css( 'padding-left', '19px' );
		$.ajax({
		  url: "issues/status?s=solved&id="+$id
		}).done(function() { 
		  issuesProjectTable.api().ajax.reload( null, false );
		});
	}

	window.deleteIssues = function deleteIssues( elem ) {
		showSweetAlert( elem, function(){
			var $id 			= $( elem ).attr('data-unique');
			showLoading('Deleting Item...');
			$.ajax({
			  	url: "issues/deleteData?id="+$id
			}).done(function() { 
				swal.close();
			  	issuesProjectTable.api().ajax.reload( null, false );
		        $('.issue-detail-wrapper').removeClass('open');
		        $('.vi-overlay').removeClass('in');
			});
		});

		return false;
	}

	initDataTable('.bod--ongoing--project', {
		'columns' : [ { "width": "1%" }, null, null, null, null,
				{ 
		    		'className': 'text-center' 
		    	},
		    	{ 
		    		'className': 'text-center' 
		    	},
		    	{ 
		    		'className': 'text-center' 
		    	},
		    	{ 
		    		'className': 'text-center' 
		    	},
		    	{ 
		    		'className': 'text-center' 
		    	},
		    	{ 
		    		'className': 'text-center' 
		    	}]
	});

	initDataTable('.bod--reject--project', {
		'columns' : [ { "width": "1%" }, null, null,
				{ 
		    		'className': 'text-center' 
		    	},
		    	{ 
		    		'className': 'text-center' 
		    	},
		    	{ 
		    		'className': 'text-center' 
		    	},
		    	{ 
		    		'className': 'text-center' 
		    	},
		    	{ 
		    		'className': 'text-center' 
		    	}]
	});

	initDataTable('.bod--finished--project', {
		'columns' : [ { "width": "1%" }, null, null,
				{ 
		    		'className': 'text-center' 
		    	},
		    	{ 
		    		'className': 'text-center' 
		    	},
		    	{ 
		    		'className': 'text-center' 
		    	},
		    	{ 
		    		'className': 'text-center' 
		    	}]
	});

	window.bodSearchHistory = function bodSearchHistory(form) {
		
		var formData = $(form).serializeArray();

		showLoading('Searching...');

		$.ajax({
			type: 'GET',
			data: formData,
			url: $(form).attr('action'),
			success: function (r) {
				
				swal.close();
				$('.timeline.default--result').hide();
				$('.timeline.search--result').html(r).show();
			}
		})
		return false;
	}

	window.checkSearchHistory = function checkSearchHistory(ele) {
		
		var value = $(ele).val();

		if( value == '' ) {
			$('.timeline.default--result').show();
			$('.timeline.search--result').html('').hide();
		}
	}

	initDataTable('.datatable--inventori', {
		'columns' : [ { "width": "1%" }, null, null,
				{ 
		    		'className': 'text-center' 
		    	},
		    	{ 
		    		'className': 'text-center' 
		    	},
		    	{ 
		    		'className': 'text-center' 
		    	},
		    	{ 
		    		'className': 'text-center' 
		    	}]
	});

	function detailInventori() {
		
		var modalEle 	= $('#modal-detail-inventori');
		var loader 		= modalEle.find('.modal-body.modal--loader');
		var content 	= modalEle.find('.modal-body.modal--result');

		window.getDetailInventori = function getDetailInventori( ele ) {

			var dataTarget 	= $(ele).data('url');

			modalEle.modal('show');

			$.get( dataTarget, function( data ) {

				loader.hide();
				content.html( data ).show();
			});

			return false;

		}

		modalEle.on('hidden.bs.modal', function (e) {

			loader.show();
			content.html('').hide();

		});

	} detailInventori();

	$('#datatable').dataTable({
    	"processing": true,
		"serverSide": true,
        "order": [],	
		"ajax": {
			'url' : $('#datatable').attr('data-url'),
			'type': 'POST'
		},
		//Set column definition initialisation properties.
        'columnDefs': [
	        { 
	            'targets': [ 0 ], //first column / numbering column
	        },
        ],
	});

	var thisDataTable = $('#datatable').dataTable();

	window.approvePo = function approvePo( ele ) {
	  var $url 		= $( ele ).attr('data-url');
	  swal({
	    title: 'Purchase Order Approved',
	    text: 'You has been approved a purchase order.',
	    type: 'success'
	  })
	  $(location).attr('href', $url);
	  return false;
	}

	window.rejectPo = function rejectPo( ele ) {
	  var $url 		= $( ele ).attr('data-url');
	  swal({
	    title: 'Reject Purchase Order',
	    text: 'You are about reject a purchase order. Please Add an additional note',
	    input: 'textarea',
	    showCancelButton: true,
	    confirmButtonText: 'Submit',
	    showLoaderOnConfirm: true,
	    preConfirm: function (email) {
	      return new Promise(function (resolve, reject) {
	        setTimeout(function() {
	          if (email === 'taken@example.com') {
	            reject('This email is already taken.')
	          } else {
	            resolve()
	          }
	  		  $(location).attr('href', $url);
	        }, 1000)
	      })
	    },
	    allowOutsideClick: false
	  }).then(function (email) {
	    swal({
	      type: 'success',
	      title: 'Data has been saved'
	    })
	  })

	  return false;
	}

	// -----------------------------------------------------
	// Line Chart in Implementation Page
	// -----------------------------------------------------
	var implementationLineChart = $(".implementation--line--chart");
	if( implementationLineChart.length > 0 ) {

		var urlGet = implementationLineChart.data('url');
		$.get( urlGet, function( r ) {
			$('.gma--chart.gma-section-loading').hide();
			$('.gma--chart.content').show();

			console.log(r)

	        var line_chart = new Chart( implementationLineChart, {
	            type: 'line',
	            data: {
	                labels: r.labels,
	                datasets: [
	                {
	                	fill: false,
	                    label: 'Plan',
	                    data: r.dataPlan,
	                    backgroundColor: '#f44336',
	                    borderColor: '#f44336',
	                    borderWidth: 1
	                },
	                {
	                	fill: false,
	                    label: 'Actual',
	                    data: r.dataActual,
	                    backgroundColor: '#3694F4',
	                    borderColor: '#3694F4',
	                    borderWidth: 1
	                },
	                ]
	            },
	            options: {
	            	legend: {
	            		labels: {
	                		boxWidth: 20
	            		},
	            		position: 'bottom'
	            	},
	                scales: {
	                    yAxes: [{
	                        ticks: {
	                            beginAtZero:true
	                        }
	                    }]
	                }
	            }
	        });
		});
	}

	$(".knob").knob({
		'format' : function (value) {
			return value + '%';
		},
		draw: function () {

	        // "tron" case
	        if (this.$.data('skin') == 'tron') {

	          var a = this.angle(this.cv)  // Angle
	              , sa = this.startAngle          // Previous start angle
	              , sat = this.startAngle         // Start angle
	              , ea                            // Previous end angle
	              , eat = sat + a                 // End angle
	              , r = true;

	          this.g.lineWidth = this.lineWidth;

	          this.o.cursor
	          && (sat = eat - 0.3)
	          && (eat = eat + 0.3);

	          if (this.o.displayPrevious) {
	            ea = this.startAngle + this.angle(this.value);
	            this.o.cursor
	            && (sa = ea - 0.3)
	            && (ea = ea + 0.3);
	            this.g.beginPath();
	            this.g.strokeStyle = this.previousColor;
	            this.g.arc(this.xy, this.xy, this.radius - this.lineWidth, sa, ea, false);
	            this.g.stroke();
	          }

	          this.g.beginPath();
	          this.g.strokeStyle = r ? this.o.fgColor : this.fgColor;
	          this.g.arc(this.xy, this.xy, this.radius - this.lineWidth, sat, eat, false);
	          this.g.stroke();

	          this.g.lineWidth = 2;
	          this.g.beginPath();
	          this.g.strokeStyle = this.o.fgColor;
	          this.g.arc(this.xy, this.xy, this.radius - this.lineWidth + 1 + this.lineWidth * 2 / 3, 0, 2 * Math.PI, false);
	          this.g.stroke();

	          return false;
	        }
        }
	})

});