$(document).ready( function () {

	function toRp(angka){
	    var rev     = parseInt(angka, 10).toString().split('').reverse().join('');
	    var rev2    = '';
	    for(var i = 0; i < rev.length; i++){
	        rev2  += rev[i];
	        if((i + 1) % 3 === 0 && i !== (rev.length - 1)){
	            rev2 += '.';
	        }
	    }
	    return rev2.split('').reverse().join('');
	}

    $('#issues-project').dataTable({
    	"processing": true,
		"serverSide": true,
        "order": [],	
		"ajax": {
			'url' : $('#issues-project').attr('data-url'),
			'type': 'POST'
		},
		//Set column definition initialisation properties.
        'columnDefs': [
	        { 
	            'targets': [ 0 ], //first column / numbering column
	            'orderable': true, //set not orderable
	        },
        ],
	});

    var issuesProjectTable = $('#issues-project').dataTable();
 
    $('#issues-project tbody').on( 'click', 'tr', function () {
        if ( $(this).hasClass('selected') ) {
            $(this).removeClass('selected');
        }
        else {
            issuesProjectTable.$('tr.selected').removeClass('selected');
            $(this).addClass('selected');
        }
    } );
 
 	// select row and do something
    // $('#button').click( function () {
    //     issuesProjectTable.row('.selected').remove().draw( false );
	// } );

	// outside document ready
	window.openDetail = function openDetail( elem ) {
		var $id 			= $( elem ).attr('data-id');
		var $username  		= $( elem ).attr('data-username');
		var $userid  		= $( elem ).attr('data-userid');
		var $login  		= $( elem ).attr('data-login');
		var $type  			= $( elem ).attr('data-type');
		var $name 			= $( elem ).attr('data-name');
		var $title 			= $( elem ).attr('data-title');
		var $priority 		= $( elem ).attr('data-priority');
		var $status 		= $( elem ).attr('data-status');
		var $description 	= $( elem ).attr('data-description');
		var $image  		= $( elem ).attr('data-image');
		var $created  		= $( elem ).attr('data-created');
		var $modified  		= $( elem ).attr('data-modified');
		var $html 	= '<a href="#" class="close-issue" onclick="return closeIssueSeeDetail()"><i class="fa fa-times-circle-o"></i></a>';
		$html 	   += '<div class="issue-detail-header">';
		$html 	   += '<div class="pull-left">';
		$html 	   += '<h3>'+ $name +'</h3>';
		$html 	   += '<p>'+ $title +'</p></div>';
		if ($login === $userid) {
			$html 	   += '<div class="pull-right">';
			if ( $status == 'Solved' ) {
				$html 	   += '<div style="position: relative; z-index: 6;"><div style="padding-left:19px" class="label bg-aqua label-lg flat pointer" id="currentStatus" onclick="currentStatus( this )">'+ $status +' <span style="width:40px" class="fa fa-caret-down"></span></div>';
			}else{
				$html 	   += '<div style="position: relative; z-index: 6;"><div style="padding-left:16px" class="label bg-aqua label-lg flat pointer" id="currentStatus" onclick="currentStatus( this )">'+ $status +' <span style="width:8px" class="fa fa-caret-down"></span></div>';
			}
			$html 	   += '<div class="label bg-yellow-active label-lg flat pointer" onclick="deleteIssues( this )" data-unique="'+ $id +'"><span class="fa fa-trash-o"></span></div></div>';
			$html 	   += '<div class="label bg-aqua label-lg flat issues-detail-status-list disappear"> <ul class="list-unstyled"><li onclick="chooseStatusOne( this )" data-username="'+$username+'" data-unique="'+ $id +'" class="pointer">On Progress</li>';
			$html 	   += '<li onclick="chooseStatusTwo( this )" data-username="'+$username+'" data-unique="'+ $id +'" class="pointer">Solved</li></ul></div></div>';
		}else{
			$html 	   += '<div class="pull-right">';
			if ( $status == 'Solved' ) {
				$html 	   += '<div style="position: relative; z-index: 6;"><div style="padding-left:19px" class="label bg-aqua label-lg flat pointer" id="currentStatus" >'+ $status +' </div>';
			}else{
				$html 	   += '<div style="position: relative; z-index: 6;"><div style="padding-left:16px" class="label bg-aqua label-lg flat pointer" id="currentStatus" >'+ $status +' </div>';
			}
			$html 	   += '</div></div>';
		}
		$html 	   += '<div class="clearfix"></div><div class="col-md-12 no-padding">';
		$html 	   += '<div class="label bg-aqua label-lg flat" style="margin-right:20px">Type : '+ $type +'</div>';
		$html 	   += '<div class="label bg-yellow-active label-lg flat">Priority : '+ $priority +'</div></div>';
		$html 	   += '</div>';
		$html 	   += '<div class="issue-detail-body">';
		$html 	   += '<div class="no-filter box box-solid">';
		$html 	   += '<div class="box-body">';
		$html 	   += '<div class="post">';
		$html 	   += '<div class="user-block">';
		$html 	   += '<img class="img-circle img-bordered-sm" src="'+ $image +'" alt="user image">';
		$html 	   += '<span class="username">';
		$html 	   += '<a href="#">'+ $username +'</a></span>';
		$html 	   += '<span class="description">'+ $created +'</span></div>';
		$html 	   += '<p>'+ $description +'</p>';
		$html 	   += '<p id="modified">Last Updated by '+$username+' on '+$modified+'</p></div></div></div>';
		
		$('#sideDetail').html($html);

		// console.log(elem);
		// console.log($id+'\n'+$username+'\n'+$type+'\n'+$name+'\n'+$title+'\n'+$priority+'\n'+$status+'\n'+$description+'\n'+$image+'\n'+$created);
	    
	    if( $('.issue-detail-wrapper').hasClass('open') ) {
	        $('.issue-detail-wrapper').removeClass('open');
	        $('.vi-overlay').removeClass('in');
	    } else {
	        $('.issue-detail-wrapper').addClass('open');
	        $('.vi-overlay').addClass('in');
	    }
	}

	window.closeIssueSeeDetail = function closeIssueSeeDetail( ele ) {
	    
	    if( $('.issue-detail-wrapper').hasClass('open') ) {

	        $('.issue-detail-wrapper').removeClass('open');
	        $('.vi-overlay').removeClass('in');

	    } else {
	        
	        $('.issue-detail-wrapper').addClass('open');
	        $('.vi-overlay').addClass('in');

	    }

	    return false;
	}

	window.currentStatus = function currentStatus( ele ) {
	    
	    if( $( '.issues-detail-status-list' ).hasClass('disappear') ) {

	        $( '.issues-detail-status-list' ).removeClass('disappear');
	        $( '.issues-detail-status-list' ).addClass('appear');
	        $( 'li.pointer' ).css( "color", "white" );

	    } else {

	        $( '.issues-detail-status-list' ).removeClass('appear');
	        $( 'li.pointer' ).css( "color", "transparent" );
	        $( '.issues-detail-status-list' ).addClass('disappear');
	    }
	    return false;
	}

	window.chooseStatusOne = function chooseStatusOne( elem ) {
		var $id 			= $( elem ).attr('data-unique');
		var $username 			= $( elem ).attr('data-username');

	    $( '#modified' ).html( 'Last Updated by '+$username+' on '+dateNow() );
	    $( '#currentStatus' ).html( 'On Progress <span class="fa fa-caret-down"></span>' ).css( 'padding-left', '16px' );
		$.ajax({
		  url: "issues/status?s=on progress&id="+$id
		}).done(function() {
		  issuesProjectTable.api().ajax.reload( null, false );
		});
	}

	window.chooseStatusTwo = function chooseStatusTwo( elem ) {
		var $id 			= $( elem ).attr('data-unique');
		var $username 			= $( elem ).attr('data-username');

	    $( '#modified' ).html( 'Last Updated by '+$username+' on '+dateNow() );
	    $( '#currentStatus' ).html( 'Solved <span style="width:40px" class="fa fa-caret-down"></span>' ).css( 'padding-left', '19px' );
		$.ajax({
		  url: "issues/status?s=solved&id="+$id
		}).done(function() { 
		  issuesProjectTable.api().ajax.reload( null, false );
		});
	}

	window.deleteIssues = function deleteIssues( elem ) {
		showSweetAlert( elem, function(){
			var $id 			= $( elem ).attr('data-unique');
			showLoading('Deleting Item...');
			$.ajax({
			  	url: "issues/deleteData?id="+$id
			}).done(function() { 
				swal.close();
			  	issuesProjectTable.api().ajax.reload( null, false );
		        $('.issue-detail-wrapper').removeClass('open');
		        $('.vi-overlay').removeClass('in');
			});
		});

		return false;
	}

	$('#datatable').dataTable({
    	"processing": true,
		"serverSide": true,
        "order": [],	
		"ajax": {
			'url' : $('#datatable').attr('data-url'),
			'type': 'POST'
		},
		//Set column definition initialisation properties.
        'columnDefs': [
	        { 
	            'targets': [ 0 ], //first column / numbering column
	        },
        ],
	});

	var thisDataTable = $('#datatable').dataTable();

	$('#detailStock').on('show.bs.modal', function (event) {
		$('#list').html('');

		var button = $(event.relatedTarget) // Button that triggered the modal
		var $id = button.data('id') // Extract info from data-* attributes
		var $url = button.data('url') // Extract info from data-* attributes
		var $name = button.data('name') // Extract info from data-* attributes
		var $stock = button.data('stock') // Extract info from data-* attributes
		var $need = button.data('need') // Extract info from data-* attributes
		var $unit = button.data('unit') // Extract info from data-* attributes
		var $availabelity = button.data('stockist') // Extract info from data-* attributes

		if ($availabelity < 0) { var $stockist = '<span style="color:red">'+ $availabelity +' '+ $unit +'</span>' }
		else {  var $stockist = '<span style="color:green">'+ $availabelity +' '+ $unit +'</span>'  }

		// If necessary, you could initiate an AJAX request here (and then do the updating in a callback).
		// Update the modal's content. We'll use jQuery here, but you could use a data binding library or other methods instead.
		var modal = $(this)
	  	modal.find('#code').text($id)
	  	modal.find('#name').text($name)
	  	modal.find('#stock').text($stock +' '+ $unit)
	  	modal.find('#need').text($need +' '+ $unit)
	  	modal.find('#instock').html($stockist)

		$('#loading').html('<center><img src="'+LOADERGIF+'" style="width:50px" class="img-circle"><p>Retrieving data...</p></center>');

		$.ajax({
		  	url: $url+$id,
		  	method: 'POST'
		}).done(function(r) { 
			$('#list').html(r);
			$('#loading').html('');
		});
	})

  	$('textarea').summernote({
  		height: 180,
		toolbar: [
		// [groupName, [list of button]]
			['style', ['bold', 'italic', 'underline', 'clear']],
			['fontsize', ['fontsize']],
			['para', ['ul', 'ol', 'paragraph']],
			['height', ['height']]
		]
  	})

	// purchase list page start
	$('#amount,#qty,#price,#additional_desc_item~.note-editor,#payment_term,#delivery,#additional_desc_po~.note-editor,#pic,#fob').hide();
	$('#amount').val($('#qty').val() * $('#price').val())
	$('#amountShadow').text(toRp($('#qty').val() * $('#price').val()))
	$('#createPO').on('show.bs.modal', function (event) {
		var button = $(event.relatedTarget) // Button that triggered the modal
		var $url  = button.data('url') // Extract info from data-* attributes
		var $name = button.data('name') // Extract info from data-* attributes
		var $unit = button.data('unit') // Extract info from data-* attributes
		var $cost = button.data('cost') // Extract info from data-* attributes
		var $need = button.data('need') // Extract info from data-* attributes
		var $itemCode = button.data('itemcode') // Extract info from data-* attributes
		var modal = $(this)
	  	modal.find('#item').val($name+' ('+$unit+') ')
	  	modal.find('#price').val($cost)
	  	modal.find('#itemCode').val($itemCode)
	  	modal.find('#qty').val($need/2)
	  	modal.find('#priceShadow').text(toRp($cost))
	  	modal.find('#qtyShadow').text(toRp($need/2))
	  	modal.find('#qty').attr({min:'1',max:$need})
	  	modal.find('form').attr({action:$url,method:'post'})
	})
	$('#commitPO').on('click', function(e){
		var $form = $('#createPO').find('form');
		$.ajax({
		  	url: $form[0].action,
		  	method: 'POST',
		  	data: $form.serialize()
		}).done(function() { 
			location.reload();
		});
	})
	$('#createPO').on('hidden.bs.modal', function(){
		showSweetConfirm( this, function(){
	    	$('#formPO')[0].reset();
		});
		return false;
	});
	window.rangeInputNumber = function rangeInputNumber( elem ) {
		var max = parseInt($(elem).attr('max'));
		var min = parseInt($(elem).attr('min'));
		var shadow = '#'+elem.id+'Shadow';

		if (max != undefined || max != null || max != '') {
			if ($(elem).val() > max)
			{
			  $(elem).val(max);
			  $(shadow).text(toRp(max))
			}
			else if ($(elem).val() < min)
			{
			  $(elem).val(min);
			  $(shadow).text(toRp(min))
			}     
			else
			{
			  $(shadow).text(toRp($(elem).val()))
			}  
		}else{
			if ($(elem).val() < min)
			{
			  $(elem).val(min);
			  $(shadow).text(toRp(min))
			}     
			else
			{
			  $(shadow).text(toRp($(elem).val()))
			}  
		}
	}
	window.editVal = function editVal( elem ) {
		$(elem).parent().siblings('input,.note-editor').slideDown( "slow" );
		$(elem).siblings(elem).fadeIn( "slow" );
		$(elem).hide();
	}
	window.doneVal = function doneVal( elem ) {
		$(elem).parent().siblings('input,.note-editor').slideUp( "slow" );
		$(elem).siblings(elem).fadeIn( "slow" );
		$(elem).hide();
	}
	window.dpVal = function dpVal( elem ) {
		$('#dp').slideDown( "slow" );
		$('#payment_percent').attr( "required",'true' );
		$('#payment_note_full').slideUp( "slow" );
		$('#payment_note_full').find('input').val('');
		$('#payment_note_full').siblings(elem).children('.fa-check-square-o').hide();
		$('#payment_note_full').siblings(elem).children('.fa-edit').fadeIn();
	}
	window.lunasVal = function lunasVal( elem ) {
		var $price = $('#price').val()
		var $qty = $('#qty').val()
		var $amount = $qty * $price

		$('#dp').slideUp( "slow" );
		$('#dp').find('input').val('');
		$('#debtShadow').text('');
		$('#amount').val($amount)
		$('#amountShadow').text(toRp($amount));
	}
	window.lunasEditVal = function lunasEditVal( elem ) {
		$('#payment_note_full').slideDown( "slow" );
		$(elem).siblings(elem).fadeIn( "slow" );
		$(elem).hide();
	}
	window.lunasDoneVal = function lunasDoneVal( elem ) {
		$('#payment_note_full').slideUp( "slow" );
		$(elem).siblings(elem).fadeIn( "slow" );
		$(elem).hide();
	}
	$('#qty,#price,#payment_percent').on('keyup, keydown', function(elem){
		// Allow: backspace, delete, tab, escape, enter and .
        if ($.inArray(elem.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
             // Allow: Ctrl+A, Command+A
            (elem.keyCode === 65 && (elem.ctrlKey === true || elem.metaKey === true)) || 
             // Allow: home, end, left, right, down, up
            (elem.keyCode >= 35 && elem.keyCode <= 40)) {
                 // let it happen, don't do anything
                 return;
        }
        // Ensure that it is a number and stop the keypress
        if ((elem.shiftKey || (elem.keyCode < 48 || elem.keyCode > 57)) && (elem.keyCode < 96 || elem.keyCode > 105)) {
            elem.preventDefault();
        }
	})
	$('#payment_term,#delivery,#pic,#fob').on('keyup', function(){
		var input = $(this).val();
		var shadow = '#'+this.id+'Shadow';

		$(shadow).html(input);
	})
	$('#qty,#price,#payment_percent').on('keyup', function(){
		var $payment_percent = $('#payment_percent').val()
		var $qty = $('#qty').val()
		var $price = $('#price').val()
		var $amount = $qty * $price
		var $dpamount 	= ( $payment_percent ) / 100 * $amount
		var $debt 		= ( 100 - $payment_percent ) / 100 * $amount

		$('#qtyShadow').text(toRp($qty))
		$('#priceShadow').text(toRp($price))

		if( $payment_percent != ''){
			$('#amount').val($dpamount)
			$('#amountShadow').text(toRp($dpamount))
			$('#debtShadow').text('Debt : '+toRp($debt))
		}else{
			$('#amount').val($amount)
			$('#amountShadow').text(toRp($amount))
		}
	})
	$('#price').on('focus', function(){
		var default_value = 0;
        if($(this).val() == default_value)
	        {
	             $(this).val("");
	        }
	})
	$('#price').on('blur', function(){
		var default_value = 0;
        if($(this).val().length == 0)
        {
            $(this).val(default_value);
        }
    });
    window.getPONumber = function getPONumber( elem ) {
    	var $this = $(elem);
	    swal(
	      'Purchase Order Number is',
	      $this.attr('data-nomor'),
	      'success'
	    )  
	    return false;
	}
	window.getPrint = function getPrint( elem ) {
		var $this = $( elem )
		var $target = $this.attr('data-target')
		var $printContents = document.getElementById($target).innerHTML;
		var $originalContents = document.body.innerHTML;

		document.body.innerHTML = $printContents;

		window.print();

		document.body.innerHTML = $originalContents;
	}
    // purchase list end


    //material list start
    var unit_url = $("#mi_unit").attr('data-url');
    $("#mi_unit").select2({
    	tags: true,
		width: 'resolve',
		ajax: {
			url: unit_url,
			dataType: 'json',
			delay: 250,
		},
		placeholder: 'ex. Metre, Gram',
		minimumInputLength: 1,
	});

    $('#select2-mi_unit-container').attr('style','margin-top : -7px');
    $('.select2-selection--single').attr('style','border-radius: 0px;');

    var edit_unit_url = $("#mi_edit_unit").attr('data-url');
    $("#mi_edit_unit").select2({
    	tags: true,
		width: 'resolve',
		ajax: {
			url: edit_unit_url,
			dataType: 'json',
			delay: 250,
		},
		placeholder: 'ex. Metre, Gram',
		minimumInputLength: 1,
	});

    $('#select2-mi_edit_unit-container').attr('style','margin-top : -7px');

  //   $('#mi_item').on('keyup', function(){
  //   	var item_url = $("#mi_item").attr('data-url');
  //   	$("#mi_code").val('generating...');
		// $.ajax({
		//   	url: item_url,
		//   	method: 'POST',
		//   	data: "item=" + $("#mi_item").val() + ' ',
		// }).done(function(response) { 
		// 	$("#mi_code").val(response);
		// });
  //   });

    $('#mi_item').on('keyup', function(){
    	var item_url = $("#mi_item").attr('data-check');
    	$("#mi_code_check").text('generating...');
		$.ajax({
		  	url: item_url,
		  	method: 'POST',
		  	data: "item=" + $("#mi_item").val(),
		}).success(function(response) { 
			$("#mi_code_check").html(response);
		});
    });

    $('#saveMaterial').on('click',function(){
    	var addForm_url = $("#mi_addForm").attr('action');
		showLoading('Saving Data...');
		$.ajax({
		  	url: addForm_url,
		  	method: 'POST',
		  	data: $('#mi_addForm').serialize(),
		}).done(function() { 
			$("#addMaterial").modal('hide')
			swal.close();
		  	thisDataTable.dataTable().api().ajax.reload( null, false );
		});
    });

    $('#applyEditMaterial').on('click',function(){
    	var url = $("#editMaterial").find('form').attr('action');
		showLoading('Updating Data...');
		$.ajax({
		  	url: url,
		  	method: 'POST',
		  	data: $("#editMaterial").find('form').serialize(),
		}).done(function() { 
			$("#editMaterial").modal('hide')
			swal.close();
		  	thisDataTable.dataTable().api().ajax.reload( null, false );
		});
    });

	$('#editMaterial').on('show.bs.modal', function (event) {
		var button 	= $(event.relatedTarget) // Button that triggered the modal
		var $url 	= button.data('url') // Extract info from data-* attributes
		var $name 	= button.data('name') // Extract info from data-* attributes
		var $id 	= button.data('id') // Extract info from data-* attributes
		var $unit 	= button.data('unit') // Extract info from data-* attributes
		var $cost 	= button.data('cost') // Extract info from data-* attributes
		// If necessary, you could initiate an AJAX request here (and then do the updating in a callback).
		// Update the modal's content. We'll use jQuery here, but you could use a data binding library or other methods instead.
		var modal = $(this)
		modal.find('.modal-title').text('Update for ' + $name)
		$('#mi_edit_unit').append('<option value="'+$unit+'" selected="selected">'+$unit+'</option>');
		$('#mi_edit_unit').trigger('change');
		modal.find('#mi_edit_item').val($name)
		modal.find('#mi_edit_code').val($id)
		modal.find('#mi_edit_cost').val($cost)
		modal.find('form').attr('action',$url)	
	})

	window.deleteMaterial = function deleteMaterial( elem ) {
		showSweetAlert( elem, function(){
			var $id 			= $( elem ).attr('data-id');
			showLoading('Deleting Item...');
			$.ajax({
			  	url: "material_list/delete?id="+$id
			}).done(function() { 
				swal.close();
			  	thisDataTable.api().ajax.reload( null, false );
			});
		});

		return false;
	}

    //material list end


    //service list start
    var unit_url_s = $("#s_unit").attr('data-url');
    $("#s_unit").select2({
    	tags: true,
		width: 'resolve',
		ajax: {
			url: unit_url_s,
			dataType: 'json',
			delay: 250,
		},
		placeholder: 'ex. Metre, Gram',
		minimumInputLength: 1,
	});

    $('#select2-s_unit-container').attr('style','margin-top : -7px');
    $('.select2-selection--single').attr('style','border-radius: 0px;');

    var edit_unit_url_s = $("#s_edit_unit").attr('data-url');
    $("#s_edit_unit").select2({
    	tags: true,
		width: 'resolve',
		ajax: {
			url: edit_unit_url_s,
			dataType: 'json',
			delay: 250,
		},
		placeholder: 'ex. Metre, Gram',
		minimumInputLength: 1,
	});

    $('#select2-s_edit_unit-container').attr('style','margin-top : -7px');

    var material_url_s = $("#s_material").attr('data-url');
    $("#s_material").select2({
		width: 'resolve',
		ajax: {
			url: material_url_s,
			dataType: 'json',
			delay: 250,
		},
		placeholder: 'Material\'s Name',
		minimumInputLength: 1,
	});

    $('#select2-s_material-container').attr('style','margin-top : -7px');
    $('.select2-selection--single').attr('style','border-radius: 0px;');

    var edit_material_url_s = $("#s_edit_material").attr('data-url');
    $("#s_edit_material").select2({
		width: 'resolve',
		ajax: {
			url: edit_material_url_s,
			dataType: 'json',
			delay: 250,
		},
		placeholder: 'Material\'s Name',
		minimumInputLength: 1,
	});

    $('#select2-s_edit_material-container').attr('style','margin-top : -7px');

  //   $('#s_item').on('blur', function(){
  //   	var item_url = $("#s_item").attr('data-url');
  //   	$("#s_code").val('generating...');
		// $.ajax({
		//   	url: item_url,
		//   	method: 'POST',
		//   	data: "item=" + $("#s_item").val() + ' ',
		// }).done(function(response) { 
		// 	$("#s_code").val(response);
		// });
  //   });

    $('#s_item').on('keyup', function(){
    	var item_url = $("#s_item").attr('data-check');
    	$("#s_code_check").text('generating...');
		$.ajax({
		  	url: item_url,
		  	method: 'POST',
		  	data: "item=" + $("#s_item").val(),
		}).success(function(response) { 
			$("#s_code_check").html(response);
		});
    });

    $('#s_edit_item').on('blur', function(){
    	var item_url = $("#s_edit_item").attr('data-url');
    	$("#s_code").val('generating...');
		$.ajax({
		  	url: item_url,
		  	method: 'POST',
		  	data: "item=" + $("#s_edit_item").val() + ' ',
		}).done(function(response) { 
			$("#s_edit_code").val(response);
		});
    });

    $('#s_edit_item').on('keyup', function(){
    	var item_url = $("#s_edit_item").attr('data-check');
    	$("#s_code_check").text('generating...');
		$.ajax({
		  	url: item_url,
		  	method: 'POST',
		  	data: "item=" + $("#s_edit_item").val(),
		}).success(function(response) { 
			$("#s_code_check").html(response);
		});
    });

    $('#saveService').on('click',function(){
    	var addForm_url = $("#s_addForm").attr('action');
		showLoading('Saving Data...');
		$.ajax({
		  	url: addForm_url,
		  	method: 'POST',
		  	data: $('#s_addForm').serialize(),
		}).done(function() { 
			$("#addService").modal('hide')
			swal.close();
		  	thisDataTable.dataTable().api().ajax.reload( null, false );
		});
    });

    $('#applyEditService').on('click',function(){
    	var url = $("#editService").find('form').attr('action');
		showLoading('Updating Data...');
		$.ajax({
		  	url: url,
		  	method: 'POST',
		  	data: $("#editService").find('form').serialize(),
		}).done(function() { 
			$("#editService").modal('hide')
			swal.close();
		  	thisDataTable.dataTable().api().ajax.reload( null, false );
		});
    });

	$('#viewService').on('show.bs.modal', function (event) {
		var button 	= $(event.relatedTarget) // Button that triggered the modal
		var $url 	= button.data('url') // Extract info from data-* attributes
		var $name 	= button.data('name') // Extract info from data-* attributes
		var $id 	= button.data('id') // Extract info from data-* attributes
		var $unit 	= button.data('unit') // Extract info from data-* attributes
		var $cost 	= button.data('cost') // Extract info from data-* attributes
		// If necessary, you could initiate an AJAX request here (and then do the updating in a callback).
		// Update the modal's content. We'll use jQuery here, but you could use a data binding library or other methods instead.
		var modal = $(this)
		modal.find('.modal-title').text($name)
		modal.find('#v_item').val($name)
		modal.find('#v_code').val($id)
		modal.find('#v_unit').val($unit)
		modal.find('#v_cost').val( formatRupiah($cost,false) )
		showLoading('Get Related Materials...')

		$.ajax({
			url: $url,
		})
		.done(function(r) {
			console.log(r)
			modal.find('#v_related').html(r)
			swal.close();
		})
		
		modal.find('#v_cost').val( formatRupiah($cost,false) )
	})

	window.deleteService = function deleteService( elem ) {
		showSweetAlert( elem, function(){
			var $id 			= $( elem ).attr('data-id');
			showLoading('Deleting Item...');
			$.ajax({
			  	url: "service_list/delete?id="+$id
			}).done(function() { 
				swal.close();
			  	thisDataTable.api().ajax.reload( null, false );
			});
		});

		return false;
	}

    //service list end



    window.approveRequestMaterial = function approveRequestMaterial( ele ) {
		showSweetConfirm( ele, function(){
			$link = $(ele).attr('data-target');
			$.ajax({
				url: $link
			})
			.done(function( r ) {
				// if ( r == 'ok'){
					
				// }
				swal.close();
			  	$('#assigned-list').dataTable().api().ajax.reload( null, false );
			})			
		});
	};

	window.rejectRequestMaterial = function rejectRequestMaterial( ele ) {
		showSweetConfirm( ele, function(){
			$link = $(ele).attr('data-target');
			$.ajax({
				url: $link
			})
			.done(function() {
				swal.close();
			  	$('#assigned-list').dataTable().api().ajax.reload( null, false );
			})			
		});
	};

	// =========================================
	// add po + add service start
	// =========================================
	var count = 1;

    window.addMore = function addMore( ele ) {
	    $( ele ).hide();
	    $( ele ).next('#delete').show();
		$( ele ).find('#select2--selector').remove();
	    
	    clone = $("#itemListForm").clone();  
	    clone.find('#select2--selector').attr('id', function(i, val) {
            return val + count;
        });

        clone.find( '#add' ).show();
        clone.find( '#delete' ).hide();
        if (clone.find('#select2--selector'+count).next().hasClass('select2-container')) {
		  clone.find('#select2--selector'+count).next().remove();
		}
        clone.find('#select2--selector'+count).val('').trigger('change');
        clone.insertAfter($( ele ).closest("#itemListForm"));

		$('#select2--selector'+count).select2({
			width: 'resolve',
			ajax: {
				url:  $( '#select2--selector'+count ).data('url'),
				dataType: 'json',
				delay: 250,
			},
			placeholder: 'Item Name',
			minimumInputLength: 1,
		});
		$('#select2-select2--selector'+count+'-container').attr('style','margin-top : -7px;');
        
	    $('.select2-selection--single').attr('style','border-radius: 0px; height : 34px');
        count++;
	}
	
    window.deleteThis = function deleteThis( ele ) {
	    $( ele ).closest("#itemListForm").remove();  
	}

	var select2url = $('#select2--selector').data('url');
	$('#select2--selector').select2({
		width: 'resolve',
		ajax: {
			url: select2url,
			dataType: 'json',
			delay: 250,
		},
		placeholder: 'Item Name',
		minimumInputLength: 1,
	});

	$('#select2-select2--selector-container').attr('style','margin-top : -7px;');
    $('.select2-selection--single').attr('style','border-radius: 0px; height : 34px');
    // =========================================
    // add PO + add service end
    // =========================================

    // =========================================
    // edit service start
    // =========================================
 	window.addMoreE = function addMoreE( ele ) {
	    $( ele ).hide();
	    $( ele ).next('#delete').show();
		$( ele ).find('#select2--selector--edit').remove();
	    
	    clone = $("#itemListFormE").clone();  
	    clone.find('#select2--selector--edit').attr('id', function(i, val) {
            return val + count;
        });

        clone.find( '#add' ).show();
        clone.find( '#delete' ).hide();
        if (clone.find('#select2--selector--edit'+count).next().hasClass('select2-container')) {
		  clone.find('#select2--selector--edit'+count).next().remove();
		}
        clone.find('#select2--selector--edit'+count).val('').trigger('change');
        clone.insertAfter( $(ele).closest("#itemListFormE"));

		$('#select2--selector--edit'+count).select2({
			width: 'resolve',
			ajax: {
				url:  $( '#select2--selector--edit'+count ).data('url'),
				dataType: 'json',
				delay: 250,
			},
			placeholder: 'Item Name',
			minimumInputLength: 1,
		});
		$('#select2-select2--selector--edit'+count+'-container').attr('style','margin-top : -7px;');
        
        count++;
	}
	
    window.deleteThisE = function deleteThisE( ele ) {
	    $( ele ).closest("#itemListFormE").remove();  
	}

	var select2url = $('#select2--selector--edit').data('url');
	$('#select2--selector--edit').select2({
		width: 'resolve',
		ajax: {
			url: select2url,
			dataType: 'json',
			delay: 250,
		},
		placeholder: 'Item Name',
		minimumInputLength: 1,
	});

	$('#select2-select2--selector--edit-container').attr('style','margin-top : -7px;');
	//=========================================
	// edit service end
	//========================================= 

    // Select2 for Material Assignment Project
	var unit_url = $("#request-material-project").attr('data-url');
	$('.request-material-project').select2({
		width: 'resolve',
		ajax: {
			url: unit_url,
			dataType: 'json',
			delay: 250,
		},
		placeholder: 'Project',
		minimumInputLength: 2,
	});
	$('#select2-request-material-project-container').attr('style','margin-top : -7px');
    $('.select2-selection--single').attr('style','border-radius: 0px;');

     // Material Request By
    $("#request-material-project").change(function(){

    	$('#request-material-item').empty();

    	var id_project = $("#request-material-project").val();


		var unit_url = "materialassignment/getMaterial/"+id_project;


		$('.request-material-item').select2({
			width: 'resolve',
			ajax: {
				url: unit_url,
				dataType: 'json',
				delay: 250,
			},
			placeholder: 'Item Name',
		});
		$('#select2-request-material-item-container').attr('style','margin-top : -7px');
	    $('.select2-selection--single').attr('style','border-radius: 0px;');
    });

    // ===========================================
    // Default Data Table
    // ===========================================
    var unit_url = $('#assigned-list').attr('data-url');

    $('#assigned-list').dataTable({
    	"paging": false,
    	"processing": true,
		"serverSide": true,
        "order": [],	
		"ajax": {
			'url' : unit_url,
			'type': 'POST'
		},
		//Set column definition initialisation properties.
        'columnDefs': [
	        { 
	            'targets': [ 0 ], //first column / numbering column
	            'orderable': true, //set not orderable
	        },
        ],
	});

    // ===========================================
    var unit_url = $("#filter-rfs").attr('data-url');
	$('.filter-rfs').select2({
		width: 'resolve',
		ajax: {
			url: unit_url,
			dataType: 'json',
			delay: 250,
		},
		placeholder: 'RFS Date',
	});
	$('#select2-filter-rfs-container').attr('style','margin-top : -7px');
    $('.select2-selection--single').attr('style','border-radius: 0px;');

    // ===========================================
    $("#filter-rfs").change(function(){

    	$('#filter-area').empty();

    	var rfs_date = $("#filter-rfs").val();


		var unit_url = "materialassignment/getArea/"+rfs_date;


		$('.filter-area').select2({
			width: 'resolve',
			ajax: {
				url: unit_url,
				dataType: 'json',
				delay: 250,
			},
			placeholder: 'Area Name',
		});
		$('#select2-filter-area-container').attr('style','margin-top : -7px');
	    $('.select2-selection--single').attr('style','border-radius: 0px;');
    });

    // ============================================
    $("#filter-area").change(function(){

    	$('#filter-client').empty();
    	var rfs_date = $("#filter-rfs").val();
    	var id_prov = $("#filter-area").val();


		var unit_url = "materialassignment/getClient/"+rfs_date+"/"+id_prov;
		


		$('.filter-client').select2({
			width: 'resolve',
			ajax: {
				url: unit_url,
				dataType: 'json',
				delay: 250,
			},
			placeholder: 'Client',
		});
		$('#select2-filter-client-container').attr('style','margin-top : -7px');
	    $('.select2-selection--single').attr('style','border-radius: 0px;');
    });

    // ============================================
    $("#filter-client").change(function(){

    	$('#filter-project').empty();
    	var rfs_date = $("#filter-rfs").val();
    	var id_prov = $("#filter-area").val();
    	var client = $("#filter-client").val();


		var unit_url = "materialassignment/getProjectFilter/"+rfs_date+"/"+id_prov+"/"+client;
		


		$('.filter-project').select2({
			width: 'resolve',
			ajax: {
				url: unit_url,
				dataType: 'json',
				delay: 250,
			},
			placeholder: 'Project',
		});
		$('#select2-filter-project-container').attr('style','margin-top : -7px');
	    $('.select2-selection--single').attr('style','border-radius: 0px;');
    });

    // ============================================
    $("#applyfilterAssignmentList").on('click', function(){

    	// $('#assigned-list').empty();
		// $("#assigned-list").dataTable().fnClearTable();
		$("#assigned-list").dataTable().fnDestroy();
		
    	// table.destroy();
    	var rfs_date = $("#filter-rfs").val();
    	var id_prov = $("#filter-area").val();
    	var client = $("#filter-client").val();
    	var project = $("#filter-project").val();


		var unit_url = $('#assigned-list').attr('data-url')+rfs_date+"/"+id_prov+"/"+client+"/"+project;

	    $('#assigned-list').dataTable({
	    	"paging": false,
	    	"processing": true,
			"serverSide": true,
	        "order": [],	
			"ajax": {
				'url' : unit_url,
				'type': 'POST'
			},
			//Set column definition initialisation properties.
	        'columnDefs': [
		        { 
		            'targets': [ 0 ], //first column / numbering column
		            'orderable': true, //set not orderable
		        },
	        ],
		});
		
		$('#filterAssignmentList').modal('hide')

    });

    window.approveFPD = function approveFPD( elem ) {
		var $this = $( elem )
		var $url = $this.attr('data-target')
		$.ajax({
			url: $url,
			type: 'POST'
		})
		.done(function() {
			location.reload();
		});
	}

	$('#saveCek').on('click',function(){
    	var addForm_url = $("#cek_addForm").attr('action');
		showLoading('Saving Data...');
		$.ajax({
		  	url: addForm_url,
		  	method: 'POST',
		  	data: $('#cek_addForm').serialize(),
		}).done(function() { 
			$("#saveCek").modal('hide')
			swal.close();
		  	thisDataTable.dataTable().api().ajax.reload( null, false );
		});
	});

    $('#button--submit').click( function(event) {
    	button 	= $(this)
    	href 	= button.data('anchor')
    	form 	= button.closest('form')
		showSweetConfirm( form, function(){
			form.submit();
		});
		return false;
    });
});

