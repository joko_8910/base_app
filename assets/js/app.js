// -----------------------------------------------------
// Modern confirmation dialog
// Use this for pre-delete an item
// 
// usage:
// 1. Use inside tag html
// <a onclick="return showSweetAlert(this)" data-alert-mesage="Message in dialog">delete</a>
//
// 2. Use inside javascript, e.g inside function
// function delete(ele){
// 		showSweetAlert( ele, function(){
// 			
// 			you custom function or any action after user click aggree to delete
// 		});
// }
// -----------------------------------------------------
function showSweetAlert( elem, customFunction, dismisFunction ) {

	var msg 		= $(elem).attr('data-alert-message');
	var checkAttr 	= $(elem).attr('data-alert-message'); 

	swal({
	  title: 'Are you sure?',
	  text: ( msg == 'undefined' || msg == null ) ? "You won't be able to revert this!" : msg,
	  type: 'warning',
	  showCancelButton: true,
	  confirmButtonColor: '#3085d6',
	  cancelButtonColor: '#d33',
	  confirmButtonText: 'Yes, delete it!',
	  cancelButtonText: 'No, cancel!'
	}).then(function () {

		if( undefined != customFunction ) {
    		customFunction.call( $(this) );
		}

    	if (typeof checkAttr !== typeof undefined && checkAttr !== false) {
			window.location = $(elem).attr('href');
		}

	}, function (dismiss) {

		if( undefined != dismisFunction ) {
			
    		dismisFunction.call( $(this) );

		} else {

			swal.close();
		}

	});
	
	return false;

}

// -----------------------------------------------------
// Modern confirmation dialog
// Use this for pre-action an item exclude delete action
// 
// usage:
// 1. Use inside tag html
// <a onclick="return showSweetAlert(this)" data-alert-mesage="Message in dialog">delete</a>
//
// 2. Use inside javascript, e.g inside function
// function delete(ele){
// 		showSweetAlert( ele, function(){
// 			
// 			your custom function or any action after user click aggree to delete
// 		});
// }
// -----------------------------------------------------
function showSweetConfirm( elem, customFunction ) {

	var msg 		= $(elem).attr('data-alert-message');
	var checkAttr 	= $(elem).attr('data-alert-message'); 

	swal({
	  title: 'Are you sure?',
	  text: ( msg == 'undefined' || msg == null ) ? "Are you sure want to do this?" : msg,
	  type: 'warning',
	  showCancelButton: true,
	  confirmButtonColor: '#3085d6',
	  cancelButtonColor: '#d33',
	  confirmButtonText: 'Yes, I Am sure',
	  cancelButtonText: 'No, I am not'
	}).then(function () {

		if( undefined != customFunction ) {
			customFunction();
		}

    	if (typeof checkAttr !== typeof undefined && checkAttr !== false) {
			window.location = $(elem).attr('href');
		}

	}, function (dismiss) {

		swal.close();

	});
	
	return false;

}


function selectRedirectTo( ele, baseUrl ) {

	var theUrl = ele.value;
	
	if( theUrl != '-' ) {
		window.location = baseUrl + '/' + theUrl;
	}
}

// -----------------------------------------------------
// Konversi angka menjadi format rupiah
// -----------------------------------------------------
function formatRupiah(angka, withRp = true ){
    var rev     = Math.ceil(angka, 10).toString().split('').reverse().join('');
    var rev2    = '';
    for(var i = 0; i < rev.length; i++){
        rev2  += rev[i];
        if((i + 1) % 3 === 0 && i !== (rev.length - 1)){
            rev2 += '.';
        }
    }

    if( withRp != true ) {
	    return rev2.split('').reverse().join('');
    } else {
	    return 'Rp ' + rev2.split('').reverse().join('');
    }
}


// -----------------------------------------------------
// Modal preloader
// Use this when you are executing an ajax
// While system proccess the ajax, just show this modal
// -----------------------------------------------------
function showLoading( msg ) {

	swal({
	  text: msg,
	  imageUrl: LOADERGIF,
	  imageWidth: 90,
	  imageAlt: 'Loading',
	  showConfirmButton: false,
	  allowEscapeKey: false,
	  allowOutsideClick: false,
	  customClass: 'gma-swal-loader'
	});
}

// -----------------------------------------------------
// Modal Error
// Use this when you want display error dialog
// -----------------------------------------------------
function showError( msg ) {
	return swal(
	  'Oops...',
	 	msg,
	  'error'
	)
}

function showSuccess( msg ) {
	return swal({
		title: 'Success',
		text: msg,
		type: 'success'
	});
}

$(document).ready( function () {

	function enableSentServer() {
	
		if (typeof URLSTREAM == 'undefined') {
			return false;
		}

		var stream = new EventSource( URLSTREAM );

		stream.addEventListener('notification', function (event) {

		    var response = JSON.parse(event.data);
		    var data;

		    if( response.dm != null ) {

		    	data = response.dm;
			    swal({	
				  title: 'New Message',
				  html: data.message,
				  imageUrl: BASEURL + 'assets/images/letter.png',
				  imageWidth: 90,
				  imageAlt: 'Loading',
				  padding: 0,
				  allowEscapeKey: false,
				  allowOutsideClick: false,
				  customClass: 'gma-swal-inbox',
				  confirmButtonText: 'I have read this message'
				}).then(function (name) {

					preloaderPage();
					location.reload();

				});

		    }

		    if( response.notif != null ) {

		    	data = response.notif;

		  		  	Push.create("GMA Project", {
				    body: data.message,
				    // icon: '/icon.png',
				    timeout: 4000,
				    onClick: function () {
				        window.focus();
				        this.close();
				    }
				});
		    }

		});

	}

	// You can disable or enable live notification by commenting code below
	enableSentServer();

	$.fn.modal.Constructor.prototype.enforceFocus = function () {};

	// -----------------------------------------------------
	// Initiate iCheck Style for checkbox and radio
	// -----------------------------------------------------
    $('input[type="checkbox"].icheck, input[type="radio"].icheck').iCheck({
      checkboxClass: 'icheckbox_flat-blue',
      radioClass   : 'iradio_flat-blue'
    });


    // -----------------------------------------------------
	// Initiate Datepicker
	// -----------------------------------------------------
	$('.datepicker').datepicker({
		format: 'yyyy-mm-dd'
	});


	// -----------------------------------------------------
	// Custom Initate Select2
	// Use this function if you wanna use select2 for progress search or advanced select search
	// This select was use remote data. So make sure you have understand how to do this
	// 
	// Usage:
	// initSelect2Ajax(<your html class name>, <function after select2 was selected. this optional>)
	// 
	// eg. 
	// We have html like this :
	// <select class="cari-provinsi"></select>
	// 
	// So, in our javascript gonna be like this
	// initSelect2Ajax( '.cari-provinsi');
	// -----------------------------------------------------
	window.initSelect2Ajax = function initSelect2Ajax( className, extraFunction, extendOption ) {

		var defautltOption = {
			width: 'resolve',
		 	theme: "bootstrap",
		 	placeholder: $(className).data('placeholder'),
		 	minimumInputLength: 2,
		 	ajax: {
			    url: $(className).attr('data-url'),
			    dataType: 'json',
			    data: function (params) {
			      var query = {
			        s: params.term,
			      }

			      return query;

			    },
			    processResults: function (data, params) {
			      // parse the results into the format expected by Select2
			      // since we are using custom formatting functions we do not need to
			      // alter the remote JSON data, except to indicate that infinite
			      // scrolling can be used
			      params.page = params.page || 1;

			      return {
			        results: data.items
			      };
			    },
			    cache: true
			}
		};

    	if( undefined != extendOption ) {
			$.extend( defautltOption, extendOption );
    	}

		$( className ).select2( defautltOption );

		if( undefined != extraFunction ) {
			extraFunction();
		}
	}

	// -----------------------------------------------------
	// Custom Initate Select2
	// Use this function if you wanna use select2 but not use remote data
	// 
	// Usage:
	// initSelect2(<your html class name>, <function after select2 was selected. this optional>)
	// 
	// eg. 
	// We have html like this :
	// <select class="cari-provinsi"></select>
	// 
	// So, in our javascript gonna be like this
	// initSelect2( '.cari-provinsi');
	// -----------------------------------------------------
	window.initSelect2 = function initSelect2( className, extraFunction ) {

		$( className ).select2({
		 	theme: "bootstrap",
		 	placeholder: $(className).data('placeholder')
		});

		if( undefined != extraFunction ) {
			extraFunction();
		}
	}


    // -----------------------------------------------------
	// Ajax File Upload
	// -----------------------------------------------------
	window.gmaAjaxFileUpload = function gmaAjaxFileUpload( ele, customFunction ) {

	    $( ele ).fileupload({
			url: $( ele ).attr('data-url'),
	        dataType: 'json',
	        add: function (e, data) {
	            
	            showLoading('System now uploading the image');
	            data.submit();

	        },
	        done: function (e, data) {

	        	var r = data.result
	        	if( r.status == 'ok' ) {
	        		
	        		customFunction.call( $(this), data, r );
	        		swal.close();

	        	} else {

	        		swal("Error on Uploading", r.message, "error");

	        	}

	        }
	    });

	}

	window.ajaxDeleteImage = function ajaxDeleteImage( ele, urlDelete, filename, customFunction ) {
		
		var button = $(ele);

		showSweetAlert( ele,function () {

			$.ajax({
				type: 'POST',
				url: urlDelete,
				data: {
					'file' : filename
				},
				success: function (r) {
					
					if( r == 'ok' ) {

		        		customFunction.call( this, button, r);
					} else {

		        		swal("Server error.", 'Sorry, but currently we have problem with server. please try again', "error");
					}
				}
			});
		});

		return false;

	}

	window.ajaxDeleteFile = function ajaxDeleteFile( ele, urlDelete, filename, rawname, customFunction ) {
		
		var button = $(ele);

		showSweetAlert( ele,function () {

			$.ajax({
				type: 'POST',
				url: urlDelete,
				data: {
					'file' : filename
				},
				success: function (r) {
					
					if( r == 'ok' ) {

		        		customFunction.call( this, button, filename, rawname);
					} else {

		        		swal("Server error.", 'Sorry, but currently we have problem with server. please try again', "error");
					}
				}
			});
		});

		return false;

	}

	window.preloaderPage = function preloaderPage() {

		$('.pace-progress').attr('data-progress-text', '0%');
		$('.gma--wrapper').addClass('gma--wrapper--hide');
    	$('.pace').attr('class', 'pace active');
    	$('body').removeClass('pace-done');
    	$('body').addClass('pace-running');
	}

	$(document).on('click', '.display--preloader--first', function () {
    	
    	var attr = $(this).attr('onclick');
    	if (!attr) {
	    	preloaderPage();
		}
    	
    });

	$.extend( $.fn.dataTable.defaults, {
	    "autoWidth": false,
		"language": {
		    "lengthMenu": "Show Data _MENU_"
		  },
        "processing": true,
        "serverSide": true,
    } );

    window.initDataTable = function initDataTable( className, extendOption ) {

    	var defautltOption = {
    		"order": [],
    		"oLanguage": {
	        	"sProcessing": '<div class="gma--datatable--loader"><img src="'+ LOADERGIF +'" alt="loading" /></div>'
	        },
    		"ajax": {
	            "type": "POST",
	        	"url": $( className ).data('url'),
		        }
    	};

    	if( undefined != extendOption ) {
			$.extend( defautltOption, extendOption );
    	}

    	$( className ).DataTable(defautltOption);
    }

    $('.format--rupiah, .format--number').maskMoney({
		thousands: '.',
		decimal : ',',
		precision: 0
	});

    //test input mask money
	$("#currency").maskMoney({
    formatOnBlur: true,
    reverse: true,
    prefix: 'Rp ',
    selectAllOnFocus: true,
    precision: 0,
    decimal: '.',
    thousands: ','
	}).on('input', function(e) {
      var v = $(this).maskMoney('unmasked')[0];
      $('#' + this.id + 'Hidden').val(v);
 	});
	//end test

	window.getHtmlJsonData = function getHtmlJsonData() {

		var JsonText = JSON.parse( $('.html--json--data').html() );
		return JsonText;
		
	}

	window.ucFirst = function ucFirst( string ) {
		return string.substring(0, 1).toUpperCase() + string.substring(1).toLowerCase();
	}

	function toRp(angka){
	    var rev     = parseInt(angka, 10).toString().split('').reverse().join('');
	    var rev2    = '';
	    for(var i = 0; i < rev.length; i++){
	        rev2  += rev[i];
	        if((i + 1) % 3 === 0 && i !== (rev.length - 1)){
	            rev2 += '.';
	        }
	    }
	    return rev2.split('').reverse().join('');
	}

	$(document).popover({
	  	selector: '[data-toggle=popover]',
	    html: true,
	    trigger: 'focus',
        content: function() {
          return $( this ).next("#popover-content").html();
        },
        title: function() {
          return $( this ).next("#popover-title").html();
        }
	});
});

function dateNow(){
	var $d = new Date();
	var month = new Array();
	month[0] = "January";
	month[1] = "February";
	month[2] = "March";
	month[3] = "April";
	month[4] = "May";
	month[5] = "June";
	month[6] = "July";
	month[7] = "August";
	month[8] = "September";
	month[9] = "October";
	month[10] = "November";
	month[11] = "December";

	return  $d.getDate().toString() +' '+ month[$d.getMonth()] +' '+ $d.getFullYear().toString();
}